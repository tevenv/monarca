﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Colorfull;

namespace Colorful_visual
{
    public partial class Producto : Form
    {
        int idusuario;
        C_Producto obj = new C_Producto();
        public Producto(int idusuario)
        {
            this.idusuario = idusuario;
            InitializeComponent();
        }

        private void Abrircrear(object formadmin)
        {
            if (this.contenedor4.Controls.Count > 0)
            {
                this.contenedor4.Controls.RemoveAt(0);
            }

            Form admin = formadmin as Form;

            admin.TopLevel = false;
            admin.Dock = DockStyle.Fill;
            this.contenedor4.Controls.Add(admin);
            this.contenedor4.Tag = admin;
            admin.Show();
        }

        private void Abriringresar(object formadmin)
        {
            if (this.contenedor4.Controls.Count > 0)
            {
                this.contenedor4.Controls.RemoveAt(0);
            }

            Form admin = formadmin as Form;

            admin.TopLevel = false;
            admin.Dock = DockStyle.Fill;
            this.contenedor4.Controls.Add(admin);
            this.contenedor4.Tag = admin;
            admin.Show();
        }

        private void Abrirextraer(object formadmin)
        {
            if (this.contenedor4.Controls.Count > 0)
            {
                this.contenedor4.Controls.RemoveAt(0);
            }

            Form admin = formadmin as Form;

            admin.TopLevel = false;
            admin.Dock = DockStyle.Fill;
            this.contenedor4.Controls.Add(admin);
            this.contenedor4.Tag = admin;
            admin.Show();
        }

        private void Abrirmodificar(object formadmin)
        {
            if (this.contenedor4.Controls.Count > 0)
            {
                this.contenedor4.Controls.RemoveAt(0);
            }

            Form admin = formadmin as Form;

            admin.TopLevel = false;
            admin.Dock = DockStyle.Fill;
            this.contenedor4.Controls.Add(admin);
            this.contenedor4.Tag = admin;
            admin.Show();
        }

        private void Crear_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Abrircrear(new CREAR_PRODUC(idusuario));
        }

        private void LinkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Abriringresar(new INGRESAR_PRODUCTO(idusuario));
        }

        private void LinkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Abrirextraer(new EXTRAER_PRODUCTO(idusuario));
        }

        private void Modificar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Abrirmodificar(new MODIFICAR_PRODUCTO(idusuario));
        }

        private void iniciarpermi(object sender, EventArgs e)
        {
            DataTable permi = obj.asignarpermi("INSUMOS", idusuario);
            Crear.Enabled = permi.Rows[0][0].Equals(true) ? true : false;
            Modificar.Enabled = permi.Rows[0][1].Equals(true) ? true : false;
            linkLabel1.Enabled = permi.Rows[0][3].Equals(true) ? true : false;
            linkLabel2.Enabled = permi.Rows[0][3].Equals(true) ? true : false;

        }
    }
}
