﻿using ColorFul_visual;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Colorful_visual
{
    public partial class Administrador : Form
    {
        int idusuario;
        DataTable permi;
        
        public Administrador(int idusuario, DataTable permi)
        {
            this.idusuario = idusuario;
            this.permi = permi;
            InitializeComponent();
        }

        private void abrirHijo(Form formHijo)
        {
            this.contenedor2.Controls.Clear();

            int ancho = (-formHijo.Size.Width+contenedor2.Width)/2;
            int alto = (-formHijo.Size.Height + contenedor2.Height)/2;
            
            formHijo.TopLevel = false;
            formHijo.Location = new Point(ancho, alto);
            this.contenedor2.Controls.Add(formHijo);
            this.contenedor2.Tag = formHijo;
            formHijo.Show();
        }

        private void USUARIO_Click(object sender, EventArgs e)
        {
            abrirHijo(new usuario(idusuario));
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            abrirHijo(new permisos(idusuario));
        }

        private void iniciarpermi(object sender, EventArgs e)
        {
            int aux = 0;
            for (int i=0; i < permi.Rows.Count; i++)
            {
                for (int j = 2; j < 5; j++)
                {
                    // Thread.Sleep(200);
                    if (permi.Rows[i][1].Equals("USUARIOS") && permi.Rows[i][j].Equals(true))
                    {
                        USUARIO.Enabled = true;
                         aux = 1;
                        break;
                    }
                    else
                    {
                        USUARIO.Enabled = false;
                    }
                }
                if (aux == 1)
                {
                    aux = 0;
                    break;
                }
               
            }

            for (int i = 0; i < permi.Rows.Count; i++)
            {
                for (int j = 2; j < 5; j++)
                {
                    // Thread.Sleep(200);
                    if (permi.Rows[i][1].Equals("PERMISOS") && permi.Rows[i][j].Equals(true))
                    {
                        button2.Enabled = true;
                        aux = 1;
                        break;
                        
                    }
                    else
                    {
                        button2.Enabled = false;
                    }
                }
                if (aux == 1)
                {
                    aux = 0;
                    break;
                }
            }

        }

    }
}
