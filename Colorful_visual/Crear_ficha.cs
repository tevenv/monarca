﻿using Colorful_visual;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Colorfull;

namespace ColorFul_visual
{
    public partial class Crear_ficha : Form
    {
        Ficha obj = new Ficha();
        
        Panel contenedor4;

        string cod;
        string nom;
        int est;
        int idusuario;

        public Crear_ficha(Panel contenedor4, int idusuario )
        {
            this.contenedor4 = contenedor4;
            this.idusuario = idusuario;

            InitializeComponent();
        }

        public void Abririingresar(Object formadmin)
        {
            this.contenedor4.Controls.Clear();          

            Form admin = formadmin as Form;

            admin.TopLevel = false;
            admin.Dock = DockStyle.Fill;
            this.contenedor4.Controls.Add(admin);
            this.contenedor4.Tag = admin;
            admin.Show();          
        }

        private void GUARDAR_Click(object sender, EventArgs e)
        {
            DataTable tabla = obj.fichatec(Codigo.Text);
            if (tabla.Rows.Count>=1)
            {
                MessageBox.Show("la ficha tecnica ya existe, revisa en la lista de fichas tecnicas con el codigo o el Nombre para poder obtener mas información ");
            }
            else
            {

                if (string.IsNullOrEmpty(Codigo.Text))
                {
                    ERROR.SetError(Codigo, "Valor requerido, este valor debe ser unico");
                }
                else
                {
                    if (string.IsNullOrEmpty(Nombre.Text))
                    {
                        ERROR.SetError(Codigo, "Valor requerido, este valor debe ser unico");
                    }
                    else
                    {
                        if (ESTADO.SelectedIndex >= 0)
                        {
                            cod = Codigo.Text;
                            nom = Nombre.Text;
                            est = (ESTADO.Text.Equals("Activo")) ? 1 : 0;
                            this.Abririingresar(new ingresar_ficha(cod, nom, est, idusuario));
                        }
                        else
                        {
                            MessageBox.Show("Seleccione un estado");
                        }
                    }
                }
            }
           
        }

        private void BUSCAR_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(BUSCA.Text))
            {
                MessageBox.Show("Ingresa por favor un parametro de busqueda");
            }
            else
            {
                Form formulario = new Buscador(BUSCA.Text, MUESTRA, 3);
                this.BUSCA.Clear();
                this.MUESTRA.Items.Clear();
                formulario.Show();

            }
        }

        private void INICIAPERMI(object sender, EventArgs e)
        {
            DataTable permi = obj.asignarpermi("INSUMOS", idusuario);
            NOMFICH.Visible = permi.Rows[0][2].Equals(true) ? true : false;
            BUSCA.Visible = permi.Rows[0][2].Equals(true) ? true : false;
            BUSCAR.Visible = permi.Rows[0][2].Equals(true) ? true : false;
        }
    }
}
