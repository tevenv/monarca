﻿namespace Colorful_visual
{
    partial class clientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Estado = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.BUSCA = new System.Windows.Forms.Button();
            this.nitt = new System.Windows.Forms.Label();
            this.BUSCAR = new System.Windows.Forms.TextBox();
            this.MUESTRA = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.DIRECCION = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TELEFONO = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.APELLIDO = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.NOMBRE = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.DOCUMENTO = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.FECHACUMPLE = new System.Windows.Forms.DateTimePicker();
            this.ACTUALIZAR = new System.Windows.Forms.Button();
            this.MODIFICAR = new System.Windows.Forms.Button();
            this.GUARDAR = new System.Windows.Forms.Button();
            this.ERROR = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ERROR)).BeginInit();
            this.SuspendLayout();
            // 
            // Estado
            // 
            this.Estado.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Estado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Estado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Estado.FormattingEnabled = true;
            this.Estado.Items.AddRange(new object[] {
            "Activo",
            "Inactivo"});
            this.Estado.Location = new System.Drawing.Point(453, 325);
            this.Estado.Name = "Estado";
            this.Estado.Size = new System.Drawing.Size(153, 21);
            this.Estado.TabIndex = 76;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(399, 328);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 17);
            this.label7.TabIndex = 75;
            this.label7.Text = "Estado";
            // 
            // BUSCA
            // 
            this.BUSCA.BackColor = System.Drawing.Color.Transparent;
            this.BUSCA.BackgroundImage = global::Colorful_visual.Properties.Resources.buscar;
            this.BUSCA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BUSCA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BUSCA.FlatAppearance.BorderSize = 0;
            this.BUSCA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BUSCA.Location = new System.Drawing.Point(584, 11);
            this.BUSCA.Name = "BUSCA";
            this.BUSCA.Size = new System.Drawing.Size(24, 24);
            this.BUSCA.TabIndex = 74;
            this.BUSCA.UseVisualStyleBackColor = false;
            // 
            // nitt
            // 
            this.nitt.AutoSize = true;
            this.nitt.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nitt.Location = new System.Drawing.Point(361, 11);
            this.nitt.Name = "nitt";
            this.nitt.Size = new System.Drawing.Size(74, 17);
            this.nitt.TabIndex = 73;
            this.nitt.Text = "Documento";
            // 
            // BUSCAR
            // 
            this.BUSCAR.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BUSCAR.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.BUSCAR.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BUSCAR.Location = new System.Drawing.Point(441, 12);
            this.BUSCAR.Multiline = true;
            this.BUSCAR.Name = "BUSCAR";
            this.BUSCAR.Size = new System.Drawing.Size(130, 20);
            this.BUSCAR.TabIndex = 72;
            // 
            // MUESTRA
            // 
            this.MUESTRA.BackColor = System.Drawing.Color.WhiteSmoke;
            this.MUESTRA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MUESTRA.Font = new System.Drawing.Font("Raleway Medium", 8.999999F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MUESTRA.FormattingEnabled = true;
            this.MUESTRA.ItemHeight = 17;
            this.MUESTRA.Location = new System.Drawing.Point(409, 50);
            this.MUESTRA.Name = "MUESTRA";
            this.MUESTRA.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.MUESTRA.Size = new System.Drawing.Size(198, 221);
            this.MUESTRA.TabIndex = 71;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(23, 273);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 17);
            this.label5.TabIndex = 70;
            this.label5.Text = "Direccion";
            // 
            // DIRECCION
            // 
            this.DIRECCION.BackColor = System.Drawing.Color.WhiteSmoke;
            this.DIRECCION.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DIRECCION.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DIRECCION.Location = new System.Drawing.Point(145, 269);
            this.DIRECCION.Multiline = true;
            this.DIRECCION.Name = "DIRECCION";
            this.DIRECCION.Size = new System.Drawing.Size(193, 20);
            this.DIRECCION.TabIndex = 69;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(23, 213);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 17);
            this.label4.TabIndex = 68;
            this.label4.Text = "Telefono";
            // 
            // TELEFONO
            // 
            this.TELEFONO.BackColor = System.Drawing.Color.WhiteSmoke;
            this.TELEFONO.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TELEFONO.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TELEFONO.Location = new System.Drawing.Point(145, 210);
            this.TELEFONO.Multiline = true;
            this.TELEFONO.Name = "TELEFONO";
            this.TELEFONO.Size = new System.Drawing.Size(193, 20);
            this.TELEFONO.TabIndex = 67;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(23, 161);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 17);
            this.label3.TabIndex = 66;
            this.label3.Text = "Apellido";
            // 
            // APELLIDO
            // 
            this.APELLIDO.BackColor = System.Drawing.Color.WhiteSmoke;
            this.APELLIDO.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.APELLIDO.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.APELLIDO.Location = new System.Drawing.Point(145, 157);
            this.APELLIDO.Multiline = true;
            this.APELLIDO.Name = "APELLIDO";
            this.APELLIDO.Size = new System.Drawing.Size(193, 20);
            this.APELLIDO.TabIndex = 65;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(23, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 17);
            this.label2.TabIndex = 64;
            this.label2.Text = "Nombre";
            // 
            // NOMBRE
            // 
            this.NOMBRE.BackColor = System.Drawing.Color.WhiteSmoke;
            this.NOMBRE.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NOMBRE.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NOMBRE.Location = new System.Drawing.Point(145, 101);
            this.NOMBRE.Multiline = true;
            this.NOMBRE.Name = "NOMBRE";
            this.NOMBRE.Size = new System.Drawing.Size(193, 20);
            this.NOMBRE.TabIndex = 63;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 17);
            this.label1.TabIndex = 62;
            this.label1.Text = "Documento";
            // 
            // DOCUMENTO
            // 
            this.DOCUMENTO.BackColor = System.Drawing.Color.WhiteSmoke;
            this.DOCUMENTO.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DOCUMENTO.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DOCUMENTO.Location = new System.Drawing.Point(145, 44);
            this.DOCUMENTO.Multiline = true;
            this.DOCUMENTO.Name = "DOCUMENTO";
            this.DOCUMENTO.Size = new System.Drawing.Size(193, 20);
            this.DOCUMENTO.TabIndex = 61;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(23, 328);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 17);
            this.label6.TabIndex = 78;
            this.label6.Text = "Cumpleaños";
            // 
            // FECHACUMPLE
            // 
            this.FECHACUMPLE.CalendarMonthBackground = System.Drawing.Color.WhiteSmoke;
            this.FECHACUMPLE.CustomFormat = "yyyy/MM/dd";
            this.FECHACUMPLE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FECHACUMPLE.Location = new System.Drawing.Point(145, 322);
            this.FECHACUMPLE.Name = "FECHACUMPLE";
            this.FECHACUMPLE.Size = new System.Drawing.Size(193, 20);
            this.FECHACUMPLE.TabIndex = 79;
            // 
            // ACTUALIZAR
            // 
            this.ACTUALIZAR.BackColor = System.Drawing.Color.Transparent;
            this.ACTUALIZAR.BackgroundImage = global::Colorful_visual.Properties.Resources.ACTUALIZAR;
            this.ACTUALIZAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ACTUALIZAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ACTUALIZAR.FlatAppearance.BorderSize = 0;
            this.ACTUALIZAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ACTUALIZAR.Location = new System.Drawing.Point(392, 375);
            this.ACTUALIZAR.Name = "ACTUALIZAR";
            this.ACTUALIZAR.Size = new System.Drawing.Size(95, 32);
            this.ACTUALIZAR.TabIndex = 82;
            this.ACTUALIZAR.UseVisualStyleBackColor = false;
            // 
            // MODIFICAR
            // 
            this.MODIFICAR.BackColor = System.Drawing.Color.Transparent;
            this.MODIFICAR.BackgroundImage = global::Colorful_visual.Properties.Resources.MODIFICAR;
            this.MODIFICAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MODIFICAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MODIFICAR.FlatAppearance.BorderSize = 0;
            this.MODIFICAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MODIFICAR.Location = new System.Drawing.Point(270, 375);
            this.MODIFICAR.Name = "MODIFICAR";
            this.MODIFICAR.Size = new System.Drawing.Size(95, 32);
            this.MODIFICAR.TabIndex = 81;
            this.MODIFICAR.UseVisualStyleBackColor = false;
            // 
            // GUARDAR
            // 
            this.GUARDAR.BackColor = System.Drawing.Color.Transparent;
            this.GUARDAR.BackgroundImage = global::Colorful_visual.Properties.Resources.GUARDAR;
            this.GUARDAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.GUARDAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.GUARDAR.FlatAppearance.BorderSize = 0;
            this.GUARDAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GUARDAR.Location = new System.Drawing.Point(146, 375);
            this.GUARDAR.Name = "GUARDAR";
            this.GUARDAR.Size = new System.Drawing.Size(95, 32);
            this.GUARDAR.TabIndex = 80;
            this.GUARDAR.UseVisualStyleBackColor = false;
            // 
            // ERROR
            // 
            this.ERROR.ContainerControl = this;
            // 
            // clientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(631, 425);
            this.Controls.Add(this.ACTUALIZAR);
            this.Controls.Add(this.MODIFICAR);
            this.Controls.Add(this.GUARDAR);
            this.Controls.Add(this.FECHACUMPLE);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Estado);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.BUSCA);
            this.Controls.Add(this.nitt);
            this.Controls.Add(this.BUSCAR);
            this.Controls.Add(this.MUESTRA);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.DIRECCION);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TELEFONO);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.APELLIDO);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.NOMBRE);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DOCUMENTO);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "clientes";
            this.Text = "clientes";
            ((System.ComponentModel.ISupportInitialize)(this.ERROR)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox Estado;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button BUSCA;
        private System.Windows.Forms.Label nitt;
        private System.Windows.Forms.TextBox BUSCAR;
        private System.Windows.Forms.ListBox MUESTRA;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox DIRECCION;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TELEFONO;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox APELLIDO;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox NOMBRE;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox DOCUMENTO;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker FECHACUMPLE;
        private System.Windows.Forms.Button ACTUALIZAR;
        private System.Windows.Forms.Button MODIFICAR;
        private System.Windows.Forms.Button GUARDAR;
        private System.Windows.Forms.ErrorProvider ERROR;
    }
}