﻿namespace Colorful_visual
{
    partial class ingresar_ficha
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.CANTIDAD = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.INSUMO = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Nombre = new System.Windows.Forms.Label();
            this.Codigo = new System.Windows.Forms.Label();
            this.MOSTRAR = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GUARDAR = new System.Windows.Forms.Button();
            this.ERROR = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.MOSTRAR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ERROR)).BeginInit();
            this.SuspendLayout();
            // 
            // CANTIDAD
            // 
            this.CANTIDAD.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CANTIDAD.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CANTIDAD.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CANTIDAD.Location = new System.Drawing.Point(319, 95);
            this.CANTIDAD.Multiline = true;
            this.CANTIDAD.Name = "CANTIDAD";
            this.CANTIDAD.Size = new System.Drawing.Size(149, 20);
            this.CANTIDAD.TabIndex = 22;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(135, 63);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 14);
            this.label7.TabIndex = 21;
            this.label7.Text = "Insumo";
            // 
            // INSUMO
            // 
            this.INSUMO.BackColor = System.Drawing.Color.WhiteSmoke;
            this.INSUMO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.INSUMO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.INSUMO.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.INSUMO.FormattingEnabled = true;
            this.INSUMO.Location = new System.Drawing.Point(84, 92);
            this.INSUMO.Name = "INSUMO";
            this.INSUMO.Size = new System.Drawing.Size(157, 23);
            this.INSUMO.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(369, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 14);
            this.label1.TabIndex = 23;
            this.label1.Text = "Cantidad";
            // 
            // Nombre
            // 
            this.Nombre.AutoSize = true;
            this.Nombre.Font = new System.Drawing.Font("Raleway Medium", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nombre.Location = new System.Drawing.Point(351, 15);
            this.Nombre.Name = "Nombre";
            this.Nombre.Size = new System.Drawing.Size(72, 18);
            this.Nombre.TabIndex = 25;
            this.Nombre.Text = "Nombre";
            // 
            // Codigo
            // 
            this.Codigo.AutoSize = true;
            this.Codigo.Font = new System.Drawing.Font("Raleway Medium", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Codigo.Location = new System.Drawing.Point(120, 15);
            this.Codigo.Name = "Codigo";
            this.Codigo.Size = new System.Drawing.Size(63, 18);
            this.Codigo.TabIndex = 24;
            this.Codigo.Text = "Codigo";
            // 
            // MOSTRAR
            // 
            this.MOSTRAR.AllowUserToAddRows = false;
            this.MOSTRAR.AllowUserToDeleteRows = false;
            this.MOSTRAR.AllowUserToOrderColumns = true;
            this.MOSTRAR.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.MOSTRAR.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.MOSTRAR.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MOSTRAR.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(168)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Raleway Medium", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(168)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.MOSTRAR.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.MOSTRAR.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MOSTRAR.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.MOSTRAR.EnableHeadersVisualStyles = false;
            this.MOSTRAR.GridColor = System.Drawing.Color.WhiteSmoke;
            this.MOSTRAR.Location = new System.Drawing.Point(84, 152);
            this.MOSTRAR.Name = "MOSTRAR";
            this.MOSTRAR.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.MOSTRAR.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.MOSTRAR.RowHeadersVisible = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Raleway Light", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.MOSTRAR.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.MOSTRAR.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.MOSTRAR.Size = new System.Drawing.Size(384, 108);
            this.MOSTRAR.TabIndex = 26;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Codigo";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Nombre";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Insumo";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Cantidad";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // GUARDAR
            // 
            this.GUARDAR.BackColor = System.Drawing.Color.Transparent;
            this.GUARDAR.BackgroundImage = global::Colorful_visual.Properties.Resources.GUARDAR;
            this.GUARDAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.GUARDAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.GUARDAR.FlatAppearance.BorderSize = 0;
            this.GUARDAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GUARDAR.Location = new System.Drawing.Point(237, 279);
            this.GUARDAR.Name = "GUARDAR";
            this.GUARDAR.Size = new System.Drawing.Size(95, 32);
            this.GUARDAR.TabIndex = 27;
            this.GUARDAR.UseVisualStyleBackColor = false;
            this.GUARDAR.Click += new System.EventHandler(this.GUARDAR_Click);
            // 
            // ERROR
            // 
            this.ERROR.ContainerControl = this;
            // 
            // ingresar_ficha
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(558, 328);
            this.Controls.Add(this.GUARDAR);
            this.Controls.Add(this.MOSTRAR);
            this.Controls.Add(this.Nombre);
            this.Controls.Add(this.Codigo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CANTIDAD);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.INSUMO);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ingresar_ficha";
            this.Text = "x";
            ((System.ComponentModel.ISupportInitialize)(this.MOSTRAR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ERROR)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox CANTIDAD;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox INSUMO;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Nombre;
        private System.Windows.Forms.Label Codigo;
        private System.Windows.Forms.DataGridView MOSTRAR;
        private System.Windows.Forms.Button GUARDAR;
        private System.Windows.Forms.ErrorProvider ERROR;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
    }
}