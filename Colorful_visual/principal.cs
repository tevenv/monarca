﻿using ColorFul_visual;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


using Colorfull;

namespace Colorful_visual
{
    
    public partial class principal : Form
    {
        Inscripcion obj = new Inscripcion();
        int idusuario;
        DataTable permi;

        private Administrador adminForm;
        private almacen almacenForm;
        private Informes informesForm;
        private Ventas ventasForm;

        public principal(int idusuario, DataTable permi)
        {
            this.permi = permi;
            this.idusuario = idusuario;
            InitializeComponent();
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;

            Size pantalla = System.Windows.Forms.SystemInformation.PrimaryMonitorSize;

            int ancho = (pantalla.Width);

            central1.Location = new Point(central1.Location.X, central1.Location.Y);

        }

        private void abrirHijo(Form formHijo)
        {
            this.contenedor1.Controls.Clear();

            formHijo.TopLevel = false;
            formHijo.Dock = DockStyle.Fill;
            this.contenedor1.Controls.Add(formHijo);
            this.contenedor1.Tag = formHijo;
            formHijo.Show();
        }

        private void CERRAR_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MINI_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void ADMIN_Click(object sender, EventArgs e)
        {
            adminForm = adminForm ?? new Administrador(idusuario, permi);
            abrirHijo(adminForm);
        }

        private void TALLER_Click(object sender, EventArgs e)
        {
            almacenForm = almacenForm ?? new almacen(idusuario, permi);
            abrirHijo(almacenForm);
        }

        private void Ventas_Click(object sender, EventArgs e)
        {
            ventasForm = ventasForm ?? new Ventas();
            abrirHijo(ventasForm);
        }

        private void INFO_Click(object sender, EventArgs e)
        {
            informesForm = informesForm ?? new Informes();
            abrirHijo(informesForm);
        }

        

         private void iniciopermi(object sender, EventArgs e)
         {
             for(int i = 0; i < permi.Rows.Count; i++)
             {
                 for(int j=2; j < 5; j++)
                 {
                    // Thread.Sleep(200);
                     if (permi.Rows[i][0].Equals("ADMINISTRADOR") && permi.Rows[i][j].Equals(true))
                     {
                         ADMIN.Enabled = true;
                         break;
                     }
                     else
                     {
                         ADMIN.Enabled = false;
                     }
                 }
                 break;
             }
             

             for (int i = 0; i < permi.Rows.Count; i++)
             {
                 for (int j = 2; j < 5; j++)
                 {
                    //Thread.Sleep(200);
                     if (permi.Rows[i][0].Equals("TALLER") && permi.Rows[i][j].Equals(true))
                     {
                         TALLER.Enabled = true;
                         break;
                     }
                     else
                     {
                         TALLER.Enabled = false;
                     }
                 }
                 if (TALLER.Enabled == true)
                 {
                     break;
                 }
             }
            if (permi.Rows.Count.Equals(0))
            {
                ADMIN.Enabled = false;
                TALLER.Enabled = false;
            }

            DataTable img =  obj.mostrarlog(1);
            logo.Image = Image.FromFile(img.Rows[0][0].ToString());
            logo.BackColor = Color.Transparent;
            slogan.Text = img.Rows[0][1].ToString();
         }

        
    }
}
