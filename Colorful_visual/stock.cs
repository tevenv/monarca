﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Colorful_visual
{
    public partial class stock : Form
    {
        public stock()
        {
            InitializeComponent();
        }

        private void abrirHijo(Form formHijo)
        {
            if (this.contenedor1.Controls.Count > 0)
            {
                this.contenedor1.Controls.RemoveAt(0);
            }

            formHijo.TopLevel = false;
            formHijo.Dock = DockStyle.Fill;
            this.contenedor1.Controls.Add(formHijo);
            this.contenedor1.Tag = formHijo;
            formHijo.Show();
        }

        private void Ingresar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            abrirHijo(new ingresar_stock());
        }

        private void Extraer_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            abrirHijo(new extraer_stock());
        }
    }
}
