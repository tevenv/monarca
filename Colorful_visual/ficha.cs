﻿using ColorFul_visual;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Colorfull;

namespace Colorful_visual
{
    public partial class ficha : Form
    {
        int idusuario;
        Ficha obj = new Ficha();
        public ficha(int idusuario)
        {
            this.idusuario = idusuario;
            InitializeComponent();
           
        }

        private void Abrircrear(object formadmin)
        {
            if (this.contenedor4.Controls.Count > 0)
            {
                this.contenedor4.Controls.RemoveAt(0);
            }

            Form admin = formadmin as Form;

            admin.TopLevel = false;
            admin.Dock = DockStyle.Fill;
            this.contenedor4.Controls.Add(admin);
            this.contenedor4.Tag = admin;
            admin.Show();
        }

        private void Abrirmodificar(object formadmin)
        {
            if (this.contenedor4.Controls.Count > 0)
            {
                this.contenedor4.Controls.RemoveAt(0);
            }

            Form admin = formadmin as Form;

            admin.TopLevel = false;
            admin.Dock = DockStyle.Fill;
            this.contenedor4.Controls.Add(admin);
            this.contenedor4.Tag = admin;
            admin.Show();
        }

       
        private void Crear_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Abrircrear(new Crear_ficha(this.contenedor4, idusuario));
        }

        private void Modificar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
             Abrirmodificar(new MODIFICAR_FICHA(idusuario));
        }

        private void asignarpermi(object sender, EventArgs e)
        {
            DataTable permi = obj.asignarpermi("INSUMOS", idusuario);
            Crear.Enabled = permi.Rows[0][0].Equals(true) ? true : false;
            Modificar.Enabled = permi.Rows[0][1].Equals(true) ? true : false;
        }
    }
}
