﻿namespace Colorful_visual
{
    partial class Producto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Modificar = new System.Windows.Forms.LinkLabel();
            this.Crear = new System.Windows.Forms.LinkLabel();
            this.contenedor4 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Modificar
            // 
            this.Modificar.ActiveLinkColor = System.Drawing.Color.WhiteSmoke;
            this.Modificar.AutoSize = true;
            this.Modificar.Font = new System.Drawing.Font("Raleway Medium", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Modificar.LinkColor = System.Drawing.Color.Fuchsia;
            this.Modificar.Location = new System.Drawing.Point(341, 1);
            this.Modificar.Name = "Modificar";
            this.Modificar.Size = new System.Drawing.Size(125, 26);
            this.Modificar.TabIndex = 2;
            this.Modificar.TabStop = true;
            this.Modificar.Text = "MODIFICAR";
            this.Modificar.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Modificar_LinkClicked);
            // 
            // Crear
            // 
            this.Crear.ActiveLinkColor = System.Drawing.Color.WhiteSmoke;
            this.Crear.AutoSize = true;
            this.Crear.Font = new System.Drawing.Font("Raleway Medium", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Crear.LinkColor = System.Drawing.Color.Fuchsia;
            this.Crear.Location = new System.Drawing.Point(12, 1);
            this.Crear.Name = "Crear";
            this.Crear.Size = new System.Drawing.Size(78, 26);
            this.Crear.TabIndex = 0;
            this.Crear.TabStop = true;
            this.Crear.Text = "CREAR";
            this.Crear.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Crear_LinkClicked);
            // 
            // contenedor4
            // 
            this.contenedor4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contenedor4.Location = new System.Drawing.Point(0, 27);
            this.contenedor4.Name = "contenedor4";
            this.contenedor4.Size = new System.Drawing.Size(566, 327);
            this.contenedor4.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.linkLabel2);
            this.panel1.Controls.Add(this.linkLabel1);
            this.panel1.Controls.Add(this.Modificar);
            this.panel1.Controls.Add(this.Crear);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(566, 27);
            this.panel1.TabIndex = 4;
            // 
            // linkLabel2
            // 
            this.linkLabel2.ActiveLinkColor = System.Drawing.Color.WhiteSmoke;
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Font = new System.Drawing.Font("Raleway Medium", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel2.LinkColor = System.Drawing.Color.Fuchsia;
            this.linkLabel2.Location = new System.Drawing.Point(219, 1);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(102, 26);
            this.linkLabel2.TabIndex = 4;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "EXTRAER";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabel2_LinkClicked);
            // 
            // linkLabel1
            // 
            this.linkLabel1.ActiveLinkColor = System.Drawing.Color.WhiteSmoke;
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Raleway Medium", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.LinkColor = System.Drawing.Color.Fuchsia;
            this.linkLabel1.Location = new System.Drawing.Point(98, 1);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(114, 26);
            this.linkLabel1.TabIndex = 3;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "INGRESAR";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabel1_LinkClicked);
            // 
            // Producto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(566, 354);
            this.Controls.Add(this.contenedor4);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Producto";
            this.Text = "Producto";
            this.Load += new System.EventHandler(this.iniciarpermi);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.LinkLabel Modificar;
        private System.Windows.Forms.LinkLabel Crear;
        public System.Windows.Forms.Panel contenedor4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.LinkLabel linkLabel1;
    }
}