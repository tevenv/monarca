﻿namespace Colorful_visual
{
    partial class Administrador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.USUARIO = new System.Windows.Forms.Button();
            this.contenedor2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.BackgroundImage = global::Colorful_visual.Properties.Resources.rojao;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 85);
            this.panel1.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.USUARIO);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(702, 85);
            this.panel2.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.BackgroundImage = global::Colorful_visual.Properties.Resources.PERMISOS;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(330, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(137, 58);
            this.button2.TabIndex = 1;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // USUARIO
            // 
            this.USUARIO.BackgroundImage = global::Colorful_visual.Properties.Resources.USUARIO;
            this.USUARIO.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.USUARIO.Cursor = System.Windows.Forms.Cursors.Hand;
            this.USUARIO.FlatAppearance.BorderSize = 0;
            this.USUARIO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.USUARIO.Location = new System.Drawing.Point(166, 12);
            this.USUARIO.Name = "USUARIO";
            this.USUARIO.Size = new System.Drawing.Size(137, 58);
            this.USUARIO.TabIndex = 0;
            this.USUARIO.UseVisualStyleBackColor = true;
            this.USUARIO.Click += new System.EventHandler(this.USUARIO_Click);
            // 
            // contenedor2
            // 
            this.contenedor2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contenedor2.Location = new System.Drawing.Point(0, 85);
            this.contenedor2.Name = "contenedor2";
            this.contenedor2.Size = new System.Drawing.Size(800, 365);
            this.contenedor2.TabIndex = 2;
            // 
            // Administrador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.contenedor2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "Administrador";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administrador";
            this.Load += new System.EventHandler(this.iniciarpermi);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button USUARIO;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel contenedor2;
    }
}