﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Colorfull;

namespace Colorful_visual
{
    
    public partial class CREAR_INSUMO : Form
    {
        int idusuario;
        AG_insu obj = new AG_insu();
        validador aux = new validador();
        public CREAR_INSUMO(int idusuario)
        {
            this.idusuario = idusuario;
            InitializeComponent();
            this.seleccionar();
        }

        private void Guardar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Codigo.Text))
            {
                ERROR.SetError(Codigo, "Valor requerido, este valor debe ser unico");
            }
            else
            {
                if (string.IsNullOrEmpty(Nombre.Text))
                {
                    ERROR.SetError(Nombre, "Valor requerido");
                }
                else
                {
                    obj.Codigo = Codigo.Text;
                    obj.Nombre = Nombre.Text;
                    obj.Cantidad = 0;
                    if (Proveedor.SelectedIndex >=0)
                    {
                        
                        if (Estado.SelectedIndex >= 0)
                        {
                            obj.Estado = (Estado.Text.Equals("Activo")) ? 1 : 0;
                            obj.Usuario = idusuario;
                            this.Mostrar.Items.Clear();

                            try
                            {
                                if (obj.crear() >= 1 )
                                {
                                    if (obj.almacenar("CRE",0) >= 1)
                                    {

                                        DataTable tabla = obj.consultarinsumo(Codigo.Text);

                                        for (int i = 0; i < tabla.Columns.Count; i++)
                                        {
                                            Mostrar.Items.Add(tabla.Rows[0][i].ToString());
                                            Mostrar.Items.Add(" ");

                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("Se presento un error, revise que los campos esten completos o llame a apoyo tecnico");
                                    }
                                    
                                }
                                else
                                {
                                    if (obj.crear() == -2)
                                    {
                                        MessageBox.Show("El insumo ya existe, revisa en la lista de insumos con el codigo o el Nombre para poder obtener mas información ");
                                    }
                                    else
                                    {
                                        MessageBox.Show("Se presento un error inesperado, revise que los datos esten completos.");
                                    }
                                }

                            }
                            catch (Exception ex)
                            {

                                MessageBox.Show("Se a presentado un error inesperado, llamar al apoyo tecnico, error numero" + ex);
                            }
                            this.limpiar(this);

                        }
                        else
                        {
                            MessageBox.Show("Seleccione un estado");
                        }
                    }
                    else
                    {
                        ERROR.SetError(Proveedor, "Valor requerido");

                    }
                }
            }

        }

        private void seleccionar()
        {
            DataTable tabla = obj.traerproveedor();
            
            for(int i =0; i< tabla.Rows.Count; i++)
            {
                Proveedor.Items.Add(tabla.Rows[i]["Nombre"].ToString());
               
            }      

        }

        private void limpiar(Control aux)
        {
            foreach (var txt in aux.Controls)
            {
                if (txt is TextBox)
                {
                    ((TextBox)txt).Clear();

                }
                else if (txt is ComboBox)
                {
                    ((ComboBox)txt).SelectedIndex = -1;
                }

            }

            ERROR.Clear();
        }


        private void Proveedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable tabla = obj.traerproveedor();

            for (int i = 0; i < tabla.Rows.Count; i++)
            {
                if (tabla.Rows[i]["Nombre"].ToString().Equals(Proveedor.Text))
                {
                    obj.Proveedor = tabla.Rows[i]["Nit"].ToString();
                    break;
                }

            }
        }

        private void Buscador_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Buscar.Text))
            {
                MessageBox.Show("Ingresa por favor un parametro de busqueda");
            }
            else
            {
                Form formulario = new Buscador(Buscar.Text, Mostrar, 2);
                this.limpiar(this);
                this.Mostrar.Items.Clear();
                formulario.Show();

            }
        }

        private void asignarpermi(object sender, EventArgs e)
        {
            DataTable permi = obj.asignarpermi("INSUMOS", idusuario);
            inss.Visible = permi.Rows[0][2].Equals(true) ? true : false;
            Buscar.Visible = permi.Rows[0][2].Equals(true) ? true : false;
            buscador.Visible = permi.Rows[0][2].Equals(true) ? true : false;
        }
    }
}
