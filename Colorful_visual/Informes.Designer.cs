﻿namespace Colorful_visual
{
    partial class Informes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.PROVEEDOR = new System.Windows.Forms.Button();
            this.INSUMOS = new System.Windows.Forms.Button();
            this.PRODUCTO = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.contenedor1 = new System.Windows.Forms.Panel();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.PRODUCTO);
            this.panel3.Controls.Add(this.INSUMOS);
            this.panel3.Controls.Add(this.PROVEEDOR);
            this.panel3.Location = new System.Drawing.Point(2, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(764, 83);
            this.panel3.TabIndex = 0;
            // 
            // PROVEEDOR
            // 
            this.PROVEEDOR.BackgroundImage = global::Colorful_visual.Properties.Resources.PROVEEDOR;
            this.PROVEEDOR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PROVEEDOR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PROVEEDOR.FlatAppearance.BorderSize = 0;
            this.PROVEEDOR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PROVEEDOR.Location = new System.Drawing.Point(158, 12);
            this.PROVEEDOR.Name = "PROVEEDOR";
            this.PROVEEDOR.Size = new System.Drawing.Size(137, 58);
            this.PROVEEDOR.TabIndex = 1;
            this.PROVEEDOR.UseVisualStyleBackColor = true;
            this.PROVEEDOR.Click += new System.EventHandler(this.PROVEEDOR_Click);
            // 
            // INSUMOS
            // 
            this.INSUMOS.BackgroundImage = global::Colorful_visual.Properties.Resources.insumos;
            this.INSUMOS.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.INSUMOS.Cursor = System.Windows.Forms.Cursors.Hand;
            this.INSUMOS.FlatAppearance.BorderSize = 0;
            this.INSUMOS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.INSUMOS.Location = new System.Drawing.Point(320, 12);
            this.INSUMOS.Name = "INSUMOS";
            this.INSUMOS.Size = new System.Drawing.Size(137, 58);
            this.INSUMOS.TabIndex = 4;
            this.INSUMOS.UseVisualStyleBackColor = true;
            this.INSUMOS.Click += new System.EventHandler(this.INSUMOS_Click);
            // 
            // PRODUCTO
            // 
            this.PRODUCTO.BackgroundImage = global::Colorful_visual.Properties.Resources.PRODUCTO;
            this.PRODUCTO.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PRODUCTO.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PRODUCTO.FlatAppearance.BorderSize = 0;
            this.PRODUCTO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PRODUCTO.Location = new System.Drawing.Point(478, 12);
            this.PRODUCTO.Name = "PRODUCTO";
            this.PRODUCTO.Size = new System.Drawing.Size(137, 58);
            this.PRODUCTO.TabIndex = 3;
            this.PRODUCTO.UseVisualStyleBackColor = true;
            this.PRODUCTO.Click += new System.EventHandler(this.PRODUCTO_Click);
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::Colorful_visual.Properties.Resources.rojao;
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(853, 83);
            this.panel1.TabIndex = 0;
            // 
            // contenedor1
            // 
            this.contenedor1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contenedor1.Location = new System.Drawing.Point(0, 83);
            this.contenedor1.Name = "contenedor1";
            this.contenedor1.Size = new System.Drawing.Size(853, 367);
            this.contenedor1.TabIndex = 1;
            // 
            // Informes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(853, 450);
            this.Controls.Add(this.contenedor1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Informes";
            this.Text = "Informes";
            this.TransparencyKey = System.Drawing.Color.White;
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button PRODUCTO;
        private System.Windows.Forms.Button INSUMOS;
        private System.Windows.Forms.Button PROVEEDOR;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel contenedor1;
    }
}