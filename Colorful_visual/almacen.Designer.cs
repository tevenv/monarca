﻿namespace ColorFul_visual
{
    partial class almacen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.contenedor3 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.INSUMOS = new System.Windows.Forms.Button();
            this.PRODUCTO = new System.Windows.Forms.Button();
            this.FICHA = new System.Windows.Forms.Button();
            this.PROVEEDOR = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // contenedor3
            // 
            this.contenedor3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contenedor3.Location = new System.Drawing.Point(0, 100);
            this.contenedor3.Name = "contenedor3";
            this.contenedor3.Size = new System.Drawing.Size(839, 350);
            this.contenedor3.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::Colorful_visual.Properties.Resources.rojao;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(839, 100);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.INSUMOS);
            this.panel2.Controls.Add(this.PRODUCTO);
            this.panel2.Controls.Add(this.FICHA);
            this.panel2.Controls.Add(this.PROVEEDOR);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(775, 100);
            this.panel2.TabIndex = 0;
            // 
            // INSUMOS
            // 
            this.INSUMOS.BackgroundImage = global::Colorful_visual.Properties.Resources.insumos;
            this.INSUMOS.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.INSUMOS.Cursor = System.Windows.Forms.Cursors.Hand;
            this.INSUMOS.FlatAppearance.BorderSize = 0;
            this.INSUMOS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.INSUMOS.Location = new System.Drawing.Point(307, 21);
            this.INSUMOS.Name = "INSUMOS";
            this.INSUMOS.Size = new System.Drawing.Size(137, 58);
            this.INSUMOS.TabIndex = 3;
            this.INSUMOS.UseVisualStyleBackColor = true;
            this.INSUMOS.Click += new System.EventHandler(this.INSUMOS_Click);
            // 
            // PRODUCTO
            // 
            this.PRODUCTO.BackgroundImage = global::Colorful_visual.Properties.Resources.PRODUCTO;
            this.PRODUCTO.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PRODUCTO.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PRODUCTO.FlatAppearance.BorderSize = 0;
            this.PRODUCTO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PRODUCTO.Location = new System.Drawing.Point(605, 21);
            this.PRODUCTO.Name = "PRODUCTO";
            this.PRODUCTO.Size = new System.Drawing.Size(137, 58);
            this.PRODUCTO.TabIndex = 2;
            this.PRODUCTO.UseVisualStyleBackColor = true;
            this.PRODUCTO.Click += new System.EventHandler(this.PRODUCTO_Click);
            // 
            // FICHA
            // 
            this.FICHA.BackgroundImage = global::Colorful_visual.Properties.Resources.FICHA;
            this.FICHA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FICHA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FICHA.FlatAppearance.BorderSize = 0;
            this.FICHA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FICHA.Location = new System.Drawing.Point(455, 21);
            this.FICHA.Name = "FICHA";
            this.FICHA.Size = new System.Drawing.Size(137, 58);
            this.FICHA.TabIndex = 1;
            this.FICHA.UseVisualStyleBackColor = true;
            this.FICHA.Click += new System.EventHandler(this.FICHA_Click);
            // 
            // PROVEEDOR
            // 
            this.PROVEEDOR.BackgroundImage = global::Colorful_visual.Properties.Resources.PROVEEDOR;
            this.PROVEEDOR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PROVEEDOR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PROVEEDOR.FlatAppearance.BorderSize = 0;
            this.PROVEEDOR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PROVEEDOR.Location = new System.Drawing.Point(158, 21);
            this.PROVEEDOR.Name = "PROVEEDOR";
            this.PROVEEDOR.Size = new System.Drawing.Size(137, 58);
            this.PROVEEDOR.TabIndex = 0;
            this.PROVEEDOR.UseVisualStyleBackColor = true;
            this.PROVEEDOR.Click += new System.EventHandler(this.PROVEEDOR_Click);
            // 
            // almacen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(839, 450);
            this.Controls.Add(this.contenedor3);
            this.Controls.Add(this.panel1);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "almacen";
            this.Text = "almacen";
            this.TransparencyKey = System.Drawing.Color.White;
            this.Load += new System.EventHandler(this.iniciarpermi);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button PROVEEDOR;
        private System.Windows.Forms.Button PRODUCTO;
        private System.Windows.Forms.Button FICHA;
        private System.Windows.Forms.Button INSUMOS;
        private System.Windows.Forms.Panel contenedor3;
    }
}