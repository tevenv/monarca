﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Colorful_visual
{
    public partial class Informes : Form
    {
        public Informes()
        {
            InitializeComponent();
        }

      

        private void abrirproveedores(object formalma)
        {
            if (this.contenedor1.Controls.Count > 0)
            {
                this.contenedor1.Controls.RemoveAt(0);
            }

            Form formul = formalma as Form;
            int ancho = (-formul.Size.Width + contenedor1.Width) / 2;
            int alto = (-formul.Size.Height + contenedor1.Height) / 2;

            formul.TopLevel = false;
            formul.Location = new Point(ancho, alto);
            this.contenedor1.Controls.Add(formul);
            this.contenedor1.Tag = formul;
            formul.Show();
        }
        private void abririnsumos(object formalma)
        {
            if (this.contenedor1.Controls.Count > 0)
            {
                this.contenedor1.Controls.RemoveAt(0);
            }

            Form formul = formalma as Form;
            int ancho = (-formul.Size.Width + contenedor1.Width) / 2;
            int alto = (-formul.Size.Height + contenedor1.Height) / 2;

            formul.TopLevel = false;
            formul.Location = new Point(ancho, alto);
            this.contenedor1.Controls.Add(formul);
            this.contenedor1.Tag = formul;
            formul.Show();
        }

        private void abrirproductos(object formalma)
        {
            if (this.contenedor1.Controls.Count > 0)
            {
                this.contenedor1.Controls.RemoveAt(0);
            }

            Form formul = formalma as Form;
            int ancho = (-formul.Size.Width + contenedor1.Width) / 2;
            int alto = (-formul.Size.Height + contenedor1.Height) / 2;

            formul.TopLevel = false;
            formul.Location = new Point(ancho, alto);
            this.contenedor1.Controls.Add(formul);
            this.contenedor1.Tag = formul;
            formul.Show();
        }

        private void PROVEEDOR_Click(object sender, EventArgs e)
        {
           abrirproveedores(new Info_provee()); 
        }

        private void INSUMOS_Click(object sender, EventArgs e)
        {
            abririnsumos(new Info_insumos());
        }

        private void PRODUCTO_Click(object sender, EventArgs e)
        {
            abrirproductos(new Info_producto());
        }
    }
}
