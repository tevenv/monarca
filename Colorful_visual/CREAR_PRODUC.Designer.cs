﻿namespace Colorful_visual
{
    partial class CREAR_PRODUC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ESTADO = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.GUARDAR = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.BUSCAR = new System.Windows.Forms.Button();
            this.BUSCA = new System.Windows.Forms.TextBox();
            this.prod = new System.Windows.Forms.Label();
            this.MUESTRA = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.NOMBRE = new System.Windows.Forms.TextBox();
            this.CODIGO = new System.Windows.Forms.TextBox();
            this.FICHA = new System.Windows.Forms.ComboBox();
            this.ERROR = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ERROR)).BeginInit();
            this.SuspendLayout();
            // 
            // ESTADO
            // 
            this.ESTADO.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ESTADO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ESTADO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ESTADO.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ESTADO.FormattingEnabled = true;
            this.ESTADO.Items.AddRange(new object[] {
            "Activo",
            "Inactivo"});
            this.ESTADO.Location = new System.Drawing.Point(354, 259);
            this.ESTADO.Name = "ESTADO";
            this.ESTADO.Size = new System.Drawing.Size(154, 23);
            this.ESTADO.TabIndex = 75;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(32, 154);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 14);
            this.label7.TabIndex = 73;
            this.label7.Text = "Ficha tecnica";
            // 
            // GUARDAR
            // 
            this.GUARDAR.BackColor = System.Drawing.Color.Transparent;
            this.GUARDAR.BackgroundImage = global::Colorful_visual.Properties.Resources.GUARDAR;
            this.GUARDAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.GUARDAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.GUARDAR.FlatAppearance.BorderSize = 0;
            this.GUARDAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GUARDAR.Location = new System.Drawing.Point(124, 229);
            this.GUARDAR.Name = "GUARDAR";
            this.GUARDAR.Size = new System.Drawing.Size(95, 32);
            this.GUARDAR.TabIndex = 71;
            this.GUARDAR.UseVisualStyleBackColor = false;
            this.GUARDAR.Click += new System.EventHandler(this.GUARDAR_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(308, 262);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 14);
            this.label6.TabIndex = 70;
            this.label6.Text = "Estado";
            // 
            // BUSCAR
            // 
            this.BUSCAR.BackColor = System.Drawing.Color.Transparent;
            this.BUSCAR.BackgroundImage = global::Colorful_visual.Properties.Resources.buscar;
            this.BUSCAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BUSCAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BUSCAR.FlatAppearance.BorderSize = 0;
            this.BUSCAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BUSCAR.Location = new System.Drawing.Point(486, 31);
            this.BUSCAR.Name = "BUSCAR";
            this.BUSCAR.Size = new System.Drawing.Size(20, 20);
            this.BUSCAR.TabIndex = 69;
            this.BUSCAR.UseVisualStyleBackColor = false;
            this.BUSCAR.Click += new System.EventHandler(this.BUSCAR_Click);
            // 
            // BUSCA
            // 
            this.BUSCA.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BUSCA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.BUSCA.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BUSCA.Location = new System.Drawing.Point(386, 31);
            this.BUSCA.Multiline = true;
            this.BUSCA.Name = "BUSCA";
            this.BUSCA.Size = new System.Drawing.Size(94, 20);
            this.BUSCA.TabIndex = 68;
            // 
            // prod
            // 
            this.prod.AutoSize = true;
            this.prod.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prod.Location = new System.Drawing.Point(329, 34);
            this.prod.Name = "prod";
            this.prod.Size = new System.Drawing.Size(57, 14);
            this.prod.TabIndex = 67;
            this.prod.Text = "Producto";
            // 
            // MUESTRA
            // 
            this.MUESTRA.BackColor = System.Drawing.Color.WhiteSmoke;
            this.MUESTRA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MUESTRA.Font = new System.Drawing.Font("Raleway Medium", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MUESTRA.FormattingEnabled = true;
            this.MUESTRA.ItemHeight = 15;
            this.MUESTRA.Location = new System.Drawing.Point(334, 64);
            this.MUESTRA.Name = "MUESTRA";
            this.MUESTRA.Size = new System.Drawing.Size(174, 150);
            this.MUESTRA.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(32, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 14);
            this.label2.TabIndex = 64;
            this.label2.Text = "Nombre";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(32, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 14);
            this.label1.TabIndex = 63;
            this.label1.Text = "Codigo";
            // 
            // NOMBRE
            // 
            this.NOMBRE.BackColor = System.Drawing.Color.WhiteSmoke;
            this.NOMBRE.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NOMBRE.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NOMBRE.Location = new System.Drawing.Point(120, 112);
            this.NOMBRE.Multiline = true;
            this.NOMBRE.Name = "NOMBRE";
            this.NOMBRE.Size = new System.Drawing.Size(166, 20);
            this.NOMBRE.TabIndex = 72;
            // 
            // CODIGO
            // 
            this.CODIGO.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CODIGO.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CODIGO.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CODIGO.Location = new System.Drawing.Point(120, 68);
            this.CODIGO.Multiline = true;
            this.CODIGO.Name = "CODIGO";
            this.CODIGO.Size = new System.Drawing.Size(166, 20);
            this.CODIGO.TabIndex = 65;
            // 
            // FICHA
            // 
            this.FICHA.BackColor = System.Drawing.Color.WhiteSmoke;
            this.FICHA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FICHA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FICHA.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FICHA.FormattingEnabled = true;
            this.FICHA.Location = new System.Drawing.Point(120, 154);
            this.FICHA.Name = "FICHA";
            this.FICHA.Size = new System.Drawing.Size(166, 23);
            this.FICHA.TabIndex = 76;
            // 
            // ERROR
            // 
            this.ERROR.ContainerControl = this;
            // 
            // CREAR_PRODUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(558, 328);
            this.Controls.Add(this.MUESTRA);
            this.Controls.Add(this.FICHA);
            this.Controls.Add(this.ESTADO);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.NOMBRE);
            this.Controls.Add(this.GUARDAR);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.BUSCAR);
            this.Controls.Add(this.BUSCA);
            this.Controls.Add(this.prod);
            this.Controls.Add(this.CODIGO);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CREAR_PRODUC";
            this.Text = "CREAR_PRODUC";
            this.Load += new System.EventHandler(this.inicipermiso);
            ((System.ComponentModel.ISupportInitialize)(this.ERROR)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ESTADO;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button GUARDAR;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button BUSCAR;
        private System.Windows.Forms.TextBox BUSCA;
        private System.Windows.Forms.Label prod;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox MUESTRA;
        private System.Windows.Forms.TextBox NOMBRE;
        private System.Windows.Forms.TextBox CODIGO;
        private System.Windows.Forms.ComboBox FICHA;
        private System.Windows.Forms.ErrorProvider ERROR;
    }
}