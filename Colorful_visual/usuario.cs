﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Colorful_visual;
using Colorfull;

namespace ColorFul_visual
{
    public partial class usuario : Form
    {
        usuarios obj = new usuarios();
        validador val = new validador();
        int idusuario;
        public usuario(int idusuario)
        {
            this.idusuario = idusuario;
            InitializeComponent();
            MODIFICAR.Enabled = false;
            ACTUALIZAR.Enabled = false;
        }

        private void GUARDAR_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(NOMBRE.Text))
            {
                ERROR.SetError(NOMBRE,"campo requerido");
            }
            else
            {
                if (string.IsNullOrEmpty(APELLIDO.Text))
                {
                    ERROR.SetError(APELLIDO, "campo requerido");
                }
                else
                {
                    if (val.validarNumeroIdentificacion(CC.Text))
                    {
                        if (val.validarNumeroCelular(TELEFONO.Text))
                        {
                            if (string.IsNullOrEmpty(USUA.Text))
                            {
                                ERROR.SetError(USUA, "Campo requerido, este debera ser unico");
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(CONTRA.Text))
                                {
                                    ERROR.SetError(CONTRA, "Campo requerido, este debera ser un campo secreto que solo el usuario debe conocer y el administrador si asi se desea ");
                                }
                                else
                                {
                                    if (ESTADO.SelectedIndex>=0)
                                    {
                                        obj.Nombre = NOMBRE.Text;
                                        obj.Apellido = APELLIDO.Text;
                                        obj.Id = CC.Text;
                                        obj.Telefono = TELEFONO.Text;
                                        obj.Usuario = USUA.Text;
                                        obj.Contraseña = CONTRA.Text;
                                        obj.Estado = (ESTADO.Text.Equals("Activo")) ? 1 : 0;
                                        obj.Usuario2 = idusuario;
                                        MUESTRA.Items.Clear();
                                        try {
                                            if (obj.insusuar() >= 1)
                                            {
                                                DataTable tabla = obj.consusua(USUA.Text);

                                                for (int i = 0; i < tabla.Columns.Count; i++)
                                                {
                                                    MUESTRA.Items.Add(tabla.Rows[0][i].ToString());
                                                    MUESTRA.Items.Add(" ");

                                                }
                                            }
                                            else
                                            {
                                                if (obj.insusuar() == -2)
                                                {
                                                    MessageBox.Show("El usuario ya existe, revisa en la lista de usuarios con el Usuario o el Nombre para poder obtener mas información ");
                                                }
                                                else
                                                {
                                                    MessageBox.Show("Se presento un error inesperado, revise que los datos esten completos.");
                                                }
                                            }
                                        }
                                        catch(Exception ex)
                                        {
                                            MessageBox.Show("Se a presentado un error inesperado, llamar al apoyo tecnico, error numero" + ex);
                                        }
                                        this.limpiar(this);
                                        
                                    }
                                    else
                                    {
                                        MessageBox.Show("Seleccione un estado por favor");
                                    }
                                }
                            }
                        }
                        else
                        {
                            ERROR.SetError(TELEFONO, "Valor requerido, ingrese un numero de valido, si el numero es de un telefo " +
                            "fijo recomendamos poner 03 + el indicativo de la ciudad + el numero de telefono");
                        }
                    }
                    else
                    {
                        ERROR.SetError(CC, "Campo requerido, ingrese el numero de identificacion si fue expedida antes del 2003 este ebera ser un numero de 8 digitos, si fue expedida despues sera de 10 digitos");
                    }
                }


            }


        }

        private void limpiar(Control aux)
        {
            foreach (var txt in aux.Controls)
            {
                if (txt is TextBox)
                {
                    ((TextBox)txt).Clear();

                }
                else if (txt is ComboBox)
                {
                    ((ComboBox)txt).SelectedIndex = -1;
                    
                }

            }

            ERROR.Clear();
        }

        private void BUSCA_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(BUSCAR.Text))
            {
                MessageBox.Show("Ingresa por favor un parametro de busqueda");
            }
            else
            {
                Form formulario = new Buscador(BUSCAR.Text, MUESTRA, 5);
                this.limpiar(this);
                this.MUESTRA.Items.Clear();
                formulario.Show();

            }
           
            MODIFICAR.Enabled = true;
        }

        private void MODIFICAR_Click(object sender, EventArgs e)
        {
            NOMBRE.Text = MUESTRA.Items[0].ToString();
            APELLIDO.Text = MUESTRA.Items[2].ToString();
            CC.Text = MUESTRA.Items[4].ToString();
            TELEFONO.Text = MUESTRA.Items[6].ToString();
            USUA.Text = MUESTRA.Items[8].ToString();
            CONTRA.Text = MUESTRA.Items[10].ToString();
            if (MUESTRA.Items[12].Equals("True"))
            {
                ESTADO.SelectedIndex = 0;
            }
            else if ((MUESTRA.Items[12].Equals("False")))
            {
                ESTADO.SelectedIndex = 1;
            }

            TELEFONO.Focus();
            TELEFONO.SelectionLength = 0;

            NOMBRE.Enabled = false;
            APELLIDO.Enabled = false;
            CC.Enabled = false;
            USUA.Enabled = false;
            ACTUALIZAR.Enabled = true;
            MODIFICAR.Enabled = false;
            GUARDAR.Enabled = false;


        }

        private void ACTUALIZAR_Click(object sender, EventArgs e)
        {
            obj.Telefono = TELEFONO.Text;
            obj.Contraseña = CONTRA.Text;
            obj.Usuario2 = idusuario;
            obj.Estado = (ESTADO.Text.Equals("Activo")) ? 1 : 0;
            obj.Usuario = USUA.Text;
            MUESTRA.Items.Clear();
            try
            {
                if (obj.actualizarusuario() >= 1)
                {
                    DataTable tabla = obj.consusua(USUA.Text);

                    for (int i = 0; i < tabla.Columns.Count; i++)
                    {
                        MUESTRA.Items.Add(tabla.Rows[0][i].ToString());
                        MUESTRA.Items.Add(" ");

                    }
                }
                else
                {
                    MessageBox.Show("Se presento un error inesperado, revise que los datos esten completos.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se a presentado un error inesperado, llamar al apoyo tecnico, error numero" + ex);
            }
            this.limpiar(this);
            NOMBRE.Enabled = true;
            APELLIDO.Enabled = true;
            CC.Enabled = true;
            USUA.Enabled = true;
            MODIFICAR.Enabled = true;
            GUARDAR.Enabled = true;
            ACTUALIZAR.Enabled = false;
            NOMBRE.Focus();
        }

        private void iniciarpermi(object sender, EventArgs e)
        {
            DataTable permi = obj.asignarpermi("USUARIOS",idusuario);


            GUARDAR.Visible = permi.Rows[0][0].Equals(true) ? true : false;
            MODIFICAR.Visible = permi.Rows[0][1].Equals(true) ? true : false;
            ACTUALIZAR.Visible = permi.Rows[0][1].Equals(true) ? true : false;
            BUSCA.Visible = permi.Rows[0][2].Equals(true) ? true : false;
            BUSCAR.Visible = permi.Rows[0][2].Equals(true) ? true : false;
            uss.Visible = permi.Rows[0][2].Equals(true) ? true : false;

        }

        
    }
}
