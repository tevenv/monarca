﻿namespace Colorful_visual
{
    partial class facturacion_cli
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Cedula = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.GUARDAR = new System.Windows.Forms.Button();
            this.BUSCAR = new System.Windows.Forms.Button();
            this.Apellido = new System.Windows.Forms.TextBox();
            this.NOMFICH = new System.Windows.Forms.Label();
            this.Nombre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Direccion = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Telefono = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Cedula
            // 
            this.Cedula.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Cedula.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Cedula.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cedula.Location = new System.Drawing.Point(113, 73);
            this.Cedula.Multiline = true;
            this.Cedula.Name = "Cedula";
            this.Cedula.Size = new System.Drawing.Size(149, 20);
            this.Cedula.TabIndex = 30;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(55, 73);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 17);
            this.label7.TabIndex = 29;
            this.label7.Text = "Cedula";
            // 
            // GUARDAR
            // 
            this.GUARDAR.BackColor = System.Drawing.Color.Transparent;
            this.GUARDAR.BackgroundImage = global::Colorful_visual.Properties.Resources.GUARDAR;
            this.GUARDAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.GUARDAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.GUARDAR.FlatAppearance.BorderSize = 0;
            this.GUARDAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GUARDAR.Location = new System.Drawing.Point(239, 264);
            this.GUARDAR.Name = "GUARDAR";
            this.GUARDAR.Size = new System.Drawing.Size(95, 32);
            this.GUARDAR.TabIndex = 27;
            this.GUARDAR.UseVisualStyleBackColor = false;
            this.GUARDAR.Click += new System.EventHandler(this.GUARDAR_Click);
            // 
            // BUSCAR
            // 
            this.BUSCAR.BackColor = System.Drawing.Color.Transparent;
            this.BUSCAR.BackgroundImage = global::Colorful_visual.Properties.Resources.buscar;
            this.BUSCAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BUSCAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BUSCAR.FlatAppearance.BorderSize = 0;
            this.BUSCAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BUSCAR.Location = new System.Drawing.Point(285, 73);
            this.BUSCAR.Name = "BUSCAR";
            this.BUSCAR.Size = new System.Drawing.Size(20, 20);
            this.BUSCAR.TabIndex = 25;
            this.BUSCAR.UseVisualStyleBackColor = false;
            // 
            // Apellido
            // 
            this.Apellido.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Apellido.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Apellido.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Apellido.Location = new System.Drawing.Point(355, 123);
            this.Apellido.Multiline = true;
            this.Apellido.Name = "Apellido";
            this.Apellido.Size = new System.Drawing.Size(149, 20);
            this.Apellido.TabIndex = 24;
            // 
            // NOMFICH
            // 
            this.NOMFICH.AutoSize = true;
            this.NOMFICH.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NOMFICH.Location = new System.Drawing.Point(291, 126);
            this.NOMFICH.Name = "NOMFICH";
            this.NOMFICH.Size = new System.Drawing.Size(52, 17);
            this.NOMFICH.TabIndex = 23;
            this.NOMFICH.Text = "Apellido";
            // 
            // Nombre
            // 
            this.Nombre.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Nombre.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Nombre.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nombre.Location = new System.Drawing.Point(113, 123);
            this.Nombre.Multiline = true;
            this.Nombre.Name = "Nombre";
            this.Nombre.Size = new System.Drawing.Size(149, 20);
            this.Nombre.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(55, 123);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 17);
            this.label1.TabIndex = 20;
            this.label1.Text = "Nombre";
            // 
            // Direccion
            // 
            this.Direccion.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Direccion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Direccion.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Direccion.Location = new System.Drawing.Point(355, 179);
            this.Direccion.Multiline = true;
            this.Direccion.Name = "Direccion";
            this.Direccion.Size = new System.Drawing.Size(149, 20);
            this.Direccion.TabIndex = 34;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(291, 182);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 17);
            this.label2.TabIndex = 33;
            this.label2.Text = "Direccion";
            // 
            // Telefono
            // 
            this.Telefono.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Telefono.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Telefono.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Telefono.Location = new System.Drawing.Point(113, 179);
            this.Telefono.Multiline = true;
            this.Telefono.Name = "Telefono";
            this.Telefono.Size = new System.Drawing.Size(149, 20);
            this.Telefono.TabIndex = 32;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(55, 179);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 17);
            this.label3.TabIndex = 31;
            this.label3.Text = "Telefono";
            // 
            // facturacion_cli
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(558, 328);
            this.Controls.Add(this.Direccion);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Telefono);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Cedula);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.GUARDAR);
            this.Controls.Add(this.BUSCAR);
            this.Controls.Add(this.Apellido);
            this.Controls.Add(this.NOMFICH);
            this.Controls.Add(this.Nombre);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "facturacion_cli";
            this.Text = "facturacion";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox Cedula;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button GUARDAR;
        private System.Windows.Forms.Button BUSCAR;
        private System.Windows.Forms.TextBox Apellido;
        private System.Windows.Forms.Label NOMFICH;
        private System.Windows.Forms.TextBox Nombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Direccion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Telefono;
        private System.Windows.Forms.Label label3;
    }
}