﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Colorfull;
using SpreadsheetLight;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;

namespace Colorful_visual
{
    public partial class Info_provee : Form
    {
        informes obj = new informes();
        public Info_provee()
        {
            InitializeComponent();
            exportexcel.Enabled = false;
        }

        private void Consultar_Click(object sender, EventArgs e)
        {
            int estado = 0;
            
            if (Estado.Text == "Todos")
            {
                tablainfo.DataSource = obj.allproveedores();
            }
            else
            {
                estado = Estado.Text=="Activos" ? 1 : 0;
                tablainfo.DataSource = obj.proveedores(estado);
            }

            exportexcel.Enabled = true;
        }

        private void Exportexcel_Click(object sender, EventArgs e)
        {
           
            SLDocument exportar = new SLDocument();
            SLStyle estilo = new SLStyle();
                       

            estilo.Font.FontSize = 12;
            estilo.Font.Bold = true;
            estilo.Alignment.Horizontal = HorizontalAlignmentValues.Center;
            estilo.Border.BottomBorder.BorderStyle = BorderStyleValues.Thin;
            
                              
            
           int cont = 3;
             foreach(DataGridViewColumn columna in tablainfo.Columns)
             {
                  exportar.SetCellValue(3, cont, columna.HeaderText.ToString());
                  exportar.SetCellStyle(3, cont, estilo);
                  cont++;
             }

            int cont1 = 4, cont2 = 3;
           foreach(DataGridViewRow fila in tablainfo.Rows)
           {
                for (int j = 0; j < tablainfo.Columns.Count; j++)
                {
                    exportar.SetCellValue(cont1, cont2, fila.Cells[j].Value.ToString());
                    cont2++;
                }
                cont1++;
                cont2 = 3;
                
           }

       
            try
            {
                if (guardarcomo.ShowDialog() == DialogResult.OK)
                {
                    exportar.SaveAs(guardarcomo.FileName);
                    guardarcomo.FileName = "Informe proveedor";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("La hoja de calculo que deseea reemplazar se encuentra activa, por favor cierre la hoja para continuar con la operacion");
            }
            
        }
    }
}
