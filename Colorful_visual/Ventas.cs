﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Colorful_visual
{
    public partial class Ventas : Form
    {
        public Ventas()
        {
            InitializeComponent();
        }

        private void abrirHijo(Form formularioHijo)
        {
            if (this.Contenedor3.Controls.Count > 0)
            {
                this.Contenedor3.Controls.RemoveAt(0);
            }

            int ancho = (-formularioHijo.Size.Width + Contenedor3.Width) / 2;
            int alto = (-formularioHijo.Size.Height + Contenedor3.Height) / 2;

            formularioHijo.TopLevel = false;
            formularioHijo.Location = new Point(ancho, alto);
            this.Contenedor3.Controls.Add(formularioHijo);
            this.Contenedor3.Tag = formularioHijo;
            formularioHijo.Show();
        }

        private void Clientes_Click(object sender, EventArgs e)
        {
            abrirHijo(new clientes());
        }

        private void PRODUCTOS_Click(object sender, EventArgs e)
        {
            abrirHijo(new stock() );
        }

        private void Facturacion_Click(object sender, EventArgs e)
        {
            abrirHijo(new facturacion_cli(this.Contenedor3));
        }
    }
}
