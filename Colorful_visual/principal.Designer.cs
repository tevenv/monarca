﻿namespace Colorful_visual
{
    partial class principal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.contenedor1 = new System.Windows.Forms.Panel();
            this.Superior = new System.Windows.Forms.Panel();
            this.central1 = new System.Windows.Forms.Panel();
            this.Ventas = new System.Windows.Forms.Button();
            this.TALLER = new System.Windows.Forms.Button();
            this.INFO = new System.Windows.Forms.Button();
            this.ADMIN = new System.Windows.Forms.Button();
            this.slogan = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.MINI = new System.Windows.Forms.Button();
            this.CERRAR = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.logo = new System.Windows.Forms.PictureBox();
            this.Superior.SuspendLayout();
            this.central1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).BeginInit();
            this.SuspendLayout();
            // 
            // contenedor1
            // 
            this.contenedor1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.contenedor1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contenedor1.Location = new System.Drawing.Point(0, 107);
            this.contenedor1.Name = "contenedor1";
            this.contenedor1.Size = new System.Drawing.Size(981, 343);
            this.contenedor1.TabIndex = 3;
            // 
            // Superior
            // 
            this.Superior.BackColor = System.Drawing.Color.Transparent;
            this.Superior.BackgroundImage = global::Colorful_visual.Properties.Resources.morao;
            this.Superior.Controls.Add(this.central1);
            this.Superior.Controls.Add(this.slogan);
            this.Superior.Controls.Add(this.panel2);
            this.Superior.Controls.Add(this.panel3);
            this.Superior.Dock = System.Windows.Forms.DockStyle.Top;
            this.Superior.Location = new System.Drawing.Point(0, 0);
            this.Superior.Name = "Superior";
            this.Superior.Size = new System.Drawing.Size(981, 107);
            this.Superior.TabIndex = 2;
            // 
            // central1
            // 
            this.central1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.central1.Controls.Add(this.Ventas);
            this.central1.Controls.Add(this.TALLER);
            this.central1.Controls.Add(this.INFO);
            this.central1.Controls.Add(this.ADMIN);
            this.central1.Location = new System.Drawing.Point(158, 12);
            this.central1.Name = "central1";
            this.central1.Size = new System.Drawing.Size(557, 73);
            this.central1.TabIndex = 2;
            // 
            // Ventas
            // 
            this.Ventas.BackColor = System.Drawing.Color.Transparent;
            this.Ventas.BackgroundImage = global::Colorful_visual.Properties.Resources.VENTAS;
            this.Ventas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Ventas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Ventas.FlatAppearance.BorderSize = 0;
            this.Ventas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Ventas.Location = new System.Drawing.Point(310, 8);
            this.Ventas.Name = "Ventas";
            this.Ventas.Size = new System.Drawing.Size(83, 56);
            this.Ventas.TabIndex = 3;
            this.Ventas.UseVisualStyleBackColor = false;
            this.Ventas.Click += new System.EventHandler(this.Ventas_Click);
            // 
            // TALLER
            // 
            this.TALLER.BackColor = System.Drawing.Color.Transparent;
            this.TALLER.BackgroundImage = global::Colorful_visual.Properties.Resources.ALMACEN;
            this.TALLER.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.TALLER.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TALLER.FlatAppearance.BorderSize = 0;
            this.TALLER.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TALLER.Location = new System.Drawing.Point(179, 8);
            this.TALLER.Name = "TALLER";
            this.TALLER.Size = new System.Drawing.Size(83, 56);
            this.TALLER.TabIndex = 2;
            this.TALLER.UseVisualStyleBackColor = false;
            this.TALLER.Click += new System.EventHandler(this.TALLER_Click);
            // 
            // INFO
            // 
            this.INFO.BackColor = System.Drawing.Color.Transparent;
            this.INFO.BackgroundImage = global::Colorful_visual.Properties.Resources.INFORMES;
            this.INFO.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.INFO.Cursor = System.Windows.Forms.Cursors.Hand;
            this.INFO.FlatAppearance.BorderSize = 0;
            this.INFO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.INFO.Location = new System.Drawing.Point(437, 8);
            this.INFO.Name = "INFO";
            this.INFO.Size = new System.Drawing.Size(83, 56);
            this.INFO.TabIndex = 1;
            this.INFO.UseVisualStyleBackColor = false;
            this.INFO.Click += new System.EventHandler(this.INFO_Click);
            // 
            // ADMIN
            // 
            this.ADMIN.BackColor = System.Drawing.Color.Transparent;
            this.ADMIN.BackgroundImage = global::Colorful_visual.Properties.Resources.boton_admin;
            this.ADMIN.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ADMIN.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ADMIN.FlatAppearance.BorderSize = 0;
            this.ADMIN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ADMIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ADMIN.Location = new System.Drawing.Point(39, 8);
            this.ADMIN.Name = "ADMIN";
            this.ADMIN.Size = new System.Drawing.Size(83, 56);
            this.ADMIN.TabIndex = 0;
            this.ADMIN.UseVisualStyleBackColor = false;
            this.ADMIN.Click += new System.EventHandler(this.ADMIN_Click);
            // 
            // slogan
            // 
            this.slogan.AutoSize = true;
            this.slogan.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slogan.Location = new System.Drawing.Point(721, 27);
            this.slogan.Name = "slogan";
            this.slogan.Size = new System.Drawing.Size(141, 42);
            this.slogan.TabIndex = 3;
            this.slogan.Text = "Slogan";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.MINI);
            this.panel2.Controls.Add(this.CERRAR);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(874, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(107, 107);
            this.panel2.TabIndex = 0;
            // 
            // MINI
            // 
            this.MINI.BackColor = System.Drawing.Color.Transparent;
            this.MINI.BackgroundImage = global::Colorful_visual.Properties.Resources.minimizar1;
            this.MINI.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MINI.FlatAppearance.BorderSize = 0;
            this.MINI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MINI.Location = new System.Drawing.Point(9, 8);
            this.MINI.Name = "MINI";
            this.MINI.Size = new System.Drawing.Size(40, 40);
            this.MINI.TabIndex = 3;
            this.MINI.UseVisualStyleBackColor = false;
            this.MINI.Click += new System.EventHandler(this.MINI_Click);
            // 
            // CERRAR
            // 
            this.CERRAR.BackgroundImage = global::Colorful_visual.Properties.Resources.cerrar1;
            this.CERRAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CERRAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CERRAR.FlatAppearance.BorderSize = 0;
            this.CERRAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CERRAR.Location = new System.Drawing.Point(55, 8);
            this.CERRAR.Name = "CERRAR";
            this.CERRAR.Size = new System.Drawing.Size(40, 40);
            this.CERRAR.TabIndex = 2;
            this.CERRAR.UseVisualStyleBackColor = false;
            this.CERRAR.Click += new System.EventHandler(this.CERRAR_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.logo);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(138, 107);
            this.panel3.TabIndex = 1;
            // 
            // logo
            // 
            this.logo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.logo.Location = new System.Drawing.Point(19, 7);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(90, 90);
            this.logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.logo.TabIndex = 0;
            this.logo.TabStop = false;
            // 
            // principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(981, 450);
            this.ControlBox = false;
            this.Controls.Add(this.contenedor1);
            this.Controls.Add(this.Superior);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "principal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.TransparencyKey = System.Drawing.Color.Maroon;
            this.Load += new System.EventHandler(this.iniciopermi);
            this.Superior.ResumeLayout(false);
            this.Superior.PerformLayout();
            this.central1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.logo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox logo;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel Superior;
        private System.Windows.Forms.Label slogan;
        private System.Windows.Forms.Panel central1;
        private System.Windows.Forms.Button TALLER;
        private System.Windows.Forms.Button INFO;
        private System.Windows.Forms.Button ADMIN;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button MINI;
        private System.Windows.Forms.Button CERRAR;
        private System.Windows.Forms.Panel contenedor1;
        private System.Windows.Forms.Button Ventas;
    }
}

