﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Colorfull;

namespace Colorful_visual
{
    public partial class MODIFICAR_FICHA : Form
    {
        Ficha obj = new Ficha();
        int aux;
        int idusuario;

        public MODIFICAR_FICHA(int idusuario)
        {
            this.idusuario = idusuario;
            InitializeComponent();
           
        }

        private void BUSCAR_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(BUSCA.Text))
            {
                MessageBox.Show("Ingresa por favor un parametro de busqueda");
            }
            else
            {
                Form formulario = new Buscador(BUSCA.Text, MUESTRA, 3);
                this.BUSCA.Clear();
                this.MUESTRA.Items.Clear();
                formulario.Show();

            }
        }

        private void MODIFICAR_Click(object sender, EventArgs e)
        {
            int i = 0;
            while (MUESTRA.Items.Count!=0)
            {
                if(MUESTRA.Items[i].Equals("True") || MUESTRA.Items[i].Equals("False"))
                {
                    aux = i;
                    break;
                }
                i++;
            }

            if (MUESTRA.Items[aux].Equals("True"))
            {
                ESTADO.SelectedIndex = 0;
            }
            if (MUESTRA.Items[aux].Equals("False"))
            {
                ESTADO.SelectedIndex = 1;
            }
        }

        private void ACTUALIZAR_Click(object sender, EventArgs e)
        {
            int estado = 0;
            if (MUESTRA.Items[aux].Equals("True"))
            {
                 estado = 1;
            }
            if (MUESTRA.Items[aux].Equals("False"))
            {
                estado = 0;
            }

            DataTable tabla = obj.usuarioftc(MUESTRA.Items[0].ToString());
            int usuario = int.Parse(tabla.Rows[0][0].ToString());

            obj.Estado =  (ESTADO.Text.Equals("Activo")) ? 1 : 0;
            obj.Usuario = idusuario;
            obj.Ficha1 = MUESTRA.Items[0].ToString();
            string x = MUESTRA.Items[0].ToString();
            this.MUESTRA.Items.Clear();
            try
            {
                if (obj.actualizarestado(estado, usuario) >= 1 && obj.almacenar("ACT",0) >= 1  )
                {
                    tabla = obj.fichatec(x);
                    for (int i = 1; i < tabla.Columns.Count; i++)
                    {
                        this.MUESTRA.Items.Add(tabla.Rows[0][i].ToString());
                        this.MUESTRA.Items.Add(" ");
                        if (i == 2)
                        {
                            for (int j = 0; j < tabla.Rows.Count; j++)
                            {
                                this.MUESTRA.Items.Add(tabla.Rows[j][3].ToString() + " cantidad: " + tabla.Rows[j][4].ToString());
                                this.MUESTRA.Items.Add(" ");
                            }
                            i = 4;
                        }

                    }
                }
                else
                {
                    MessageBox.Show("Se presento un error inesperado, revise que los datos esten completos.");
                }

                this.limpiar(this);

            }
            catch(Exception ex)
            {
                MessageBox.Show("Se a presentado un error inesperado, llamar al apoyo tecnico, error: " + ex);
            }
           

        }

        private void limpiar(Control aux)
        {
            foreach (var txt in aux.Controls)
            {
                if (txt is TextBox)
                {
                    ((TextBox)txt).Clear();

                }
                else if (txt is ComboBox)
                {
                    ((ComboBox)txt).SelectedIndex = -1;
                }

            }

        }
    }
}
