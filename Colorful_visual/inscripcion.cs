﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Colorfull;
using System.Security.Cryptography;
using Microsoft.VisualBasic.Devices;
using System.IO;

namespace Colorful_visual
{
    public partial class inscripcion : Form
    {

        Inscripcion ins = new Inscripcion();
        usuarios usua = new usuarios();
        Permisos per = new Permisos();
        string direccion1;

        public inscripcion()
        {
            InitializeComponent();
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;

            Size pantalla = System.Windows.Forms.SystemInformation.PrimaryMonitorSize;
            Int32 Alto = (pantalla.Height - EMPRESA.Height) / 2;
            Int32 ancho = (pantalla.Width - EMPRESA.Width) / 2;
            EMPRESA.Location = new Point(ancho, Alto);
        }

        private void CERRAR_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MINI_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void Guardar_Click(object sender, EventArgs e)
        {
            //insertarempresa
            ins.Empresa = nomempre.Text;
            ins.NIT1 = nitempre.Text;
            ins.Telefono = telempre.Text;
            ins.Direccion = direccionempre.Text;
            ins.Slogan = sloganempre.Text;
            ins.Logo = guardarim();

            ins.Nombre = nomadmin.Text;
            ins.Apellido = apellidoadmin.Text;
            ins.Identificacion = identifadmin.Text;
            ins.Telefonoad = teleadmin.Text;
            ins.Usuario = usuaadmin.Text;
            ins.Contraseña = contraadmin.Text;

            //insertar usuario
            usua.Nombre = nomadmin.Text;
            usua.Apellido = apellidoadmin.Text;
            usua.Id = identifadmin.Text;
            usua.Telefono = teleadmin.Text;
            usua.Usuario = usuaadmin.Text;
            usua.Contraseña = contraadmin.Text;
            usua.Usuario2 = 1;
            usua.Estado = 1;

            try
            {
                if (ins.insertempresa() >= 1 && usua.insusuar() >= 1 && this.permisos() >= 1)
                {
                    MessageBox.Show("Se a inscrito satisfactoriamente, inicie sesión con su usuario y contraseña registrada");
                    Form formulario = new inicio();
                    formulario.Show();
                    this.Visible = false;
                }
                else
                {
                    MessageBox.Show("Se presento un error, revise que los campos esten completos");

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se a presentado un error inesperado, llamar al apoyo tecnico, error numero" + ex);

            }


        }

        private int permisos()
        {
            int aux = 0;


            string[] modulo1 = { "ADMINISTRADOR", "ADMINISTRADOR", "TALLER", "TALLER", "TALLER", "TALLER" };
            string[] modulo2 = { "USUARIOS", "PERMISOS", "PROVEEDORES", "INSUMOS", "FICHA TECNICA", "PRODUCTOS" };
            int[] insertar = { 0, 0, 0, 1, 0, 1 };

            per.Crear = 1;
            per.Modificar = 1;
            per.Consultar = 1;
            per.Usuario = 5;
            per.Usuario2 = 1;

            foreach (string mol1 in modulo1)
            {
                per.Modulo1 = mol1;
                per.Modulo2 = modulo2[aux];
                per.Insertar = insertar[aux];

                try
                {
                    if (per.inspermi() >= 1)
                    {
                        aux++;
                    }
                    else
                    {
                        MessageBox.Show("Se presento un error, revise que los campos esten completos");
                        break;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Se a presentado un error inesperado, llamar al apoyo tecnico, error numero" + ex);
                    break;
                }
            }


            return aux;
        }

        private void Subir_Click(object sender, EventArgs e)
        {
            if (cargar.ShowDialog() == DialogResult.OK)
            {
                Logo.Image = Image.FromFile(cargar.FileName);
                Logo.BackColor = Color.Transparent;
                direccion1 = cargar.FileName;
            }
        }

        private string guardarim()
        {
            Computer carga = new Computer();
            string guardar = @"c:\Monarca";
            string direccion2 = guardar + direccion1.Substring(direccion1.LastIndexOf(@"\"));

            Directory.CreateDirectory(guardar);
            carga.FileSystem.CopyFile(direccion1, direccion2);

            return direccion2; 
        }
    }
}
