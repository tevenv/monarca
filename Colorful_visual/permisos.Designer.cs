﻿namespace Colorful_visual
{
    partial class permisos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.permi = new System.Windows.Forms.Label();
            this.consulpermi = new System.Windows.Forms.CheckBox();
            this.modifipermi = new System.Windows.Forms.CheckBox();
            this.crearpermi = new System.Windows.Forms.CheckBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.consulusu = new System.Windows.Forms.CheckBox();
            this.modifiusu = new System.Windows.Forms.CheckBox();
            this.crearusu = new System.Windows.Forms.CheckBox();
            this.usuarios = new System.Windows.Forms.Label();
            this.administrador = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.PRODUCTOS = new System.Windows.Forms.Label();
            this.consulproduct = new System.Windows.Forms.CheckBox();
            this.modifiproduct = new System.Windows.Forms.CheckBox();
            this.crearproduc = new System.Windows.Forms.CheckBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.FICHATECNICA = new System.Windows.Forms.Label();
            this.consulficha = new System.Windows.Forms.CheckBox();
            this.modifificha = new System.Windows.Forms.CheckBox();
            this.crearficha = new System.Windows.Forms.CheckBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.INSUMOS = new System.Windows.Forms.Label();
            this.consulinsu = new System.Windows.Forms.CheckBox();
            this.modifiinsu = new System.Windows.Forms.CheckBox();
            this.crearinsu = new System.Windows.Forms.CheckBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.consulprov = new System.Windows.Forms.CheckBox();
            this.modifiprov = new System.Windows.Forms.CheckBox();
            this.crearprov = new System.Windows.Forms.CheckBox();
            this.proveedores = new System.Windows.Forms.Label();
            this.Taller = new System.Windows.Forms.Label();
            this.Guardar = new System.Windows.Forms.Button();
            this.ACTUALIZAR = new System.Windows.Forms.Button();
            this.USUARIO = new System.Windows.Forms.Label();
            this.buscador = new System.Windows.Forms.Button();
            this.Buscar = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.MODIFICAR = new System.Windows.Forms.Button();
            this.Inserinsu = new System.Windows.Forms.CheckBox();
            this.inserpro = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Controls.Add(this.panel1);
            this.flowLayoutPanel1.Controls.Add(this.panel2);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(117, 80);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(458, 273);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.administrador);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(430, 228);
            this.panel1.TabIndex = 3;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.permi);
            this.panel5.Controls.Add(this.consulpermi);
            this.panel5.Controls.Add(this.modifipermi);
            this.panel5.Controls.Add(this.crearpermi);
            this.panel5.Location = new System.Drawing.Point(24, 125);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(392, 72);
            this.panel5.TabIndex = 2;
            // 
            // permi
            // 
            this.permi.AutoSize = true;
            this.permi.Font = new System.Drawing.Font("Raleway Medium", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.permi.Location = new System.Drawing.Point(3, 3);
            this.permi.Name = "permi";
            this.permi.Size = new System.Drawing.Size(73, 14);
            this.permi.TabIndex = 7;
            this.permi.Text = "PERMISOS";
            // 
            // consulpermi
            // 
            this.consulpermi.AutoSize = true;
            this.consulpermi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.consulpermi.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consulpermi.Location = new System.Drawing.Point(279, 38);
            this.consulpermi.Name = "consulpermi";
            this.consulpermi.Size = new System.Drawing.Size(79, 19);
            this.consulpermi.TabIndex = 6;
            this.consulpermi.Text = "Consultar";
            this.consulpermi.UseVisualStyleBackColor = true;
            // 
            // modifipermi
            // 
            this.modifipermi.AutoSize = true;
            this.modifipermi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.modifipermi.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modifipermi.Location = new System.Drawing.Point(145, 38);
            this.modifipermi.Name = "modifipermi";
            this.modifipermi.Size = new System.Drawing.Size(77, 19);
            this.modifipermi.TabIndex = 5;
            this.modifipermi.Text = "Modificar";
            this.modifipermi.UseVisualStyleBackColor = true;
            this.modifipermi.CheckedChanged += new System.EventHandler(this.modifpermis);
            // 
            // crearpermi
            // 
            this.crearpermi.AutoSize = true;
            this.crearpermi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.crearpermi.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crearpermi.Location = new System.Drawing.Point(3, 38);
            this.crearpermi.Name = "crearpermi";
            this.crearpermi.Size = new System.Drawing.Size(55, 19);
            this.crearpermi.TabIndex = 4;
            this.crearpermi.Text = "Crear";
            this.crearpermi.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.consulusu);
            this.panel6.Controls.Add(this.modifiusu);
            this.panel6.Controls.Add(this.crearusu);
            this.panel6.Controls.Add(this.usuarios);
            this.panel6.Location = new System.Drawing.Point(24, 44);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(392, 75);
            this.panel6.TabIndex = 1;
            // 
            // consulusu
            // 
            this.consulusu.AutoSize = true;
            this.consulusu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.consulusu.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consulusu.Location = new System.Drawing.Point(279, 47);
            this.consulusu.Name = "consulusu";
            this.consulusu.Size = new System.Drawing.Size(79, 19);
            this.consulusu.TabIndex = 3;
            this.consulusu.Text = "Consultar";
            this.consulusu.UseVisualStyleBackColor = true;
            // 
            // modifiusu
            // 
            this.modifiusu.AutoSize = true;
            this.modifiusu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.modifiusu.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modifiusu.Location = new System.Drawing.Point(145, 47);
            this.modifiusu.Name = "modifiusu";
            this.modifiusu.Size = new System.Drawing.Size(77, 19);
            this.modifiusu.TabIndex = 2;
            this.modifiusu.Text = "Modificar";
            this.modifiusu.UseVisualStyleBackColor = true;
            this.modifiusu.CheckedChanged += new System.EventHandler(this.todos);
            // 
            // crearusu
            // 
            this.crearusu.AutoSize = true;
            this.crearusu.BackColor = System.Drawing.Color.WhiteSmoke;
            this.crearusu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.crearusu.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crearusu.Location = new System.Drawing.Point(6, 42);
            this.crearusu.Name = "crearusu";
            this.crearusu.Size = new System.Drawing.Size(55, 19);
            this.crearusu.TabIndex = 1;
            this.crearusu.Text = "Crear";
            this.crearusu.UseVisualStyleBackColor = false;
            // 
            // usuarios
            // 
            this.usuarios.AutoSize = true;
            this.usuarios.Font = new System.Drawing.Font("Raleway Medium", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usuarios.Location = new System.Drawing.Point(3, 4);
            this.usuarios.Name = "usuarios";
            this.usuarios.Size = new System.Drawing.Size(75, 14);
            this.usuarios.TabIndex = 0;
            this.usuarios.Text = "USUARIOS";
            // 
            // administrador
            // 
            this.administrador.AutoSize = true;
            this.administrador.Font = new System.Drawing.Font("Raleway Medium", 8.999999F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.administrador.Location = new System.Drawing.Point(4, 8);
            this.administrador.Name = "administrador";
            this.administrador.Size = new System.Drawing.Size(118, 14);
            this.administrador.TabIndex = 0;
            this.administrador.Text = "ADMINISTRADOR";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.Taller);
            this.panel2.Location = new System.Drawing.Point(3, 237);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(430, 380);
            this.panel2.TabIndex = 1;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.inserpro);
            this.panel8.Controls.Add(this.PRODUCTOS);
            this.panel8.Controls.Add(this.consulproduct);
            this.panel8.Controls.Add(this.modifiproduct);
            this.panel8.Controls.Add(this.crearproduc);
            this.panel8.Location = new System.Drawing.Point(24, 281);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(392, 72);
            this.panel8.TabIndex = 9;
            // 
            // PRODUCTOS
            // 
            this.PRODUCTOS.AutoSize = true;
            this.PRODUCTOS.Font = new System.Drawing.Font("Raleway Medium", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PRODUCTOS.Location = new System.Drawing.Point(3, 3);
            this.PRODUCTOS.Name = "PRODUCTOS";
            this.PRODUCTOS.Size = new System.Drawing.Size(89, 14);
            this.PRODUCTOS.TabIndex = 7;
            this.PRODUCTOS.Text = "PRODUCTOS";
            // 
            // consulproduct
            // 
            this.consulproduct.AutoSize = true;
            this.consulproduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.consulproduct.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consulproduct.Location = new System.Drawing.Point(279, 38);
            this.consulproduct.Name = "consulproduct";
            this.consulproduct.Size = new System.Drawing.Size(79, 19);
            this.consulproduct.TabIndex = 6;
            this.consulproduct.Text = "Consultar";
            this.consulproduct.UseVisualStyleBackColor = true;
            // 
            // modifiproduct
            // 
            this.modifiproduct.AutoSize = true;
            this.modifiproduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.modifiproduct.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modifiproduct.Location = new System.Drawing.Point(182, 38);
            this.modifiproduct.Name = "modifiproduct";
            this.modifiproduct.Size = new System.Drawing.Size(77, 19);
            this.modifiproduct.TabIndex = 5;
            this.modifiproduct.Text = "Modificar";
            this.modifiproduct.UseVisualStyleBackColor = true;
            this.modifiproduct.CheckedChanged += new System.EventHandler(this.modificarprod);
            // 
            // crearproduc
            // 
            this.crearproduc.AutoSize = true;
            this.crearproduc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.crearproduc.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crearproduc.Location = new System.Drawing.Point(3, 38);
            this.crearproduc.Name = "crearproduc";
            this.crearproduc.Size = new System.Drawing.Size(55, 19);
            this.crearproduc.TabIndex = 4;
            this.crearproduc.Text = "Crear";
            this.crearproduc.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.FICHATECNICA);
            this.panel7.Controls.Add(this.consulficha);
            this.panel7.Controls.Add(this.modifificha);
            this.panel7.Controls.Add(this.crearficha);
            this.panel7.Location = new System.Drawing.Point(24, 203);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(392, 72);
            this.panel7.TabIndex = 8;
            // 
            // FICHATECNICA
            // 
            this.FICHATECNICA.AutoSize = true;
            this.FICHATECNICA.Font = new System.Drawing.Font("Raleway Medium", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FICHATECNICA.Location = new System.Drawing.Point(3, 3);
            this.FICHATECNICA.Name = "FICHATECNICA";
            this.FICHATECNICA.Size = new System.Drawing.Size(108, 14);
            this.FICHATECNICA.TabIndex = 7;
            this.FICHATECNICA.Text = "FICHA TECNICA";
            // 
            // consulficha
            // 
            this.consulficha.AutoSize = true;
            this.consulficha.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.consulficha.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consulficha.Location = new System.Drawing.Point(279, 38);
            this.consulficha.Name = "consulficha";
            this.consulficha.Size = new System.Drawing.Size(79, 19);
            this.consulficha.TabIndex = 6;
            this.consulficha.Text = "Consultar";
            this.consulficha.UseVisualStyleBackColor = true;
            // 
            // modifificha
            // 
            this.modifificha.AutoSize = true;
            this.modifificha.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.modifificha.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modifificha.Location = new System.Drawing.Point(145, 38);
            this.modifificha.Name = "modifificha";
            this.modifificha.Size = new System.Drawing.Size(77, 19);
            this.modifificha.TabIndex = 5;
            this.modifificha.Text = "Modificar";
            this.modifificha.UseVisualStyleBackColor = true;
            this.modifificha.CheckedChanged += new System.EventHandler(this.modificarficha);
            // 
            // crearficha
            // 
            this.crearficha.AutoSize = true;
            this.crearficha.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.crearficha.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crearficha.Location = new System.Drawing.Point(3, 38);
            this.crearficha.Name = "crearficha";
            this.crearficha.Size = new System.Drawing.Size(55, 19);
            this.crearficha.TabIndex = 4;
            this.crearficha.Text = "Crear";
            this.crearficha.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.Inserinsu);
            this.panel4.Controls.Add(this.INSUMOS);
            this.panel4.Controls.Add(this.consulinsu);
            this.panel4.Controls.Add(this.modifiinsu);
            this.panel4.Controls.Add(this.crearinsu);
            this.panel4.Location = new System.Drawing.Point(24, 125);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(392, 72);
            this.panel4.TabIndex = 2;
            // 
            // INSUMOS
            // 
            this.INSUMOS.AutoSize = true;
            this.INSUMOS.Font = new System.Drawing.Font("Raleway Medium", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.INSUMOS.Location = new System.Drawing.Point(3, 3);
            this.INSUMOS.Name = "INSUMOS";
            this.INSUMOS.Size = new System.Drawing.Size(68, 14);
            this.INSUMOS.TabIndex = 7;
            this.INSUMOS.Text = "INSUMOS";
            // 
            // consulinsu
            // 
            this.consulinsu.AutoSize = true;
            this.consulinsu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.consulinsu.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consulinsu.Location = new System.Drawing.Point(279, 38);
            this.consulinsu.Name = "consulinsu";
            this.consulinsu.Size = new System.Drawing.Size(79, 19);
            this.consulinsu.TabIndex = 6;
            this.consulinsu.Text = "Consultar";
            this.consulinsu.UseVisualStyleBackColor = true;
            // 
            // modifiinsu
            // 
            this.modifiinsu.AutoSize = true;
            this.modifiinsu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.modifiinsu.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modifiinsu.Location = new System.Drawing.Point(182, 38);
            this.modifiinsu.Name = "modifiinsu";
            this.modifiinsu.Size = new System.Drawing.Size(77, 19);
            this.modifiinsu.TabIndex = 5;
            this.modifiinsu.Text = "Modificar";
            this.modifiinsu.UseVisualStyleBackColor = true;
            this.modifiinsu.CheckedChanged += new System.EventHandler(this.modificarinsu);
            // 
            // crearinsu
            // 
            this.crearinsu.AutoSize = true;
            this.crearinsu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.crearinsu.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crearinsu.Location = new System.Drawing.Point(3, 38);
            this.crearinsu.Name = "crearinsu";
            this.crearinsu.Size = new System.Drawing.Size(55, 19);
            this.crearinsu.TabIndex = 4;
            this.crearinsu.Text = "Crear";
            this.crearinsu.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.consulprov);
            this.panel3.Controls.Add(this.modifiprov);
            this.panel3.Controls.Add(this.crearprov);
            this.panel3.Controls.Add(this.proveedores);
            this.panel3.Location = new System.Drawing.Point(24, 44);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(392, 75);
            this.panel3.TabIndex = 1;
            // 
            // consulprov
            // 
            this.consulprov.AutoSize = true;
            this.consulprov.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.consulprov.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consulprov.Location = new System.Drawing.Point(279, 47);
            this.consulprov.Name = "consulprov";
            this.consulprov.Size = new System.Drawing.Size(79, 19);
            this.consulprov.TabIndex = 3;
            this.consulprov.Text = "Consultar";
            this.consulprov.UseVisualStyleBackColor = true;
            // 
            // modifiprov
            // 
            this.modifiprov.AutoSize = true;
            this.modifiprov.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.modifiprov.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modifiprov.Location = new System.Drawing.Point(145, 47);
            this.modifiprov.Name = "modifiprov";
            this.modifiprov.Size = new System.Drawing.Size(77, 19);
            this.modifiprov.TabIndex = 2;
            this.modifiprov.Text = "Modificar";
            this.modifiprov.UseVisualStyleBackColor = true;
            this.modifiprov.CheckedChanged += new System.EventHandler(this.modifiprove);
            // 
            // crearprov
            // 
            this.crearprov.AutoSize = true;
            this.crearprov.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.crearprov.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crearprov.Location = new System.Drawing.Point(6, 42);
            this.crearprov.Name = "crearprov";
            this.crearprov.Size = new System.Drawing.Size(55, 19);
            this.crearprov.TabIndex = 1;
            this.crearprov.Text = "Crear";
            this.crearprov.UseVisualStyleBackColor = true;
            // 
            // proveedores
            // 
            this.proveedores.AutoSize = true;
            this.proveedores.Font = new System.Drawing.Font("Raleway Medium", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.proveedores.Location = new System.Drawing.Point(3, 4);
            this.proveedores.Name = "proveedores";
            this.proveedores.Size = new System.Drawing.Size(104, 14);
            this.proveedores.TabIndex = 0;
            this.proveedores.Text = "PROVEEDORES";
            // 
            // Taller
            // 
            this.Taller.AutoSize = true;
            this.Taller.Font = new System.Drawing.Font("Raleway Medium", 8.999999F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Taller.Location = new System.Drawing.Point(4, 8);
            this.Taller.Name = "Taller";
            this.Taller.Size = new System.Drawing.Size(56, 14);
            this.Taller.TabIndex = 0;
            this.Taller.Text = "TALLER";
            // 
            // Guardar
            // 
            this.Guardar.BackColor = System.Drawing.Color.Transparent;
            this.Guardar.BackgroundImage = global::Colorful_visual.Properties.Resources.GUARDAR;
            this.Guardar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Guardar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Guardar.FlatAppearance.BorderSize = 0;
            this.Guardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Guardar.Location = new System.Drawing.Point(148, 381);
            this.Guardar.Name = "Guardar";
            this.Guardar.Size = new System.Drawing.Size(95, 32);
            this.Guardar.TabIndex = 57;
            this.Guardar.UseVisualStyleBackColor = false;
            this.Guardar.Click += new System.EventHandler(this.Guardar_Click);
            // 
            // ACTUALIZAR
            // 
            this.ACTUALIZAR.BackgroundImage = global::Colorful_visual.Properties.Resources.ACTUALIZAR;
            this.ACTUALIZAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ACTUALIZAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ACTUALIZAR.FlatAppearance.BorderSize = 0;
            this.ACTUALIZAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ACTUALIZAR.Location = new System.Drawing.Point(425, 381);
            this.ACTUALIZAR.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ACTUALIZAR.Name = "ACTUALIZAR";
            this.ACTUALIZAR.Size = new System.Drawing.Size(95, 32);
            this.ACTUALIZAR.TabIndex = 58;
            this.ACTUALIZAR.UseVisualStyleBackColor = true;
            this.ACTUALIZAR.Click += new System.EventHandler(this.ACTUALIZAR_Click);
            // 
            // USUARIO
            // 
            this.USUARIO.AutoSize = true;
            this.USUARIO.Font = new System.Drawing.Font("Raleway Medium", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.USUARIO.Location = new System.Drawing.Point(124, 46);
            this.USUARIO.Name = "USUARIO";
            this.USUARIO.Size = new System.Drawing.Size(88, 19);
            this.USUARIO.TabIndex = 59;
            this.USUARIO.Text = "USUARIO";
            // 
            // buscador
            // 
            this.buscador.BackColor = System.Drawing.Color.Transparent;
            this.buscador.BackgroundImage = global::Colorful_visual.Properties.Resources.buscar;
            this.buscador.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buscador.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buscador.FlatAppearance.BorderSize = 0;
            this.buscador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buscador.Location = new System.Drawing.Point(545, 10);
            this.buscador.Name = "buscador";
            this.buscador.Size = new System.Drawing.Size(24, 24);
            this.buscador.TabIndex = 62;
            this.buscador.UseVisualStyleBackColor = false;
            this.buscador.Click += new System.EventHandler(this.Buscador_Click);
            // 
            // Buscar
            // 
            this.Buscar.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Buscar.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Buscar.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Buscar.Location = new System.Drawing.Point(443, 12);
            this.Buscar.Multiline = true;
            this.Buscar.Name = "Buscar";
            this.Buscar.Size = new System.Drawing.Size(94, 20);
            this.Buscar.TabIndex = 61;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(378, 15);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 14);
            this.label10.TabIndex = 60;
            this.label10.Text = "USUARIO";
            // 
            // MODIFICAR
            // 
            this.MODIFICAR.BackgroundImage = global::Colorful_visual.Properties.Resources.MODIFICAR;
            this.MODIFICAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MODIFICAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MODIFICAR.FlatAppearance.BorderSize = 0;
            this.MODIFICAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MODIFICAR.Location = new System.Drawing.Point(289, 381);
            this.MODIFICAR.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MODIFICAR.Name = "MODIFICAR";
            this.MODIFICAR.Size = new System.Drawing.Size(95, 32);
            this.MODIFICAR.TabIndex = 63;
            this.MODIFICAR.UseVisualStyleBackColor = true;
            this.MODIFICAR.Click += new System.EventHandler(this.MODIFICAR_Click);
            // 
            // Inserinsu
            // 
            this.Inserinsu.AutoSize = true;
            this.Inserinsu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Inserinsu.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Inserinsu.Location = new System.Drawing.Point(88, 38);
            this.Inserinsu.Name = "Inserinsu";
            this.Inserinsu.Size = new System.Drawing.Size(66, 19);
            this.Inserinsu.TabIndex = 8;
            this.Inserinsu.Text = "Insertar";
            this.Inserinsu.UseVisualStyleBackColor = true;
            // 
            // inserpro
            // 
            this.inserpro.AutoSize = true;
            this.inserpro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.inserpro.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inserpro.Location = new System.Drawing.Point(88, 38);
            this.inserpro.Name = "inserpro";
            this.inserpro.Size = new System.Drawing.Size(66, 19);
            this.inserpro.TabIndex = 9;
            this.inserpro.Text = "Insertar";
            this.inserpro.UseVisualStyleBackColor = true;
            // 
            // permisos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(700, 451);
            this.Controls.Add(this.MODIFICAR);
            this.Controls.Add(this.buscador);
            this.Controls.Add(this.Buscar);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.USUARIO);
            this.Controls.Add(this.ACTUALIZAR);
            this.Controls.Add(this.Guardar);
            this.Controls.Add(this.flowLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "permisos";
            this.Text = "permisos";
            this.Load += new System.EventHandler(this.cargarpermi);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox consulprov;
        private System.Windows.Forms.CheckBox modifiprov;
        private System.Windows.Forms.CheckBox crearprov;
        private System.Windows.Forms.Label proveedores;
        private System.Windows.Forms.Label Taller;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label permi;
        private System.Windows.Forms.CheckBox consulpermi;
        private System.Windows.Forms.CheckBox modifipermi;
        private System.Windows.Forms.CheckBox crearpermi;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.CheckBox consulusu;
        private System.Windows.Forms.CheckBox modifiusu;
        private System.Windows.Forms.CheckBox crearusu;
        private System.Windows.Forms.Label usuarios;
        private System.Windows.Forms.Label administrador;
        private System.Windows.Forms.Label INSUMOS;
        private System.Windows.Forms.CheckBox consulinsu;
        private System.Windows.Forms.CheckBox modifiinsu;
        private System.Windows.Forms.CheckBox crearinsu;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label PRODUCTOS;
        private System.Windows.Forms.CheckBox consulproduct;
        private System.Windows.Forms.CheckBox modifiproduct;
        private System.Windows.Forms.CheckBox crearproduc;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label FICHATECNICA;
        private System.Windows.Forms.CheckBox consulficha;
        private System.Windows.Forms.CheckBox modifificha;
        private System.Windows.Forms.CheckBox crearficha;
        private System.Windows.Forms.Button Guardar;
        private System.Windows.Forms.Button ACTUALIZAR;
        private System.Windows.Forms.Label USUARIO;
        private System.Windows.Forms.Button buscador;
        private System.Windows.Forms.TextBox Buscar;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button MODIFICAR;
        private System.Windows.Forms.CheckBox inserpro;
        private System.Windows.Forms.CheckBox Inserinsu;
    }
}