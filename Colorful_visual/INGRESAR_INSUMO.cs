﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Colorfull;

namespace Colorful_visual
{
    public partial class INGRESAR_INSUMO : Form
    {
        AG_insu obj = new AG_insu();
        validador aux = new validador();
        int idusuario;
        public INGRESAR_INSUMO(int idusuario)
        {
            this.idusuario = idusuario;
            InitializeComponent();
            cargarinsu();
        }

        private void cargarinsu()
        {
            DataTable tabla = obj.traeinsumosr();

            for (int i = 0; i < tabla.Rows.Count; i++)
            {
                CODIGO.Items.Add(tabla.Rows[i]["idinsumo"].ToString());

            }
        }

        private void CODIGO_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable tabla = obj.busqueda(CODIGO.Text);
            for (int i = 0; i < tabla.Rows.Count; i++)
            {
                if (tabla.Rows[i][1].ToString().Equals(CODIGO.Text))
                {
                    NOMBRE.Text = tabla.Rows[i][0].ToString();
                    break;
                }          
            }

            this.CANTIDAD.Focus();

        }
       // 
        private void GUARDAR_Click(object sender, EventArgs e)
        {
            if (aux.validarSoloNumeros(CANTIDAD.Text))
            {
                obj.Cantidad = this.sumar(int.Parse(CANTIDAD.Text));
                obj.Codigo = CODIGO.Text;
                obj.Estado = 1;
                obj.Usuario = idusuario;
                try
                {
                    if (obj.actualizarinsumo() >= 1 && obj.almacenar("ING", int.Parse(CANTIDAD.Text)) >= 1)
                    {
                        DataGridViewRow fila = new DataGridViewRow();
                        fila.CreateCells(MOSTRAR);

                        fila.Cells[0].Value = CODIGO.Text;
                        fila.Cells[1].Value = NOMBRE.Text;
                        fila.Cells[2].Value = CANTIDAD.Text;

                        MOSTRAR.Rows.Add(fila);
                        

                    }
                    else
                    {
                        MessageBox.Show("Se presento un error inesperado, revise que los datos esten completos.");
                    }

                    this.limpiar(this);

                }catch(Exception ex)
                {
                    MessageBox.Show("Se a presentado un error inesperado, llamar al apoyo tecnico, error: " + ex);
                }
                

            }
            else
            {
                ERROR.SetError(CANTIDAD, "Valor requerido, solo se permiten numeros enteros");
            }
        }

        private int sumar(int x)
        {
            DataTable tabla = obj.traercantidad(CODIGO.Text);

            return x + int.Parse(tabla.Rows[0]["cantidad"].ToString());
        }

        private void limpiar(Control aux)
        {
            foreach (var txt in aux.Controls)
            {
                if (txt is TextBox)
                {
                    ((TextBox)txt).Clear();

                }
                else if (txt is ComboBox)
                {
                    ((ComboBox)txt).SelectedIndex = -1;
                }

            }

            ERROR.Clear();
        }
    }
}
