﻿namespace Colorful_visual
{
    partial class CREAR_INSUMO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label7 = new System.Windows.Forms.Label();
            this.Nombre = new System.Windows.Forms.TextBox();
            this.Guardar = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.buscador = new System.Windows.Forms.Button();
            this.Buscar = new System.Windows.Forms.TextBox();
            this.inss = new System.Windows.Forms.Label();
            this.Codigo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Mostrar = new System.Windows.Forms.ListBox();
            this.Proveedor = new System.Windows.Forms.ComboBox();
            this.Estado = new System.Windows.Forms.ComboBox();
            this.ERROR = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ERROR)).BeginInit();
            this.SuspendLayout();
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(23, 161);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 14);
            this.label7.TabIndex = 58;
            this.label7.Text = "Proveedor";
            // 
            // Nombre
            // 
            this.Nombre.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Nombre.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Nombre.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nombre.Location = new System.Drawing.Point(92, 119);
            this.Nombre.Multiline = true;
            this.Nombre.Name = "Nombre";
            this.Nombre.Size = new System.Drawing.Size(166, 20);
            this.Nombre.TabIndex = 57;
            // 
            // Guardar
            // 
            this.Guardar.BackColor = System.Drawing.Color.Transparent;
            this.Guardar.BackgroundImage = global::Colorful_visual.Properties.Resources.GUARDAR;
            this.Guardar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Guardar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Guardar.FlatAppearance.BorderSize = 0;
            this.Guardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Guardar.Location = new System.Drawing.Point(93, 237);
            this.Guardar.Name = "Guardar";
            this.Guardar.Size = new System.Drawing.Size(95, 32);
            this.Guardar.TabIndex = 56;
            this.Guardar.UseVisualStyleBackColor = false;
            this.Guardar.Click += new System.EventHandler(this.Guardar_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(331, 246);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 14);
            this.label6.TabIndex = 55;
            this.label6.Text = "Estado";
            // 
            // buscador
            // 
            this.buscador.BackColor = System.Drawing.Color.Transparent;
            this.buscador.BackgroundImage = global::Colorful_visual.Properties.Resources.buscar;
            this.buscador.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buscador.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buscador.FlatAppearance.BorderSize = 0;
            this.buscador.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buscador.Location = new System.Drawing.Point(501, 39);
            this.buscador.Name = "buscador";
            this.buscador.Size = new System.Drawing.Size(24, 24);
            this.buscador.TabIndex = 54;
            this.buscador.UseVisualStyleBackColor = false;
            this.buscador.Click += new System.EventHandler(this.Buscador_Click);
            // 
            // Buscar
            // 
            this.Buscar.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Buscar.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Buscar.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Buscar.Location = new System.Drawing.Point(399, 41);
            this.Buscar.Multiline = true;
            this.Buscar.Name = "Buscar";
            this.Buscar.Size = new System.Drawing.Size(94, 20);
            this.Buscar.TabIndex = 53;
            // 
            // inss
            // 
            this.inss.AutoSize = true;
            this.inss.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inss.Location = new System.Drawing.Point(349, 44);
            this.inss.Name = "inss";
            this.inss.Size = new System.Drawing.Size(48, 14);
            this.inss.TabIndex = 52;
            this.inss.Text = "Insumo";
            // 
            // Codigo
            // 
            this.Codigo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Codigo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Codigo.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Codigo.Location = new System.Drawing.Point(92, 75);
            this.Codigo.Multiline = true;
            this.Codigo.Name = "Codigo";
            this.Codigo.Size = new System.Drawing.Size(166, 20);
            this.Codigo.TabIndex = 50;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(23, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 14);
            this.label2.TabIndex = 49;
            this.label2.Text = "Nombre";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 14);
            this.label1.TabIndex = 48;
            this.label1.Text = "Codigo";
            // 
            // Mostrar
            // 
            this.Mostrar.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Mostrar.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Mostrar.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mostrar.FormattingEnabled = true;
            this.Mostrar.ItemHeight = 15;
            this.Mostrar.Location = new System.Drawing.Point(352, 75);
            this.Mostrar.Name = "Mostrar";
            this.Mostrar.Size = new System.Drawing.Size(178, 105);
            this.Mostrar.TabIndex = 63;
            // 
            // Proveedor
            // 
            this.Proveedor.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Proveedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Proveedor.FormattingEnabled = true;
            this.Proveedor.Location = new System.Drawing.Point(92, 158);
            this.Proveedor.Name = "Proveedor";
            this.Proveedor.Size = new System.Drawing.Size(166, 21);
            this.Proveedor.TabIndex = 66;
            this.Proveedor.SelectedIndexChanged += new System.EventHandler(this.Proveedor_SelectedIndexChanged);
            // 
            // Estado
            // 
            this.Estado.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Estado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Estado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Estado.FormattingEnabled = true;
            this.Estado.Items.AddRange(new object[] {
            "Activo",
            "Inactivo"});
            this.Estado.Location = new System.Drawing.Point(377, 243);
            this.Estado.Name = "Estado";
            this.Estado.Size = new System.Drawing.Size(153, 21);
            this.Estado.TabIndex = 67;
            // 
            // ERROR
            // 
            this.ERROR.ContainerControl = this;
            // 
            // CREAR_INSUMO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(558, 328);
            this.Controls.Add(this.Estado);
            this.Controls.Add(this.Proveedor);
            this.Controls.Add(this.Mostrar);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Nombre);
            this.Controls.Add(this.Guardar);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.buscador);
            this.Controls.Add(this.Buscar);
            this.Controls.Add(this.inss);
            this.Controls.Add(this.Codigo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CREAR_INSUMO";
            this.Text = "CREAR_INSUMO";
            this.Load += new System.EventHandler(this.asignarpermi);
            ((System.ComponentModel.ISupportInitialize)(this.ERROR)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Nombre;
        private System.Windows.Forms.Button Guardar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buscador;
        private System.Windows.Forms.TextBox Buscar;
        private System.Windows.Forms.Label inss;
        private System.Windows.Forms.TextBox Codigo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox Mostrar;
        private System.Windows.Forms.ComboBox Proveedor;
        private System.Windows.Forms.ComboBox Estado;
        private System.Windows.Forms.ErrorProvider ERROR;
    }
}