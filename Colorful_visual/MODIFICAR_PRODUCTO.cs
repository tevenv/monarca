﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Colorfull;

namespace Colorful_visual
{
    public partial class MODIFICAR_PRODUCTO : Form
    {
        C_Producto obj = new C_Producto();
        int idusuario;
        public MODIFICAR_PRODUCTO(int idusuario)
        {
            this.idusuario = idusuario;
            InitializeComponent();
            MODIFICAR.Enabled = false;
            ACTUALIZAR.Enabled = false;
            CANTIDAD.Enabled = false;
            ESTADO.Enabled = false;
        }

        private void BUSCAR_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(BUSCA.Text))
            {
                MessageBox.Show("Ingresa por favor un parametro de busqueda");
            }
            else
            {
                Form formulario = new Buscador(BUSCA.Text, MUESTRA, 4);
                this.limpiar(this);
                this.MUESTRA.Items.Clear();
                formulario.Show();
                MODIFICAR.Enabled = true;
                ACTUALIZAR.Enabled = true;

            }
        }

        private void limpiar(Control aux)
        {
            foreach (var txt in aux.Controls)
            {
                if (txt is TextBox)
                {
                    ((TextBox)txt).Clear();

                }
                else if (txt is ComboBox)
                {
                    ((ComboBox)txt).SelectedIndex = -1;
                }

            }
           
        }

        private void MODIFICAR_Click(object sender, EventArgs e)
        {
            CANTIDAD.Text = MUESTRA.Items[4].ToString();

            if (MUESTRA.Items[6].Equals("True"))
            {
                ESTADO.SelectedIndex = 0;
            }
            if (MUESTRA.Items[6].Equals("False"))
            {
                ESTADO.SelectedIndex = 1;
            }
            CANTIDAD.Enabled = true;
            ESTADO.Enabled = true;
            CANTIDAD.Focus();
            CANTIDAD.SelectionLength = 0;
            ACTUALIZAR.Enabled = true;
        }

        private void ACTUALIZAR_Click(object sender, EventArgs e)
        {
            string codigo = MUESTRA.Items[0].ToString();
            obj.Codigo = MUESTRA.Items[0].ToString();
            obj.Estado = (ESTADO.Text.Equals("Activo")) ? 1 : 0;
            obj.Usuario = idusuario;
            this.MUESTRA.Items.Clear();

            try
            {
                if (obj.actualproducto(int.Parse(CANTIDAD.Text)) >= 1 && obj.inproducto("ACT", int.Parse(CANTIDAD.Text)) >= 1)
                {

                    DataTable tabla = obj.consulpro(codigo);

                    for (int i = 0; i < tabla.Columns.Count; i++)
                    {
                        MUESTRA.Items.Add(tabla.Rows[0][i].ToString());
                        MUESTRA.Items.Add(" ");

                    }

                }
                else
                {
                    MessageBox.Show("Se presento un error inesperado, revise que los datos esten completos.");
                }

                this.limpiar(this);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Se a presentado un error inesperado, llamar al apoyo tecnico, error: " + ex);
            }
        }
    }
}
