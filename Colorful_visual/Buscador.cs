﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Colorfull;


namespace Colorful_visual
{
    public partial class Buscador : Form
    {
        proveedor obj = new proveedor();
        AG_insu ins = new AG_insu();
        Ficha fic = new Ficha();
        C_Producto pro = new C_Producto();
        usuarios usu = new usuarios();
        Permisos per = new Permisos();
      
        

        String parametro;
        ListBox muestra;
        Label usuario;
        int aux;

        

        public Buscador(string parametro, ListBox muestra, int aux)
        {
            this.parametro = parametro;
            this.muestra = muestra;
            this.aux = aux;
                        
            InitializeComponent();

        }

        public Buscador(string parametro, Label usuario, int aux)
        {
            this.parametro = parametro;
            this.usuario = usuario;
            this.aux = aux;

            InitializeComponent();

        }




        private void iniciar(object sender, EventArgs e)
        {
            DataTable tabla = new DataTable();
            switch (aux)
            {
                case 1: 
                     tabla = obj.conspro(parametro);
                    break;
                case 2:
                     tabla = ins.busqueda(parametro);
                    break;
                case 3:
                    tabla = fic.buscar(parametro);
                    break;
                case 4:
                    tabla = pro.buscar(parametro);
                    break;
                case 5:
                    tabla = usu.buscusua(parametro);
                    break;
                case 6:
                    tabla = per.buscarusuact();
                    break;
                case 7:
                    tabla = per.conspermi(parametro);
                    break;


            }
            
            Busca.DataSource = tabla;
        }

        private void Cerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Seleccionar(object sender, DataGridViewCellEventArgs e)
        {
            int n = e.RowIndex;
            string x = "";
            DataTable tabla = new DataTable();
            DataTable tab = new DataTable();

            switch (aux)
            {
                case 1:
                    if (n != -1)
                    {
                        x = (string)Busca.Rows[n].Cells["Nit"].Value;

                    }

                    tabla = obj.proveed(x);
                    break;
                case 2:
                    if (n != -1)
                    {
                        x = (string)Busca.Rows[n].Cells[1].Value;

                    }

                    tabla = ins.consultarinsumo(x);
                    break;
                case 3:
                    if (n != -1)
                    {
                        x = (string)Busca.Rows[n].Cells[1].Value;

                    }

                    tabla = fic.fichatec(x);
                    break;
                case 4:
                    if (n != -1)
                    {
                        x = (string)Busca.Rows[n].Cells[1].Value;

                    }

                    tabla = pro.consulpro(x);              
                    break;
                case 5:
                    if (n != -1)
                    {
                        x = (string)Busca.Rows[n].Cells[2].Value;

                    }

                    tabla = usu.consusua(x);
                    break;
                case 6:
                    if (n != -1)
                    {
                        x = (string)Busca.Rows[n].Cells[2].Value;

                        DataTable ta = per.conspermiexis(x);
                        if(ta.Rows.Count >= 1)
                        {
                            this.Close();
                            MessageBox.Show("El usuario ya cuenta con permisos asignados");
                        }
                        else
                        {
                            this.usuario.Text = x;
                        }
                        

                    }
                    break;
                case 7:
                    if (n != -1)
                    {
                        x = (string)Busca.Rows[n].Cells[2].Value;

                        this.usuario.Text = x;
                    }

                    break;

            }

            if (aux == 3)
            {
                for (int i = 1; i < tabla.Columns.Count; i++)
                {
                    this.muestra.Items.Add(tabla.Rows[0][i].ToString());
                    this.muestra.Items.Add(" ");
                    if (i == 2)
                    {
                        for(int j=0; j<tabla.Rows.Count; j++)
                        {
                            this.muestra.Items.Add(tabla.Rows[j][3].ToString()+ " cantidad: " + tabla.Rows[j][4].ToString());
                            this.muestra.Items.Add(" ");
                        }
                        i = 4;
                    }

                }


            }
            else
            {
                

                for (int i = 0; i < tabla.Columns.Count; i++)
                {
                    this.muestra.Items.Add(tabla.Rows[0][i].ToString());
                    this.muestra.Items.Add(" ");

                }
                
            }

            this.Close();

        }

        
    }
}
