﻿namespace Colorful_visual
{
    partial class Info_insumos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Estado = new System.Windows.Forms.ComboBox();
            this.Fechaini = new System.Windows.Forms.DateTimePicker();
            this.fechafin = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Documento = new System.Windows.Forms.ComboBox();
            this.tablainfo = new System.Windows.Forms.DataGridView();
            this.guardarcomo = new System.Windows.Forms.SaveFileDialog();
            this.button2 = new System.Windows.Forms.Button();
            this.Consultar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.tablainfo)).BeginInit();
            this.SuspendLayout();
            // 
            // Estado
            // 
            this.Estado.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Estado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Estado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Estado.FormattingEnabled = true;
            this.Estado.Items.AddRange(new object[] {
            "Activo",
            "Inactivo",
            "Todos"});
            this.Estado.Location = new System.Drawing.Point(21, 115);
            this.Estado.Name = "Estado";
            this.Estado.Size = new System.Drawing.Size(98, 21);
            this.Estado.TabIndex = 1;
            // 
            // Fechaini
            // 
            this.Fechaini.CalendarMonthBackground = System.Drawing.Color.WhiteSmoke;
            this.Fechaini.CustomFormat = "yyyy/MM/dd";
            this.Fechaini.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Fechaini.Location = new System.Drawing.Point(20, 196);
            this.Fechaini.Name = "Fechaini";
            this.Fechaini.Size = new System.Drawing.Size(99, 20);
            this.Fechaini.TabIndex = 3;
            // 
            // fechafin
            // 
            this.fechafin.CustomFormat = "yyyy/MM/dd";
            this.fechafin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.fechafin.Location = new System.Drawing.Point(20, 280);
            this.fechafin.Name = "fechafin";
            this.fechafin.Size = new System.Drawing.Size(96, 20);
            this.fechafin.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Estado";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Documento";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 169);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Fecha inicial";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(37, 253);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Feca final";
            // 
            // Documento
            // 
            this.Documento.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Documento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Documento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Documento.FormattingEnabled = true;
            this.Documento.Items.AddRange(new object[] {
            "Inventario",
            "Ingresos",
            "Salidas"});
            this.Documento.Location = new System.Drawing.Point(21, 43);
            this.Documento.Name = "Documento";
            this.Documento.Size = new System.Drawing.Size(98, 21);
            this.Documento.TabIndex = 11;
            this.Documento.SelectedIndexChanged += new System.EventHandler(this.Documento_SelectedIndexChanged);
            // 
            // tablainfo
            // 
            this.tablainfo.AllowUserToAddRows = false;
            this.tablainfo.AllowUserToDeleteRows = false;
            this.tablainfo.AllowUserToOrderColumns = true;
            this.tablainfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.tablainfo.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.tablainfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tablainfo.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.tablainfo.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(168)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Raleway Medium", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(168)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.tablainfo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.tablainfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tablainfo.EnableHeadersVisualStyles = false;
            this.tablainfo.GridColor = System.Drawing.Color.WhiteSmoke;
            this.tablainfo.Location = new System.Drawing.Point(165, 43);
            this.tablainfo.Name = "tablainfo";
            this.tablainfo.ReadOnly = true;
            this.tablainfo.RowHeadersVisible = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.CornflowerBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.WhiteSmoke;
            this.tablainfo.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.tablainfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tablainfo.Size = new System.Drawing.Size(234, 173);
            this.tablainfo.TabIndex = 35;
            // 
            // guardarcomo
            // 
            this.guardarcomo.DefaultExt = "xlsx";
            this.guardarcomo.FileName = "Informe Insumos";
            this.guardarcomo.Filter = "Hoja de calculo(*.xls)|*.*";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.BackgroundImage = global::Colorful_visual.Properties.Resources.excelrosa;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(350, 241);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(45, 59);
            this.button2.TabIndex = 10;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // Consultar
            // 
            this.Consultar.BackColor = System.Drawing.Color.Transparent;
            this.Consultar.BackgroundImage = global::Colorful_visual.Properties.Resources.CONSULTAR;
            this.Consultar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Consultar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Consultar.FlatAppearance.BorderSize = 0;
            this.Consultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Consultar.Location = new System.Drawing.Point(176, 253);
            this.Consultar.Name = "Consultar";
            this.Consultar.Size = new System.Drawing.Size(102, 44);
            this.Consultar.TabIndex = 9;
            this.Consultar.UseVisualStyleBackColor = false;
            this.Consultar.Click += new System.EventHandler(this.Consultar_Click);
            // 
            // Info_insumos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(448, 314);
            this.Controls.Add(this.tablainfo);
            this.Controls.Add(this.fechafin);
            this.Controls.Add(this.Fechaini);
            this.Controls.Add(this.Documento);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.Consultar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Estado);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Info_insumos";
            this.Text = "Info_insumos";
            this.TransparencyKey = System.Drawing.Color.White;
            ((System.ComponentModel.ISupportInitialize)(this.tablainfo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox Estado;
        private System.Windows.Forms.DateTimePicker Fechaini;
        private System.Windows.Forms.DateTimePicker fechafin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button Consultar;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox Documento;
        private System.Windows.Forms.DataGridView tablainfo;
        private System.Windows.Forms.SaveFileDialog guardarcomo;
    }
}