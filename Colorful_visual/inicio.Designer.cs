﻿namespace Colorful_visual
{
    partial class inicio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(inicio));
            this.usuario = new System.Windows.Forms.TextBox();
            this.ingresar = new System.Windows.Forms.Button();
            this.contraseña = new System.Windows.Forms.TextBox();
            this.cerrar = new System.Windows.Forms.LinkLabel();
            this.registro = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // usuario
            // 
            this.usuario.BackColor = System.Drawing.Color.White;
            this.usuario.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.usuario.Location = new System.Drawing.Point(148, 199);
            this.usuario.Name = "usuario";
            this.usuario.Size = new System.Drawing.Size(133, 13);
            this.usuario.TabIndex = 1;
            // 
            // ingresar
            // 
            this.ingresar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(189)))));
            this.ingresar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ingresar.FlatAppearance.BorderSize = 0;
            this.ingresar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(189)))));
            this.ingresar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ingresar.Font = new System.Drawing.Font("Raleway Medium", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ingresar.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.ingresar.Location = new System.Drawing.Point(161, 297);
            this.ingresar.Name = "ingresar";
            this.ingresar.Size = new System.Drawing.Size(99, 27);
            this.ingresar.TabIndex = 3;
            this.ingresar.Text = "INGRESAR";
            this.ingresar.UseVisualStyleBackColor = false;
            this.ingresar.Click += new System.EventHandler(this.Ingresar_Click);
            // 
            // contraseña
            // 
            this.contraseña.BackColor = System.Drawing.Color.White;
            this.contraseña.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.contraseña.Location = new System.Drawing.Point(148, 244);
            this.contraseña.Name = "contraseña";
            this.contraseña.PasswordChar = '♥';
            this.contraseña.Size = new System.Drawing.Size(144, 13);
            this.contraseña.TabIndex = 2;
            // 
            // cerrar
            // 
            this.cerrar.AutoSize = true;
            this.cerrar.BackColor = System.Drawing.Color.Transparent;
            this.cerrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cerrar.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cerrar.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(189)))));
            this.cerrar.Location = new System.Drawing.Point(313, 86);
            this.cerrar.Name = "cerrar";
            this.cerrar.Size = new System.Drawing.Size(20, 19);
            this.cerrar.TabIndex = 4;
            this.cerrar.TabStop = true;
            this.cerrar.Text = "X";
            this.cerrar.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Cerrar_LinkClicked);
            // 
            // registro
            // 
            this.registro.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(73)))), ((int)(((byte)(149)))));
            this.registro.AutoSize = true;
            this.registro.BackColor = System.Drawing.Color.Transparent;
            this.registro.DisabledLinkColor = System.Drawing.Color.Transparent;
            this.registro.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(66)))), ((int)(((byte)(189)))));
            this.registro.Location = new System.Drawing.Point(183, 336);
            this.registro.Name = "registro";
            this.registro.Size = new System.Drawing.Size(55, 13);
            this.registro.TabIndex = 5;
            this.registro.TabStop = true;
            this.registro.Text = "Registrate";
            this.registro.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Registro_LinkClicked);
            // 
            // inicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(428, 400);
            this.Controls.Add(this.registro);
            this.Controls.Add(this.cerrar);
            this.Controls.Add(this.contraseña);
            this.Controls.Add(this.ingresar);
            this.Controls.Add(this.usuario);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "inicio";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "inicio";
            this.TransparencyKey = System.Drawing.SystemColors.Control;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox usuario;
        private System.Windows.Forms.Button ingresar;
        private System.Windows.Forms.TextBox contraseña;
        private System.Windows.Forms.LinkLabel cerrar;
        private System.Windows.Forms.LinkLabel registro;
    }
}