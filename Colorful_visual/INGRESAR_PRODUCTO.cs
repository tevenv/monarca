﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Colorfull;

namespace Colorful_visual
{
    public partial class INGRESAR_PRODUCTO : Form
    {
        C_Producto obj = new C_Producto();
        validador val = new validador();
        DataTable tabla;
        int Z = 0;
        int idusuario;
        public INGRESAR_PRODUCTO(int idusuario)
        {
            this.idusuario = idusuario;
            InitializeComponent();
            this.cargarinsu();
        }


        private void cargarinsu()
        {
             tabla = obj.llamarcod();

            for (int i = 0; i < tabla.Rows.Count; i++)
            {
                CODIGO.Items.Add(tabla.Rows[i][0].ToString());

            }
        }

        private void CODIGO_SelectedIndexChanged(object sender, EventArgs e)
        {
             
            for (int i = 0; i < tabla.Rows.Count; i++)
            {
                if (tabla.Rows[i][0].ToString().Equals(CODIGO.Text))
                {
                    NOMBRE.Text = tabla.Rows[i][1].ToString();
                    break;
                }
            }

            this.CANTIDAD.Focus();
        }

        private void GUARDAR_Click(object sender, EventArgs e)
        {
            
            if (CODIGO.SelectedIndex >= 0)
            {
                if (val.validarSoloNumeros(CANTIDAD.Text))
                {
                    obj.Codigo = CODIGO.Text;
                    DataTable tabla = obj.consultabpro();
                    obj.Ficha = tabla.Rows[0][1].ToString();
                    obj.Cantidad = int.Parse(CANTIDAD.Text);

                    Tuple<List<string>, List<int>, int, int> aux = obj.control();

                    List<string> insumo = new List<string>();
                    List<int> canti = new List<int>();
                    int X;
                    int J;

                    insumo = aux.Item1;
                    canti = aux.Item2;
                    X = aux.Item3;
                    J = aux.Item4;
                                        
                    
                    obj.Nombre = NOMBRE.Text;
                    obj.Estado = 1;
                    obj.Usuario = idusuario;
                    
                    if (J==1)
                    {
                        string mensage = string.Empty;

                        for (int i = 0; i < insumo.Count; i++)
                        {
                            mensage = mensage + "El insumo " + insumo[i] + " tiene una cantidad de " + canti[i].ToString() + "\n";
                        }
                        string mensa = "No se cuentan con suficientes insumos para lograr el pedido, el estado actual de los insumos son: \n";
                        MessageBox.Show(mensa + mensage);

                    }
                    else
                    {
                        if (X==1 && Z==0)
                        {
                            string mensage = string.Empty;

                            for (int i = 0; i < insumo.Count; i++)
                            {
                                mensage = mensage + "El insumo " + insumo[i] + " tiene una cantidad de " + canti[i].ToString() + "\n";
                            }
                           string mensa = "Los insumos se estan agotando, recuerda hacer un nuevo pedido o ingresar los insumos que no se encuentren registrados \n" + mensage + "Desea seguir viendo este mensaje?";
                           
                            var resultado = MessageBox.Show( mensa, " Notificacion insumos ", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                            if (resultado == DialogResult.No)
                            {
                                Z = 1;
                            }

                        }
                        int sum = this.sumar(int.Parse(CANTIDAD.Text));
                        try
                        {
                            if (obj.actualizarins() >= 1 && obj.exinsumo() >= 1 && obj.actualproducto(sum) >= 1 && obj.inproducto("ING", int.Parse(CANTIDAD.Text)) >= 1)
                            {
                                DataGridViewRow fila = new DataGridViewRow();
                                fila.CreateCells(MOSTRAR);

                                fila.Cells[0].Value = CODIGO.Text;
                                fila.Cells[1].Value = NOMBRE.Text;
                                fila.Cells[2].Value = CANTIDAD.Text;

                                MOSTRAR.Rows.Add(fila);
                            }
                            else
                            {
                                MessageBox.Show("Se presento un error inesperado, revise que los datos esten completos.");
                            }

                            this.limpiar(this);

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Se a presentado un error inesperado, llamar al apoyo tecnico, error: " + ex);
                        }

                    }


                }
                else
                {
                    ERROR.SetError(CANTIDAD, "Valor requerido, solo se permiten numeros enteros");
                }
            }
            else
            {
                MessageBox.Show("Seleccione un producto a ingresar.");
            }


        }

        private void limpiar(Control aux)
        {
            foreach (var txt in aux.Controls)
            {
                if (txt is TextBox)
                {
                    ((TextBox)txt).Clear();

                }
                else if (txt is ComboBox)
                {
                    ((ComboBox)txt).SelectedIndex = -1;
                }

            }

            ERROR.Clear();
        }

        private int sumar(int x)
        {

            DataTable tabla = obj.cantidadpro();

            return x + int.Parse(tabla.Rows[0][0].ToString());
        }

    }
}
