﻿namespace Colorful_visual
{
    partial class Info_provee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.exportexcel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Estado = new System.Windows.Forms.ComboBox();
            this.tablainfo = new System.Windows.Forms.DataGridView();
            this.guardarcomo = new System.Windows.Forms.SaveFileDialog();
            this.Consultar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.tablainfo)).BeginInit();
            this.SuspendLayout();
            // 
            // exportexcel
            // 
            this.exportexcel.BackColor = System.Drawing.Color.Transparent;
            this.exportexcel.BackgroundImage = global::Colorful_visual.Properties.Resources.excelrosa;
            this.exportexcel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.exportexcel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exportexcel.FlatAppearance.BorderSize = 0;
            this.exportexcel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exportexcel.Location = new System.Drawing.Point(303, 263);
            this.exportexcel.Name = "exportexcel";
            this.exportexcel.Size = new System.Drawing.Size(48, 55);
            this.exportexcel.TabIndex = 32;
            this.exportexcel.UseVisualStyleBackColor = false;
            this.exportexcel.Click += new System.EventHandler(this.Exportexcel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(217, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "Estado";
            // 
            // Estado
            // 
            this.Estado.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Estado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Estado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Estado.FormattingEnabled = true;
            this.Estado.Items.AddRange(new object[] {
            "Activos",
            "Inactivos",
            "Todos"});
            this.Estado.Location = new System.Drawing.Point(189, 39);
            this.Estado.Name = "Estado";
            this.Estado.Size = new System.Drawing.Size(98, 21);
            this.Estado.TabIndex = 24;
            // 
            // tablainfo
            // 
            this.tablainfo.AllowUserToAddRows = false;
            this.tablainfo.AllowUserToDeleteRows = false;
            this.tablainfo.AllowUserToOrderColumns = true;
            this.tablainfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.tablainfo.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.tablainfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tablainfo.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.tablainfo.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(168)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Raleway Medium", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(168)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.tablainfo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.tablainfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tablainfo.EnableHeadersVisualStyles = false;
            this.tablainfo.GridColor = System.Drawing.Color.WhiteSmoke;
            this.tablainfo.Location = new System.Drawing.Point(12, 92);
            this.tablainfo.Name = "tablainfo";
            this.tablainfo.ReadOnly = true;
            this.tablainfo.RowHeadersVisible = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.CornflowerBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.WhiteSmoke;
            this.tablainfo.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.tablainfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tablainfo.Size = new System.Drawing.Size(443, 153);
            this.tablainfo.TabIndex = 33;
            // 
            // guardarcomo
            // 
            this.guardarcomo.DefaultExt = "xlsx";
            this.guardarcomo.FileName = "Informe proveedor";
            this.guardarcomo.Filter = "Hoja de calculo(*.xls)|*.*";
            // 
            // Consultar
            // 
            this.Consultar.BackgroundImage = global::Colorful_visual.Properties.Resources.CONSULTAR;
            this.Consultar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Consultar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Consultar.FlatAppearance.BorderSize = 0;
            this.Consultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Consultar.Location = new System.Drawing.Point(82, 263);
            this.Consultar.Name = "Consultar";
            this.Consultar.Size = new System.Drawing.Size(102, 44);
            this.Consultar.TabIndex = 31;
            this.Consultar.UseVisualStyleBackColor = true;
            this.Consultar.Click += new System.EventHandler(this.Consultar_Click);
            // 
            // Info_provee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(467, 340);
            this.Controls.Add(this.tablainfo);
            this.Controls.Add(this.exportexcel);
            this.Controls.Add(this.Consultar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Estado);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Info_provee";
            this.Text = "Info_provee";
            this.TransparencyKey = System.Drawing.Color.White;
            ((System.ComponentModel.ISupportInitialize)(this.tablainfo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button exportexcel;
        private System.Windows.Forms.Button Consultar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox Estado;
        private System.Windows.Forms.DataGridView tablainfo;
        private System.Windows.Forms.SaveFileDialog guardarcomo;
    }
}