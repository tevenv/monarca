﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using Colorfull;

namespace Colorful_visual
{
    public partial class permisos : Form
    {
        Permisos obj = new Permisos();
        int idusuario, consultar=0;
        public permisos(int idusuario)
        {
            this.idusuario = idusuario;
            InitializeComponent();
            MODIFICAR.Enabled = false;
            ACTUALIZAR.Enabled = false;
        }

        private void Buscador_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Buscar.Text))
            {
                Form formulario = new Buscador(Buscar.Text, USUARIO, 6);
                this.limpiar(Controls);
                Guardar.Enabled = true;
                MODIFICAR.Enabled = false;
                ACTUALIZAR.Enabled = false;
                formulario.Show();
                
            }
            else
            {
                if (consultar == 1)
                {
                    Form formulario = new Buscador(Buscar.Text, USUARIO, 7);
                    this.limpiar(Controls);
                    MODIFICAR.Enabled = true;
                    Guardar.Enabled = false;
                    formulario.Show();
                }
                else
                {
                    MessageBox.Show("Usted no cuenta con permisos para este proceso");
                    this.limpiar(Controls);
                }
            }
                        
        }

        private void limpiar(Control.ControlCollection aux)
        {
            foreach (Control txt in aux)
            {
                if (txt is TextBox)
                {
                    ((TextBox)txt).Clear();

                }
                else if (txt is CheckBox)
                {
                    ((CheckBox)txt).Checked = false;

                }
                limpiar(txt.Controls);
            }
           


        }

        private void Guardar_Click(object sender, EventArgs e)
        {
            int i = 0;

            List<string> modulo1 = new List<string>();
            List<string> modulo2 = new List<string>();

            List<int> crear = new List<int>();
            List<int> modificar = new List<int>();
            List<int> consultar = new List<int>();
            List<int> insertar = new List<int>();


            if (crearusu.Checked == true || modifiusu.Checked == true || consulusu.Checked == true )
            {
                modulo1.Add(administrador.Text);
                modulo2.Add(usuarios.Text);
                
                if(crearusu.Checked == true)
                {
                    crear.Add(1);
                    
                }
                else
                {
                    crear.Add(0);
                }

                if(modifiusu.Checked == true)
                {
                    modificar.Add(1);
                    
                    
                }
                else
                {
                    modificar.Add(0);
                }

                if (consulusu.Checked == true)
                {
                    consultar.Add(1);
                }
                else
                {
                    consultar.Add(0);
                }

                insertar.Add(0);

            }

            if(crearpermi.Checked == true || modifipermi.Checked == true || consulpermi.Checked == true)
            {
                modulo1.Add(administrador.Text);
                modulo2.Add(permi.Text);
                if(crearpermi.Checked == true)
                {
                    crear.Add(1);
                }
                else
                {
                    crear.Add(0);
                }

                if (modifipermi.Checked == true)
                {
                    modificar.Add(1);
                }
                else
                {
                    modificar.Add(0);
                }

                if(consulpermi.Checked == true)
                {
                    consultar.Add(1);
                }
                else
                {
                    consultar.Add(0);
                }
                insertar.Add(0);
            }

            if(crearprov.Checked == true || modifiprov.Checked == true || consulprov.Checked == true)
            {
                modulo1.Add(Taller.Text);
                modulo2.Add(proveedores.Text);
                crear.Add(crearprov.Checked == true ? 1 : 0);
                modificar.Add(modifiprov.Checked == true ? 1 : 0);
                consultar.Add(consulprov.Checked == true ? 1 : 0);
                insertar.Add(0);
            }

            if(crearinsu.Checked == true || Inserinsu.Checked==true || modifiinsu.Checked == true || consulinsu.Checked == true)
            {
                modulo1.Add(Taller.Text);
                modulo2.Add(INSUMOS.Text);
                crear.Add(crearinsu.Checked == true ? 1 : 0);
                modificar.Add(modifiinsu.Checked == true ? 1 : 0);
                consultar.Add(consulinsu.Checked == true ? 1 : 0);
                insertar.Add(Inserinsu.Checked == true ? 1 : 0);
           
            }

            if(crearficha.Checked==true || modifificha.Checked == true || consulficha.Checked == true)
            {
                modulo1.Add(Taller.Text);
                modulo2.Add(FICHATECNICA.Text);
                crear.Add(crearficha.Checked == true ? 1 : 0);
                modificar.Add(modifificha.Checked == true ? 1 : 0);
                consultar.Add(consulficha.Checked == true ? 1 : 0);
                insertar.Add(0);
            }

            if (crearproduc.Checked == true || inserpro.Checked==true || modifiproduct.Checked == true || consulproduct.Checked == true)
            {
                modulo1.Add(Taller.Text);
                modulo2.Add(PRODUCTOS.Text);
                crear.Add(crearproduc.Checked == true ? 1 : 0);
                modificar.Add(modifiproduct.Checked == true ? 1 : 0);
                consultar.Add(consulproduct.Checked == true ? 1 : 0);
                insertar.Add(inserpro.Checked == true ? 1 : 0);
               
            }

            DataTable tabla = obj.codusua(USUARIO.Text);

            obj.Usuario = int.Parse(tabla.Rows[0][0].ToString());
            obj.Usuario2 = idusuario;

            foreach(string mol1 in modulo1)
            {
                obj.Modulo1 = mol1;
                obj.Modulo2 = modulo2[i];
                obj.Crear = crear[i];
                obj.Modificar = modificar[i];
                obj.Consultar = consultar[i];
                obj.Insertar = insertar[i];
                try
                {
                    if (obj.inspermi()>=1)
                    {
                        i++;
                    }
                    else
                    {
                        MessageBox.Show("Se presento un error, revise que los campos esten completos");
                        break;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Se a presentado un error inesperado, llamar al apoyo tecnico, error numero" + ex);
                    break;
                }
            }

            if (i >= 1)
            {
                MessageBox.Show("Los permisos fueron otorgados correctamente");
            }

        }
      
        private void MODIFICAR_Click(object sender, EventArgs e)
        {
            DataTable tabla = obj.consulpermi(USUARIO.Text);

            for(int i =0; i<tabla.Rows.Count; i++)
            {
                if (tabla.Rows[i][0].Equals(administrador.Text))
                {
                    if (tabla.Rows[i][1].Equals(usuarios.Text))
                    {
                        if (tabla.Rows[i][2].Equals(true))
                        {
                            crearusu.Checked = true;
                        }
                        else
                        {
                            crearusu.Checked = false;
                        }

                        if (tabla.Rows[i][3].Equals(true))
                        {
                            modifiusu.Checked = true;
                        }
                        else
                        {
                            modifiusu.Checked = false;
                        }
                        if (tabla.Rows[i][4].Equals(true))
                        {
                            consulusu.Checked = true;
                        }
                        else
                        {
                            consulusu.Checked = false;
                        }
                    }

                    if (tabla.Rows[i][1].Equals(permi.Text))
                    {
                        if (tabla.Rows[i][2].Equals(true))
                        {
                            crearpermi.Checked = true;
                        }
                        else
                        {
                            crearpermi.Checked = false;
                        }

                        if (tabla.Rows[i][3].Equals(true))
                        {
                            modifipermi.Checked = true;
                        }
                        else
                        {
                            modifipermi.Checked = false;
                        }

                        if (tabla.Rows[i][4].Equals(true))
                        {
                            consulpermi.Checked = true;
                        }
                        else
                        {
                            consulpermi.Checked = false;
                        }
                    }

                }

                if (tabla.Rows[i][0].Equals(Taller.Text))
                {
                    if (tabla.Rows[i][1].Equals(proveedores.Text))
                    {
                        crearprov.Checked = tabla.Rows[i][2].Equals(true) ? true : false;
                        modifiprov.Checked = tabla.Rows[i][3].Equals(true) ? true : false;
                        consulprov.Checked = tabla.Rows[i][4].Equals(true) ? true : false;
                    }
                    if (tabla.Rows[i][1].Equals(INSUMOS.Text))
                    {
                        crearinsu.Checked = tabla.Rows[i][2].Equals(true) ? true : false;
                        modifiinsu.Checked = tabla.Rows[i][3].Equals(true) ? true : false;
                        consulinsu.Checked = tabla.Rows[i][4].Equals(true) ? true : false;
                    }
                    if (tabla.Rows[i][1].Equals(FICHATECNICA.Text))
                    {
                        crearficha.Checked = tabla.Rows[i][2].Equals(true) ? true : false;
                        modifificha.Checked = tabla.Rows[i][3].Equals(true) ? true : false;
                        consulficha.Checked = tabla.Rows[i][4].Equals(true) ? true : false;
                    }
                    if (tabla.Rows[i][1].Equals(PRODUCTOS.Text))
                    {
                        crearproduc.Checked = tabla.Rows[i][2].Equals(true) ? true : false;
                        modifiproduct.Checked = tabla.Rows[i][3].Equals(true) ? true : false;
                        consulproduct.Checked = tabla.Rows[i][4].Equals(true) ? true : false;
                    }
                }

            }
            ACTUALIZAR.Enabled = true;
        }

        private void todos(object sender, EventArgs e)
        {
            consulusu.Checked = modifiusu.Checked == true ? true : false;
        }

        private void modifpermis(object sender, EventArgs e)
        {
            consulpermi.Checked = modifipermi.Checked == true ? true : false;
            
        }

        private void ACTUALIZAR_Click(object sender, EventArgs e)
        {
            int i = 0;

            List<string> modulo1 = new List<string>();
            List<string> modulo2 = new List<string>();

            List<int> crear = new List<int>();
            List<int> modificar = new List<int>();
            List<int> consultar = new List<int>();
            List<int> insertar = new List<int>();


            if (crearusu.Checked == true || modifiusu.Checked == true || consulusu.Checked == true)
            {
                modulo1.Add(administrador.Text);
                modulo2.Add(usuarios.Text);

                if (crearusu.Checked == true)
                {
                    crear.Add(1);

                }
                else
                {
                    crear.Add(0);
                }

                if (modifiusu.Checked == true)
                {
                    modificar.Add(1);


                }
                else
                {
                    modificar.Add(0);
                }

                if (consulusu.Checked == true)
                {
                    consultar.Add(1);
                }
                else
                {
                    consultar.Add(0);
                }
                insertar.Add(0);

            }

            if (crearusu.Checked == false || modifiusu.Checked == false || consulusu.Checked == false)
            {
                modulo1.Add(administrador.Text);
                modulo2.Add(usuarios.Text);

                if (crearusu.Checked == true)
                {
                    crear.Add(1);

                }
                else
                {
                    crear.Add(0);
                }

                if (modifiusu.Checked == true)
                {
                    modificar.Add(1);


                }
                else
                {
                    modificar.Add(0);
                }

                if (consulusu.Checked == true)
                {
                    consultar.Add(1);
                }
                else
                {
                    consultar.Add(0);
                }
                insertar.Add(0);
            }

            if (crearpermi.Checked == true || modifipermi.Checked == true || consulpermi.Checked == true)
            {
                modulo1.Add(administrador.Text);
                modulo2.Add(permi.Text);
                if (crearpermi.Checked == true)
                {
                    crear.Add(1);
                }
                else
                {
                    crear.Add(0);
                }

                if (modifipermi.Checked == true)
                {
                    modificar.Add(1);
                }
                else
                {
                    modificar.Add(0);
                }

                if (consulpermi.Checked == true)
                {
                    consultar.Add(1);
                }
                else
                {
                    consultar.Add(0);
                }
                insertar.Add(0);
            }

            if (crearpermi.Checked == false || modifipermi.Checked == false || consulpermi.Checked == false)
            {
                modulo1.Add(administrador.Text);
                modulo2.Add(permi.Text);
                if (crearpermi.Checked == true)
                {
                    crear.Add(1);
                }
                else
                {
                    crear.Add(0);
                }

                if (modifipermi.Checked == true)
                {
                    modificar.Add(1);
                }
                else
                {
                    modificar.Add(0);
                }

                if (consulpermi.Checked == true)
                {
                    consultar.Add(1);
                }
                else
                {
                    consultar.Add(0);
                }
                insertar.Add(0);
            }

            if (crearprov.Checked == true || modifiprov.Checked == true || consulprov.Checked == true)
            {
                modulo1.Add(Taller.Text);
                modulo2.Add(proveedores.Text);
                crear.Add(crearprov.Checked == true ? 1 : 0);
                modificar.Add(modifiprov.Checked == true ? 1 : 0);
                consultar.Add(consulprov.Checked == true ? 1 : 0);
                insertar.Add(0);
            }

            if (crearprov.Checked == false || modifiprov.Checked == false || consulprov.Checked == false)
            {
                modulo1.Add(Taller.Text);
                modulo2.Add(proveedores.Text);
                crear.Add(crearprov.Checked == true ? 1 : 0);
                modificar.Add(modifiprov.Checked == true ? 1 : 0);
                consultar.Add(consulprov.Checked == true ? 1 : 0);
                insertar.Add(0);
            }

            if (crearinsu.Checked == true || Inserinsu.Checked == true || modifiinsu.Checked == true || consulinsu.Checked == true)
            {
                modulo1.Add(Taller.Text);
                modulo2.Add(INSUMOS.Text);
                crear.Add(crearinsu.Checked == true ? 1 : 0);
                modificar.Add(modifiinsu.Checked == true ? 1 : 0);
                consultar.Add(consulinsu.Checked == true ? 1 : 0);
                insertar.Add(Inserinsu.Checked == true ? 1 : 0);
            }

            if (crearinsu.Checked == false || Inserinsu.Checked == false || modifiinsu.Checked == false || consulinsu.Checked == false)
            {
                modulo1.Add(Taller.Text);
                modulo2.Add(INSUMOS.Text);
                crear.Add(crearinsu.Checked == true ? 1 : 0);
                modificar.Add(modifiinsu.Checked == true ? 1 : 0);
                consultar.Add(consulinsu.Checked == true ? 1 : 0);
                insertar.Add(Inserinsu.Checked == true ? 1 : 0);
            }

            if (crearficha.Checked == true || modifificha.Checked == true || consulficha.Checked == true)
            {
                modulo1.Add(Taller.Text);
                modulo2.Add(FICHATECNICA.Text);
                crear.Add(crearficha.Checked == true ? 1 : 0);
                modificar.Add(modifificha.Checked == true ? 1 : 0);
                consultar.Add(consulficha.Checked == true ? 1 : 0);
                insertar.Add(0);
            }

            if (crearficha.Checked == false || modifificha.Checked == false || consulficha.Checked == false)
            {
                modulo1.Add(Taller.Text);
                modulo2.Add(FICHATECNICA.Text);
                crear.Add(crearficha.Checked == true ? 1 : 0);
                modificar.Add(modifificha.Checked == true ? 1 : 0);
                consultar.Add(consulficha.Checked == true ? 1 : 0);
                insertar.Add(0);
            }

            if (crearproduc.Checked == true || inserpro.Checked == true || modifiproduct.Checked == true || consulproduct.Checked == true)
            {
                modulo1.Add(Taller.Text);
                modulo2.Add(PRODUCTOS.Text);
                crear.Add(crearproduc.Checked == true ? 1 : 0);
                modificar.Add(modifiproduct.Checked == true ? 1 : 0);
                consultar.Add(consulproduct.Checked == true ? 1 : 0);
                insertar.Add(inserpro.Checked == true ? 1 : 0);
            }

            if (crearproduc.Checked == false || inserpro.Checked == true || modifiproduct.Checked == false || consulproduct.Checked == false)
            {
                modulo1.Add(Taller.Text);
                modulo2.Add(PRODUCTOS.Text);
                crear.Add(crearproduc.Checked == true ? 1 : 0);
                modificar.Add(modifiproduct.Checked == true ? 1 : 0);
                consultar.Add(consulproduct.Checked == true ? 1 : 0);
                insertar.Add(inserpro.Checked == true ? 1 : 0);
            }

            DataTable tabla = obj.codusua(USUARIO.Text);

            obj.Usuario = int.Parse(tabla.Rows[0][0].ToString());
            obj.Usuario2 = idusuario;

            foreach (string mol1 in modulo1)
            {
                Thread.Sleep(200);
                obj.Modulo1 = mol1;
                obj.Modulo2 = modulo2[i];
                obj.Crear = crear[i];
                obj.Modificar = modificar[i];
                obj.Consultar = consultar[i];
                obj.Insertar = insertar[i];
                try
                {
                    if (obj.actualizar() >= 1)
                    {
                        i++;
                    }
                    else
                    {
                        if (obj.inspermi() >= 1)
                        {
                            i++;
                        }
                        else
                        {
                            MessageBox.Show("Se presento un error, revise que los campos esten completos");
                            break;
                        }

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Se a presentado un error inesperado, llamar al apoyo tecnico, error numero" + ex);
                    break;
                }
            }

            if (i >= 1)
            {
                MessageBox.Show("Los permisos fueron actualizados correctamente");
            }

            MODIFICAR.Enabled = true;
            Guardar.Enabled = true;
            ACTUALIZAR.Enabled = false;

        }

        private void modifiprove(object sender, EventArgs e)
        {
            consulprov.Checked = modifiprov.Checked == true ? true : false;
        }

        private void modificarinsu(object sender, EventArgs e)
        {
            consulinsu.Checked = modifiinsu.Checked == true ? true : false;
        }

        private void modificarficha(object sender, EventArgs e)
        {
            consulficha.Checked = modifificha.Checked == true ? true : false;
        }

        private void modificarprod(object sender, EventArgs e)
        {
            consulproduct.Checked = modifiproduct.Checked == true ? true : false;
        }

        private void cargarpermi(object sender, EventArgs e)
        {
            DataTable permi = obj.asignarpermi("PERMISOS",idusuario);

            Guardar.Visible = permi.Rows[0][0].Equals(true) ? true : false;
            ACTUALIZAR.Visible = permi.Rows[0][1].Equals(true) ? true : false;
            consultar = permi.Rows[0][2].Equals(true) ? 1 : 0;
        }
    }
}
