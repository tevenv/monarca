﻿namespace Colorful_visual
{
    partial class inscripcion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.EMPRESA = new System.Windows.Forms.Panel();
            this.contraadmin = new System.Windows.Forms.TextBox();
            this.Logo = new System.Windows.Forms.PictureBox();
            this.Subir = new System.Windows.Forms.Button();
            this.Guardar = new System.Windows.Forms.Button();
            this.usuaadmin = new System.Windows.Forms.TextBox();
            this.teleadmin = new System.Windows.Forms.TextBox();
            this.identifadmin = new System.Windows.Forms.TextBox();
            this.apellidoadmin = new System.Windows.Forms.TextBox();
            this.nomadmin = new System.Windows.Forms.TextBox();
            this.sloganempre = new System.Windows.Forms.TextBox();
            this.direccionempre = new System.Windows.Forms.TextBox();
            this.telempre = new System.Windows.Forms.TextBox();
            this.nitempre = new System.Windows.Forms.TextBox();
            this.nomempre = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.MINI = new System.Windows.Forms.Button();
            this.CERRAR = new System.Windows.Forms.Button();
            this.cargar = new System.Windows.Forms.OpenFileDialog();
            this.EMPRESA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // EMPRESA
            // 
            this.EMPRESA.BackColor = System.Drawing.Color.Transparent;
            this.EMPRESA.Controls.Add(this.contraadmin);
            this.EMPRESA.Controls.Add(this.Logo);
            this.EMPRESA.Controls.Add(this.Subir);
            this.EMPRESA.Controls.Add(this.Guardar);
            this.EMPRESA.Controls.Add(this.usuaadmin);
            this.EMPRESA.Controls.Add(this.teleadmin);
            this.EMPRESA.Controls.Add(this.identifadmin);
            this.EMPRESA.Controls.Add(this.apellidoadmin);
            this.EMPRESA.Controls.Add(this.nomadmin);
            this.EMPRESA.Controls.Add(this.sloganempre);
            this.EMPRESA.Controls.Add(this.direccionempre);
            this.EMPRESA.Controls.Add(this.telempre);
            this.EMPRESA.Controls.Add(this.nitempre);
            this.EMPRESA.Controls.Add(this.nomempre);
            this.EMPRESA.Controls.Add(this.label7);
            this.EMPRESA.Controls.Add(this.label8);
            this.EMPRESA.Controls.Add(this.label9);
            this.EMPRESA.Controls.Add(this.label10);
            this.EMPRESA.Controls.Add(this.label11);
            this.EMPRESA.Controls.Add(this.label12);
            this.EMPRESA.Controls.Add(this.label6);
            this.EMPRESA.Controls.Add(this.label5);
            this.EMPRESA.Controls.Add(this.label4);
            this.EMPRESA.Controls.Add(this.label3);
            this.EMPRESA.Controls.Add(this.label2);
            this.EMPRESA.Controls.Add(this.label14);
            this.EMPRESA.Controls.Add(this.label13);
            this.EMPRESA.Controls.Add(this.label1);
            this.EMPRESA.Location = new System.Drawing.Point(2, 68);
            this.EMPRESA.Margin = new System.Windows.Forms.Padding(2);
            this.EMPRESA.Name = "EMPRESA";
            this.EMPRESA.Size = new System.Drawing.Size(1025, 559);
            this.EMPRESA.TabIndex = 6;
            // 
            // contraadmin
            // 
            this.contraadmin.BackColor = System.Drawing.Color.White;
            this.contraadmin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.contraadmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contraadmin.Location = new System.Drawing.Point(839, 403);
            this.contraadmin.Multiline = true;
            this.contraadmin.Name = "contraadmin";
            this.contraadmin.PasswordChar = '*';
            this.contraadmin.Size = new System.Drawing.Size(133, 22);
            this.contraadmin.TabIndex = 1;
            // 
            // Logo
            // 
            this.Logo.BackColor = System.Drawing.Color.White;
            this.Logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Logo.Location = new System.Drawing.Point(201, 392);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(90, 90);
            this.Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Logo.TabIndex = 2;
            this.Logo.TabStop = false;
            // 
            // Subir
            // 
            this.Subir.BackgroundImage = global::Colorful_visual.Properties.Resources.subir1;
            this.Subir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Subir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Subir.FlatAppearance.BorderSize = 0;
            this.Subir.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Subir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Subir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Subir.Location = new System.Drawing.Point(297, 400);
            this.Subir.Name = "Subir";
            this.Subir.Size = new System.Drawing.Size(72, 79);
            this.Subir.TabIndex = 43;
            this.Subir.UseVisualStyleBackColor = true;
            this.Subir.Click += new System.EventHandler(this.Subir_Click);
            // 
            // Guardar
            // 
            this.Guardar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(69)))), ((int)(((byte)(185)))));
            this.Guardar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Guardar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Guardar.FlatAppearance.BorderSize = 0;
            this.Guardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Guardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Guardar.ForeColor = System.Drawing.Color.White;
            this.Guardar.Location = new System.Drawing.Point(389, 516);
            this.Guardar.Name = "Guardar";
            this.Guardar.Size = new System.Drawing.Size(208, 50);
            this.Guardar.TabIndex = 42;
            this.Guardar.Text = "GUARDAR";
            this.Guardar.UseVisualStyleBackColor = false;
            this.Guardar.Click += new System.EventHandler(this.Guardar_Click);
            // 
            // usuaadmin
            // 
            this.usuaadmin.BackColor = System.Drawing.Color.White;
            this.usuaadmin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.usuaadmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usuaadmin.Location = new System.Drawing.Point(839, 350);
            this.usuaadmin.Margin = new System.Windows.Forms.Padding(2);
            this.usuaadmin.Multiline = true;
            this.usuaadmin.Name = "usuaadmin";
            this.usuaadmin.Size = new System.Drawing.Size(133, 22);
            this.usuaadmin.TabIndex = 40;
            // 
            // teleadmin
            // 
            this.teleadmin.BackColor = System.Drawing.Color.White;
            this.teleadmin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.teleadmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teleadmin.Location = new System.Drawing.Point(839, 299);
            this.teleadmin.Margin = new System.Windows.Forms.Padding(2);
            this.teleadmin.Multiline = true;
            this.teleadmin.Name = "teleadmin";
            this.teleadmin.Size = new System.Drawing.Size(133, 22);
            this.teleadmin.TabIndex = 39;
            // 
            // identifadmin
            // 
            this.identifadmin.BackColor = System.Drawing.Color.White;
            this.identifadmin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.identifadmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.identifadmin.Location = new System.Drawing.Point(839, 242);
            this.identifadmin.Margin = new System.Windows.Forms.Padding(2);
            this.identifadmin.Multiline = true;
            this.identifadmin.Name = "identifadmin";
            this.identifadmin.Size = new System.Drawing.Size(133, 22);
            this.identifadmin.TabIndex = 38;
            // 
            // apellidoadmin
            // 
            this.apellidoadmin.BackColor = System.Drawing.Color.White;
            this.apellidoadmin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.apellidoadmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apellidoadmin.Location = new System.Drawing.Point(839, 185);
            this.apellidoadmin.Margin = new System.Windows.Forms.Padding(2);
            this.apellidoadmin.Multiline = true;
            this.apellidoadmin.Name = "apellidoadmin";
            this.apellidoadmin.Size = new System.Drawing.Size(133, 22);
            this.apellidoadmin.TabIndex = 37;
            // 
            // nomadmin
            // 
            this.nomadmin.BackColor = System.Drawing.Color.White;
            this.nomadmin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nomadmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nomadmin.Location = new System.Drawing.Point(839, 128);
            this.nomadmin.Margin = new System.Windows.Forms.Padding(2);
            this.nomadmin.Multiline = true;
            this.nomadmin.Name = "nomadmin";
            this.nomadmin.Size = new System.Drawing.Size(133, 22);
            this.nomadmin.TabIndex = 36;
            // 
            // sloganempre
            // 
            this.sloganempre.BackColor = System.Drawing.Color.White;
            this.sloganempre.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.sloganempre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sloganempre.Location = new System.Drawing.Point(201, 340);
            this.sloganempre.Margin = new System.Windows.Forms.Padding(2);
            this.sloganempre.Multiline = true;
            this.sloganempre.Name = "sloganempre";
            this.sloganempre.Size = new System.Drawing.Size(133, 22);
            this.sloganempre.TabIndex = 33;
            // 
            // direccionempre
            // 
            this.direccionempre.BackColor = System.Drawing.Color.White;
            this.direccionempre.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.direccionempre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.direccionempre.Location = new System.Drawing.Point(201, 289);
            this.direccionempre.Margin = new System.Windows.Forms.Padding(2);
            this.direccionempre.Multiline = true;
            this.direccionempre.Name = "direccionempre";
            this.direccionempre.Size = new System.Drawing.Size(133, 22);
            this.direccionempre.TabIndex = 32;
            // 
            // telempre
            // 
            this.telempre.BackColor = System.Drawing.Color.White;
            this.telempre.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.telempre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.telempre.Location = new System.Drawing.Point(201, 238);
            this.telempre.Margin = new System.Windows.Forms.Padding(2);
            this.telempre.Multiline = true;
            this.telempre.Name = "telempre";
            this.telempre.Size = new System.Drawing.Size(133, 22);
            this.telempre.TabIndex = 31;
            // 
            // nitempre
            // 
            this.nitempre.BackColor = System.Drawing.Color.White;
            this.nitempre.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nitempre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nitempre.Location = new System.Drawing.Point(201, 185);
            this.nitempre.Margin = new System.Windows.Forms.Padding(2);
            this.nitempre.Multiline = true;
            this.nitempre.Name = "nitempre";
            this.nitempre.Size = new System.Drawing.Size(133, 22);
            this.nitempre.TabIndex = 2;
            // 
            // nomempre
            // 
            this.nomempre.BackColor = System.Drawing.Color.White;
            this.nomempre.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nomempre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nomempre.Location = new System.Drawing.Point(201, 131);
            this.nomempre.Margin = new System.Windows.Forms.Padding(2);
            this.nomempre.Multiline = true;
            this.nomempre.Name = "nomempre";
            this.nomempre.Size = new System.Drawing.Size(133, 22);
            this.nomempre.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(555, 392);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(214, 31);
            this.label7.TabIndex = 25;
            this.label7.Text = "CONTRASEÑA";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(567, 340);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(146, 31);
            this.label8.TabIndex = 24;
            this.label8.Text = "USUARIO";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(567, 289);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(169, 31);
            this.label9.TabIndex = 23;
            this.label9.Text = "TELEFONO";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(567, 237);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(251, 31);
            this.label10.TabIndex = 22;
            this.label10.Text = "IDENTIFICACION";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(567, 185);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(174, 31);
            this.label11.TabIndex = 21;
            this.label11.Text = "APELLIDOS";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(567, 128);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(139, 31);
            this.label12.TabIndex = 20;
            this.label12.Text = "NOMBRE";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(23, 340);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(133, 31);
            this.label6.TabIndex = 19;
            this.label6.Text = "SLOGAN";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(23, 392);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 31);
            this.label5.TabIndex = 18;
            this.label5.Text = "LOGO";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(23, 289);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(178, 31);
            this.label4.TabIndex = 17;
            this.label4.Text = "DIRECCION";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(23, 238);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(169, 31);
            this.label3.TabIndex = 16;
            this.label3.Text = "TELEFONO";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(23, 185);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 31);
            this.label2.TabIndex = 15;
            this.label2.Text = "NIT";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(605, 19);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(364, 31);
            this.label14.TabIndex = 13;
            this.label14.Text = "DATOS ADMINISTRADOR";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label13.Location = new System.Drawing.Point(54, 19);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(276, 33);
            this.label13.TabIndex = 12;
            this.label13.Text = "DATOS EMPRESA";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 128);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "NOMBRE";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1038, 48);
            this.panel1.TabIndex = 7;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.MINI);
            this.panel2.Controls.Add(this.CERRAR);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(942, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(96, 48);
            this.panel2.TabIndex = 0;
            // 
            // MINI
            // 
            this.MINI.BackColor = System.Drawing.Color.Transparent;
            this.MINI.BackgroundImage = global::Colorful_visual.Properties.Resources.minimizar1;
            this.MINI.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MINI.FlatAppearance.BorderSize = 0;
            this.MINI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MINI.Location = new System.Drawing.Point(3, 3);
            this.MINI.Name = "MINI";
            this.MINI.Size = new System.Drawing.Size(40, 40);
            this.MINI.TabIndex = 7;
            this.MINI.UseVisualStyleBackColor = false;
            this.MINI.Click += new System.EventHandler(this.MINI_Click);
            // 
            // CERRAR
            // 
            this.CERRAR.BackgroundImage = global::Colorful_visual.Properties.Resources.cerrar1;
            this.CERRAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CERRAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CERRAR.FlatAppearance.BorderSize = 0;
            this.CERRAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CERRAR.Location = new System.Drawing.Point(49, 3);
            this.CERRAR.Name = "CERRAR";
            this.CERRAR.Size = new System.Drawing.Size(40, 40);
            this.CERRAR.TabIndex = 6;
            this.CERRAR.UseVisualStyleBackColor = false;
            this.CERRAR.Click += new System.EventHandler(this.CERRAR_Click);
            // 
            // cargar
            // 
            this.cargar.FileName = "cargar";
            this.cargar.Filter = "ARCHIVOS PNG(*.png)|*.png|ARCHIVOS JPG(*.jpg)|*.jpg";
            // 
            // inscripcion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(166)))), ((int)(((byte)(216)))));
            this.ClientSize = new System.Drawing.Size(1038, 630);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.EMPRESA);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "inscripcion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "inscripcion";
            this.EMPRESA.ResumeLayout(false);
            this.EMPRESA.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel EMPRESA;
        private System.Windows.Forms.TextBox contraadmin;
        private System.Windows.Forms.PictureBox Logo;
        private System.Windows.Forms.Button Subir;
        private System.Windows.Forms.Button Guardar;
        private System.Windows.Forms.TextBox usuaadmin;
        private System.Windows.Forms.TextBox teleadmin;
        private System.Windows.Forms.TextBox identifadmin;
        private System.Windows.Forms.TextBox apellidoadmin;
        private System.Windows.Forms.TextBox nomadmin;
        private System.Windows.Forms.TextBox sloganempre;
        private System.Windows.Forms.TextBox direccionempre;
        private System.Windows.Forms.TextBox telempre;
        private System.Windows.Forms.TextBox nitempre;
        private System.Windows.Forms.TextBox nomempre;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button MINI;
        private System.Windows.Forms.Button CERRAR;
        private System.Windows.Forms.OpenFileDialog cargar;
    }
}