﻿namespace ColorFul_visual
{
    partial class usuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.APELLIDO = new System.Windows.Forms.TextBox();
            this.CC = new System.Windows.Forms.TextBox();
            this.TELEFONO = new System.Windows.Forms.TextBox();
            this.USUA = new System.Windows.Forms.TextBox();
            this.CONTRA = new System.Windows.Forms.TextBox();
            this.MUESTRA = new System.Windows.Forms.ListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.ESTADO = new System.Windows.Forms.ComboBox();
            this.BUSCAR = new System.Windows.Forms.TextBox();
            this.uss = new System.Windows.Forms.Label();
            this.NOMBRE = new System.Windows.Forms.TextBox();
            this.MODIFICAR = new System.Windows.Forms.Button();
            this.BUSCA = new System.Windows.Forms.Button();
            this.GUARDAR = new System.Windows.Forms.Button();
            this.ACTUALIZAR = new System.Windows.Forms.Button();
            this.ERROR = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ERROR)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(43, 37);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(43, 96);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Apellido";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(43, 155);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(157, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Numero de identificacion";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(43, 220);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Telefono";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(43, 282);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Usuario";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(43, 355);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "Contraseña";
            // 
            // APELLIDO
            // 
            this.APELLIDO.BackColor = System.Drawing.Color.WhiteSmoke;
            this.APELLIDO.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.APELLIDO.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.APELLIDO.Location = new System.Drawing.Point(204, 96);
            this.APELLIDO.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.APELLIDO.Multiline = true;
            this.APELLIDO.Name = "APELLIDO";
            this.APELLIDO.Size = new System.Drawing.Size(141, 20);
            this.APELLIDO.TabIndex = 7;
            // 
            // CC
            // 
            this.CC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CC.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CC.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CC.Location = new System.Drawing.Point(204, 152);
            this.CC.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.CC.Multiline = true;
            this.CC.Name = "CC";
            this.CC.Size = new System.Drawing.Size(141, 23);
            this.CC.TabIndex = 8;
            // 
            // TELEFONO
            // 
            this.TELEFONO.BackColor = System.Drawing.Color.WhiteSmoke;
            this.TELEFONO.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TELEFONO.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TELEFONO.Location = new System.Drawing.Point(204, 217);
            this.TELEFONO.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.TELEFONO.Multiline = true;
            this.TELEFONO.Name = "TELEFONO";
            this.TELEFONO.Size = new System.Drawing.Size(141, 23);
            this.TELEFONO.TabIndex = 9;
            // 
            // USUA
            // 
            this.USUA.BackColor = System.Drawing.Color.WhiteSmoke;
            this.USUA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.USUA.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.USUA.Location = new System.Drawing.Point(204, 279);
            this.USUA.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.USUA.Multiline = true;
            this.USUA.Name = "USUA";
            this.USUA.Size = new System.Drawing.Size(141, 23);
            this.USUA.TabIndex = 10;
            // 
            // CONTRA
            // 
            this.CONTRA.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CONTRA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CONTRA.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CONTRA.Location = new System.Drawing.Point(204, 348);
            this.CONTRA.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.CONTRA.Multiline = true;
            this.CONTRA.Name = "CONTRA";
            this.CONTRA.Size = new System.Drawing.Size(141, 23);
            this.CONTRA.TabIndex = 11;
            // 
            // MUESTRA
            // 
            this.MUESTRA.BackColor = System.Drawing.Color.WhiteSmoke;
            this.MUESTRA.Font = new System.Drawing.Font("Raleway Medium", 8.999999F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MUESTRA.FormattingEnabled = true;
            this.MUESTRA.ItemHeight = 14;
            this.MUESTRA.Location = new System.Drawing.Point(427, 80);
            this.MUESTRA.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MUESTRA.Name = "MUESTRA";
            this.MUESTRA.Size = new System.Drawing.Size(220, 242);
            this.MUESTRA.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(424, 358);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 15);
            this.label7.TabIndex = 15;
            this.label7.Text = "Estado";
            // 
            // ESTADO
            // 
            this.ESTADO.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ESTADO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ESTADO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ESTADO.Font = new System.Drawing.Font("Raleway Light", 8.249999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ESTADO.FormattingEnabled = true;
            this.ESTADO.Items.AddRange(new object[] {
            "Activo",
            "Inactivo"});
            this.ESTADO.Location = new System.Drawing.Point(498, 355);
            this.ESTADO.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ESTADO.Name = "ESTADO";
            this.ESTADO.Size = new System.Drawing.Size(148, 21);
            this.ESTADO.TabIndex = 17;
            // 
            // BUSCAR
            // 
            this.BUSCAR.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BUSCAR.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.BUSCAR.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BUSCAR.Location = new System.Drawing.Point(479, 36);
            this.BUSCAR.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.BUSCAR.Multiline = true;
            this.BUSCAR.Name = "BUSCAR";
            this.BUSCAR.Size = new System.Drawing.Size(124, 23);
            this.BUSCAR.TabIndex = 18;
            // 
            // uss
            // 
            this.uss.AutoSize = true;
            this.uss.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uss.Location = new System.Drawing.Point(420, 39);
            this.uss.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.uss.Name = "uss";
            this.uss.Size = new System.Drawing.Size(53, 15);
            this.uss.TabIndex = 19;
            this.uss.Text = "Usuario";
            // 
            // NOMBRE
            // 
            this.NOMBRE.BackColor = System.Drawing.Color.WhiteSmoke;
            this.NOMBRE.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NOMBRE.Location = new System.Drawing.Point(204, 39);
            this.NOMBRE.Multiline = true;
            this.NOMBRE.Name = "NOMBRE";
            this.NOMBRE.Size = new System.Drawing.Size(141, 20);
            this.NOMBRE.TabIndex = 20;
            // 
            // MODIFICAR
            // 
            this.MODIFICAR.BackgroundImage = global::Colorful_visual.Properties.Resources.MODIFICAR;
            this.MODIFICAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MODIFICAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MODIFICAR.FlatAppearance.BorderSize = 0;
            this.MODIFICAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MODIFICAR.Location = new System.Drawing.Point(281, 409);
            this.MODIFICAR.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MODIFICAR.Name = "MODIFICAR";
            this.MODIFICAR.Size = new System.Drawing.Size(95, 32);
            this.MODIFICAR.TabIndex = 16;
            this.MODIFICAR.UseVisualStyleBackColor = true;
            this.MODIFICAR.Click += new System.EventHandler(this.MODIFICAR_Click);
            // 
            // BUSCA
            // 
            this.BUSCA.BackgroundImage = global::Colorful_visual.Properties.Resources.buscar;
            this.BUSCA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BUSCA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BUSCA.FlatAppearance.BorderSize = 0;
            this.BUSCA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BUSCA.Location = new System.Drawing.Point(610, 36);
            this.BUSCA.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.BUSCA.Name = "BUSCA";
            this.BUSCA.Size = new System.Drawing.Size(24, 24);
            this.BUSCA.TabIndex = 13;
            this.BUSCA.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.BUSCA.UseVisualStyleBackColor = true;
            this.BUSCA.Click += new System.EventHandler(this.BUSCA_Click);
            // 
            // GUARDAR
            // 
            this.GUARDAR.BackgroundImage = global::Colorful_visual.Properties.Resources.GUARDAR;
            this.GUARDAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.GUARDAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.GUARDAR.FlatAppearance.BorderSize = 0;
            this.GUARDAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GUARDAR.Location = new System.Drawing.Point(159, 409);
            this.GUARDAR.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.GUARDAR.Name = "GUARDAR";
            this.GUARDAR.Size = new System.Drawing.Size(95, 32);
            this.GUARDAR.TabIndex = 12;
            this.GUARDAR.UseVisualStyleBackColor = true;
            this.GUARDAR.Click += new System.EventHandler(this.GUARDAR_Click);
            // 
            // ACTUALIZAR
            // 
            this.ACTUALIZAR.BackgroundImage = global::Colorful_visual.Properties.Resources.ACTUALIZAR;
            this.ACTUALIZAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ACTUALIZAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ACTUALIZAR.FlatAppearance.BorderSize = 0;
            this.ACTUALIZAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ACTUALIZAR.Location = new System.Drawing.Point(406, 409);
            this.ACTUALIZAR.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ACTUALIZAR.Name = "ACTUALIZAR";
            this.ACTUALIZAR.Size = new System.Drawing.Size(95, 32);
            this.ACTUALIZAR.TabIndex = 21;
            this.ACTUALIZAR.UseVisualStyleBackColor = true;
            this.ACTUALIZAR.Click += new System.EventHandler(this.ACTUALIZAR_Click);
            // 
            // ERROR
            // 
            this.ERROR.ContainerControl = this;
            // 
            // usuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(700, 451);
            this.Controls.Add(this.ACTUALIZAR);
            this.Controls.Add(this.NOMBRE);
            this.Controls.Add(this.uss);
            this.Controls.Add(this.BUSCAR);
            this.Controls.Add(this.ESTADO);
            this.Controls.Add(this.MODIFICAR);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.MUESTRA);
            this.Controls.Add(this.BUSCA);
            this.Controls.Add(this.GUARDAR);
            this.Controls.Add(this.CONTRA);
            this.Controls.Add(this.USUA);
            this.Controls.Add(this.TELEFONO);
            this.Controls.Add(this.CC);
            this.Controls.Add(this.APELLIDO);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Raleway Light", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "usuario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "usuario";
            this.TransparencyKey = System.Drawing.Color.Transparent;
            this.Load += new System.EventHandler(this.iniciarpermi);
            ((System.ComponentModel.ISupportInitialize)(this.ERROR)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox APELLIDO;
        private System.Windows.Forms.TextBox CC;
        private System.Windows.Forms.TextBox TELEFONO;
        private System.Windows.Forms.TextBox USUA;
        private System.Windows.Forms.TextBox CONTRA;
        private System.Windows.Forms.Button GUARDAR;
        private System.Windows.Forms.Button BUSCA;
        private System.Windows.Forms.ListBox MUESTRA;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button MODIFICAR;
        private System.Windows.Forms.ComboBox ESTADO;
        private System.Windows.Forms.TextBox BUSCAR;
        private System.Windows.Forms.Label uss;
        private System.Windows.Forms.TextBox NOMBRE;
        private System.Windows.Forms.Button ACTUALIZAR;
        private System.Windows.Forms.ErrorProvider ERROR;
    }
}