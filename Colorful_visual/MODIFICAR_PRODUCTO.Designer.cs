﻿namespace Colorful_visual
{
    partial class MODIFICAR_PRODUCTO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MUESTRA = new System.Windows.Forms.ListBox();
            this.ACTUALIZAR = new System.Windows.Forms.Button();
            this.ESTADO = new System.Windows.Forms.ComboBox();
            this.CANTIDAD = new System.Windows.Forms.TextBox();
            this.MODIFICAR = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.BUSCAR = new System.Windows.Forms.Button();
            this.BUSCA = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // MUESTRA
            // 
            this.MUESTRA.BackColor = System.Drawing.Color.WhiteSmoke;
            this.MUESTRA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MUESTRA.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MUESTRA.FormattingEnabled = true;
            this.MUESTRA.ItemHeight = 15;
            this.MUESTRA.Location = new System.Drawing.Point(346, 100);
            this.MUESTRA.Name = "MUESTRA";
            this.MUESTRA.Size = new System.Drawing.Size(178, 120);
            this.MUESTRA.TabIndex = 85;
            // 
            // ACTUALIZAR
            // 
            this.ACTUALIZAR.BackColor = System.Drawing.Color.Transparent;
            this.ACTUALIZAR.BackgroundImage = global::Colorful_visual.Properties.Resources.ACTUALIZAR;
            this.ACTUALIZAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ACTUALIZAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ACTUALIZAR.FlatAppearance.BorderSize = 0;
            this.ACTUALIZAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ACTUALIZAR.Location = new System.Drawing.Point(184, 188);
            this.ACTUALIZAR.Name = "ACTUALIZAR";
            this.ACTUALIZAR.Size = new System.Drawing.Size(95, 32);
            this.ACTUALIZAR.TabIndex = 84;
            this.ACTUALIZAR.UseVisualStyleBackColor = false;
            this.ACTUALIZAR.Click += new System.EventHandler(this.ACTUALIZAR_Click);
            // 
            // ESTADO
            // 
            this.ESTADO.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ESTADO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ESTADO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ESTADO.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ESTADO.FormattingEnabled = true;
            this.ESTADO.Items.AddRange(new object[] {
            "Activo",
            "Inactivo"});
            this.ESTADO.Location = new System.Drawing.Point(103, 138);
            this.ESTADO.Name = "ESTADO";
            this.ESTADO.Size = new System.Drawing.Size(166, 23);
            this.ESTADO.TabIndex = 83;
            // 
            // CANTIDAD
            // 
            this.CANTIDAD.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CANTIDAD.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CANTIDAD.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CANTIDAD.Location = new System.Drawing.Point(103, 100);
            this.CANTIDAD.Multiline = true;
            this.CANTIDAD.Name = "CANTIDAD";
            this.CANTIDAD.Size = new System.Drawing.Size(166, 20);
            this.CANTIDAD.TabIndex = 82;
            // 
            // MODIFICAR
            // 
            this.MODIFICAR.BackColor = System.Drawing.Color.Transparent;
            this.MODIFICAR.BackgroundImage = global::Colorful_visual.Properties.Resources.MODIFICAR;
            this.MODIFICAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MODIFICAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MODIFICAR.FlatAppearance.BorderSize = 0;
            this.MODIFICAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MODIFICAR.Location = new System.Drawing.Point(57, 188);
            this.MODIFICAR.Name = "MODIFICAR";
            this.MODIFICAR.Size = new System.Drawing.Size(95, 32);
            this.MODIFICAR.TabIndex = 81;
            this.MODIFICAR.UseVisualStyleBackColor = false;
            this.MODIFICAR.Click += new System.EventHandler(this.MODIFICAR_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(34, 142);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 14);
            this.label6.TabIndex = 80;
            this.label6.Text = "Estado";
            // 
            // BUSCAR
            // 
            this.BUSCAR.BackColor = System.Drawing.Color.Transparent;
            this.BUSCAR.BackgroundImage = global::Colorful_visual.Properties.Resources.buscar;
            this.BUSCAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BUSCAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BUSCAR.FlatAppearance.BorderSize = 0;
            this.BUSCAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BUSCAR.Location = new System.Drawing.Point(486, 50);
            this.BUSCAR.Name = "BUSCAR";
            this.BUSCAR.Size = new System.Drawing.Size(24, 24);
            this.BUSCAR.TabIndex = 79;
            this.BUSCAR.UseVisualStyleBackColor = false;
            this.BUSCAR.Click += new System.EventHandler(this.BUSCAR_Click);
            // 
            // BUSCA
            // 
            this.BUSCA.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BUSCA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.BUSCA.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BUSCA.Location = new System.Drawing.Point(384, 52);
            this.BUSCA.Multiline = true;
            this.BUSCA.Name = "BUSCA";
            this.BUSCA.Size = new System.Drawing.Size(94, 20);
            this.BUSCA.TabIndex = 78;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(334, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 14);
            this.label5.TabIndex = 77;
            this.label5.Text = "Insumo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(34, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 14);
            this.label2.TabIndex = 76;
            this.label2.Text = "Cantidad";
            // 
            // MODIFICAR_PRODUCTO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(558, 328);
            this.Controls.Add(this.MUESTRA);
            this.Controls.Add(this.ACTUALIZAR);
            this.Controls.Add(this.ESTADO);
            this.Controls.Add(this.CANTIDAD);
            this.Controls.Add(this.MODIFICAR);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.BUSCAR);
            this.Controls.Add(this.BUSCA);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MODIFICAR_PRODUCTO";
            this.Text = "MODIFICAR_PRODUCTO";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox MUESTRA;
        private System.Windows.Forms.Button ACTUALIZAR;
        private System.Windows.Forms.ComboBox ESTADO;
        private System.Windows.Forms.TextBox CANTIDAD;
        private System.Windows.Forms.Button MODIFICAR;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button BUSCAR;
        private System.Windows.Forms.TextBox BUSCA;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
    }
}