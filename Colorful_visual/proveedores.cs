﻿using Colorful_visual;


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Colorfull;

namespace ColorFul_visual
{
    public partial class proveedores : Form
    {
        proveedor obj = new proveedor();
        validador aux = new validador();
        int idusuario;
        
        public proveedores(int idusuario)
        {
            this.idusuario = idusuario;
            InitializeComponent();
            MODIFICAR.Enabled = false;
            ACTUALIZAR.Enabled = false;
        }

        private void GUARDAR_Click(object sender, EventArgs e)
        {
           if(string.IsNullOrEmpty(NOMBRE.Text))
            {
                ERROR.SetError(NOMBRE, "Valor requerido");
            }
            else
            {
                
                if (aux.validarNIT(NIT.Text))
                {
                    if (aux.validarNumeroCelular(TELEFONO.Text))
                    {
                        obj.Nombre = NOMBRE.Text;
                        obj.Nit = NIT.Text;
                        obj.Telefono = TELEFONO.Text;
                        obj.Direccion = DIRECCION.Text;
                        obj.Correo = CORREO.Text;
                        if (Estado.Text == "Activo" || Estado.Text == "Inactivo")
                        {
                            Estado.SelectionLength = 0;
                            obj.Estado = (Estado.Text.Equals("Activo")) ? 1 : 0;

                            obj.Usuario = idusuario;
                            this.MUESTRA.Items.Clear();

                            try
                            {

                                if (obj.insrpro() >= 1)
                                {
                                    DataTable tabla = obj.proveed(NIT.Text);

                                    for (int i = 0; i < tabla.Columns.Count; i++)
                                    {
                                        MUESTRA.Items.Add(tabla.Rows[0][i].ToString());
                                        MUESTRA.Items.Add(" ");

                                    }

                                }
                                else
                                {
                                    if (obj.insrpro() == -2)
                                    {
                                        MessageBox.Show("El proveedor ya existe, revisa en la lista de proveedores con el NIT o el Nombre para poder obtener mas información ");
                                    }
                                    else
                                    {
                                      MessageBox.Show("Se presento un error inesperado, revise que los datos esten completos.");
                                    }
                                    
                                }

                            }
                            catch (Exception ex)
                            {
                                
                                MessageBox.Show("Se a presentado un error inesperado, llamar al apoyo tecnico, error numero" + ex);
                            }
                            this.limpiar(this);

                        }
                        else
                        {
                            MessageBox.Show("seleccione un estado");
                        }
                    }
                    else
                    {
                        ERROR.SetError(TELEFONO, "Valor requerido, ingrese un numero de valido, si el numero es de un telefo " +
                            "fijo recomendamos poner 03 + el indicativo de la ciudad + el numero de telefono");

                    }
                }
                else
                {
                    ERROR.SetError(NIT, "Valor requerido, ingrese un numero de NIT valido");
                }
            }



        }

        private void limpiar(Control aux)
        {
            foreach (var txt in aux.Controls)
            {
                if (txt is TextBox)
                {
                    ((TextBox)txt).Clear();

                }
                else if(txt is ComboBox)
                {
                    ((ComboBox)txt).SelectedIndex =-1;
                }
                                
            }

            ERROR.Clear();
        }

        private void BUSCA_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(BUSCAR.Text))
            {
                MessageBox.Show("Ingresa por favor un parametro de busqueda");
            }
            else
            {
                Form formulario = new Buscador(BUSCAR.Text, MUESTRA, 1);
                this.limpiar(this);
                this.MUESTRA.Items.Clear();
                formulario.Show();

            }
            GUARDAR.Enabled = false;
            MODIFICAR.Enabled = true;

           
        }

        private void MODIFICAR_Click(object sender, EventArgs e)
        {
            NOMBRE.Text = MUESTRA.Items[0].ToString();
            NIT.Text = MUESTRA.Items[2].ToString();
            TELEFONO.Text = MUESTRA.Items[4].ToString();
            DIRECCION.Text = MUESTRA.Items[6].ToString();
            CORREO.Text = MUESTRA.Items[8].ToString();
            if (MUESTRA.Items[10].Equals("True"))
            {
                Estado.SelectedIndex = 0;
            }
            else if((MUESTRA.Items[10].Equals("False")))
            {
                Estado.SelectedIndex = 1;
            }

            NOMBRE.Enabled = false;
            NIT.Enabled = false;
            TELEFONO.Focus();
            TELEFONO.SelectionLength = 0;
            MODIFICAR.Enabled = false;
            ACTUALIZAR.Enabled = true;
        }

        private void ACTUALIZAR_Click(object sender, EventArgs e)
        {
           
            obj.Nit = NIT.Text;
            obj.Telefono = TELEFONO.Text;
            obj.Direccion = DIRECCION.Text;
            obj.Correo = CORREO.Text;
            if (Estado.Text == "Activo" || Estado.Text == "Inactivo")
            {
                Estado.SelectionLength = 0;
                obj.Estado = (Estado.Text.Equals("Activo")) ? 1 : 0;

                obj.Usuario = idusuario;
                this.MUESTRA.Items.Clear();

                try
                {

                    if (obj.actuprovee() >= 1)
                    {
                        DataTable tabla = obj.proveed(NIT.Text);

                        for (int i = 0; i < tabla.Columns.Count; i++)
                        {
                            MUESTRA.Items.Add(tabla.Rows[0][i].ToString());
                            MUESTRA.Items.Add(" ");

                        }

                    }
                    else
                    {
                        MessageBox.Show("Se presento un error inesperado, revise que los datos esten completos.");
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Se a presentado un error inesperado, llamar al apoyo tecnico, error numero" + ex);
                }
                this.limpiar(this);

            }
            else
            {
                MessageBox.Show("seleccione un estado");
            }

            ACTUALIZAR.Enabled = false;
            GUARDAR.Enabled = true;

        }

        private void iniciarpermi(object sender, EventArgs e)
        {
            DataTable permi = obj.asignarpermi("PROVEEDORES", idusuario);


            GUARDAR.Visible = permi.Rows[0][0].Equals(true) ? true : false;
            MODIFICAR.Visible = permi.Rows[0][1].Equals(true) ? true : false;
            ACTUALIZAR.Visible = permi.Rows[0][1].Equals(true) ? true : false;
            BUSCA.Visible = permi.Rows[0][2].Equals(true) ? true : false;
            BUSCAR.Visible = permi.Rows[0][2].Equals(true) ? true : false;
            nitt.Visible = permi.Rows[0][2].Equals(true) ? true : false;

        }
    }
}
