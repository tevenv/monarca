﻿using Colorful_visual;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ColorFul_visual
{
    public partial class almacen : Form
    {
        int idusuario;
        DataTable permi;
        public almacen(int idusuario, DataTable permi)
        {
            this.idusuario = idusuario;
            this.permi = permi;
            InitializeComponent();
        }

        private void Abriprovee(object formadmin)
        {
            if (this.contenedor3.Controls.Count > 0)
            {
                this.contenedor3.Controls.RemoveAt(0);
            }

            Form formul = formadmin as Form;
            int ancho = (-formul.Size.Width + contenedor3.Width) / 2;
            int alto = (-formul.Size.Height + contenedor3.Height) / 2;

            formul.TopLevel = false;
            formul.Location = new Point(ancho, alto);
            this.contenedor3.Controls.Add(formul);
            this.contenedor3.Tag = formul;
            formul.Show();
        }

        private void abririnsu(object formadmin)
        {
            if (this.contenedor3.Controls.Count > 0)
            {
                this.contenedor3.Controls.RemoveAt(0);
            }

            Form formul = formadmin as Form;
            int ancho = (-formul.Size.Width + contenedor3.Width) / 2;
            int alto = (-formul.Size.Height + contenedor3.Height) / 2;

            formul.TopLevel = false;
            formul.Location = new Point(ancho, alto);
            this.contenedor3.Controls.Add(formul);
            this.contenedor3.Tag = formul;
            formul.Show();
        }

        private void abrirficha(object formadmin)
        {
            if (this.contenedor3.Controls.Count > 0)
            {
                this.contenedor3.Controls.RemoveAt(0);
            }

            Form formul = formadmin as Form;
            int ancho = (-formul.Size.Width + contenedor3.Width) / 2;
            int alto = (-formul.Size.Height + contenedor3.Height) / 2;

            formul.TopLevel = false;
            formul.Location = new Point(ancho, alto);
            this.contenedor3.Controls.Add(formul);
            this.contenedor3.Tag = formul;
            formul.Show();
        }

        private void abrirproducto(object formadmin)
        {
            if (this.contenedor3.Controls.Count > 0)
            {
                this.contenedor3.Controls.RemoveAt(0);
            }

            Form formul = formadmin as Form;
            int ancho = (-formul.Size.Width + contenedor3.Width) / 2;
            int alto = (-formul.Size.Height + contenedor3.Height) / 2;

            formul.TopLevel = false;
            formul.Location = new Point(ancho, alto);
            this.contenedor3.Controls.Add(formul);
            this.contenedor3.Tag = formul;
            formul.Show();
        }

        private void PROVEEDOR_Click(object sender, EventArgs e)
        {
            Abriprovee(new proveedores(idusuario));
        }

        private void INSUMOS_Click(object sender, EventArgs e)
        {
            abririnsu(new Insumos(idusuario));
        }

        private void FICHA_Click(object sender, EventArgs e)
        {
            abrirficha(new ficha(idusuario));
        }

        private void PRODUCTO_Click(object sender, EventArgs e)
        {
            abrirproducto(new Producto(idusuario));
        }

        private void iniciarpermi(object sender, EventArgs e)
        {
            for (int i = 0; i < permi.Rows.Count; i++)
            {
                for (int j = 2; j < 5; j++)
                {
                    // Thread.Sleep(200);
                    if (permi.Rows[i][1].Equals("PROVEEDORES") && permi.Rows[i][j].Equals(true))
                    {
                        PROVEEDOR.Enabled = true;
                        break;
                    }
                    else
                    {
                        PROVEEDOR.Enabled = false;
                    }
                }
                if(PROVEEDOR.Enabled == true)
                {
                    break;
                }
                
            }

            for (int i = 0; i < permi.Rows.Count; i++)
            {
                for (int j = 2; j < 6; j++)
                {
                    // Thread.Sleep(200);
                    if (permi.Rows[i][1].Equals("INSUMOS") && permi.Rows[i][j].Equals(true))
                    {
                        INSUMOS.Enabled = true;
                        break;
                    }
                    else
                    {
                        INSUMOS.Enabled = false;
                    }
                }
                if (INSUMOS.Enabled == true)
                {
                    break;
                }
            }

            for (int i = 0; i < permi.Rows.Count; i++)
            {
                for (int j = 2; j < 5; j++)
                {
                    // Thread.Sleep(200);
                    if (permi.Rows[i][1].Equals("FICHA TECNICA") && permi.Rows[i][j].Equals(true))
                    {
                        FICHA.Enabled = true;
                        break;
                    }
                    else
                    {
                        FICHA.Enabled = false;
                    }
                }
                if (FICHA.Enabled == true)
                {
                    break;
                }
            }

            for (int i = 0; i < permi.Rows.Count; i++)
            {
                for (int j = 2; j < 6; j++)
                {
                    // Thread.Sleep(200);
                    if (permi.Rows[i][1].Equals("PRODUCTOS") && permi.Rows[i][j].Equals(true))
                    {
                        PRODUCTO.Enabled = true;
                        break;
                    }
                    else
                    {
                        PRODUCTO.Enabled = false;
                    }
                }
                if (PRODUCTO.Enabled == true)
                {
                    break;
                }
            }
        }
        
    }
}
