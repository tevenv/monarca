﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Colorfull;

namespace Colorful_visual
{
    public partial class MODIFICAR_INSUMO : Form
    {
        AG_insu obj = new AG_insu();
        int idusuario;
        public MODIFICAR_INSUMO(int idusuario)
        {
            this.idusuario = idusuario;
            InitializeComponent();
        }

        private void BUSCAR_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(BUSCA.Text))
            {
                MessageBox.Show("Ingresa por favor un parametro de busqueda");
            }
            else
            {
                Form formulario = new Buscador(BUSCA.Text, MUESTRA, 2);
                this.limpiar(this);
                this.MUESTRA.Items.Clear();
                formulario.Show();

            }

            CANTIDAD.Enabled = false;
            ESTADO.Enabled = false;

        }

        private void limpiar(Control aux)
        {
            foreach (var txt in aux.Controls)
            {
                if (txt is TextBox)
                {
                    ((TextBox)txt).Clear();

                }
                else if (txt is ComboBox)
                {
                    ((ComboBox)txt).SelectedIndex = -1;
                }

            }

            ERROR.Clear();
        }

        private void MODIFICAR_Click(object sender, EventArgs e)
        {
            CANTIDAD.Text = MUESTRA.Items[6].ToString();
            if (MUESTRA.Items[8].Equals("True"))
            {
                ESTADO.SelectedIndex = 0;
            }
            if (MUESTRA.Items[8].Equals("False"))
            {
                ESTADO.SelectedIndex = 1;
            }
            CANTIDAD.Enabled = true;
            ESTADO.Enabled = true;
            CANTIDAD.Focus();
            CANTIDAD.SelectionLength = 0;
            
        }

        private void ACTUALIZAR_Click(object sender, EventArgs e)
        {
            obj.Cantidad = int.Parse(CANTIDAD.Text);
            obj.Codigo = MUESTRA.Items[0].ToString();
            string aux = MUESTRA.Items[0].ToString();
            if (ESTADO.SelectedIndex >= 0)
            {
                obj.Estado = (ESTADO.Text.Equals("Activo")) ? 1 : 0; 
                obj.Usuario = idusuario;
                this.MUESTRA.Items.Clear();
                try
                {
                    if (obj.actualizarinsumo() >= 1 && obj.almacenar("ACT", int.Parse(CANTIDAD.Text)) >= 1)
                    {
                        DataTable tabla = obj.consultarinsumo(aux);

                        for (int i = 0; i < tabla.Columns.Count; i++)
                        {
                            MUESTRA.Items.Add(tabla.Rows[0][i].ToString());
                            MUESTRA.Items.Add(" ");

                        }
                    }
                    else
                    {
                        MessageBox.Show("Se presento un error inesperado, revise que los datos esten completos.");
                    }

                    this.limpiar(this);

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Se a presentado un error inesperado, llamar al apoyo tecnico, error: " + ex);
                }


            }
            else
            {
                ERROR.SetError(CANTIDAD, "Valor requerido, solo se permiten numeros enteros");
            }

        }
        
    }
}
