﻿namespace Colorful_visual
{
    partial class stock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.Ingresar = new System.Windows.Forms.LinkLabel();
            this.Extraer = new System.Windows.Forms.LinkLabel();
            this.contenedor1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Ingresar);
            this.panel1.Controls.Add(this.Extraer);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(566, 27);
            this.panel1.TabIndex = 0;
            // 
            // Ingresar
            // 
            this.Ingresar.ActiveLinkColor = System.Drawing.Color.WhiteSmoke;
            this.Ingresar.AutoSize = true;
            this.Ingresar.Font = new System.Drawing.Font("Raleway Medium", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ingresar.LinkColor = System.Drawing.Color.Fuchsia;
            this.Ingresar.Location = new System.Drawing.Point(110, 3);
            this.Ingresar.Name = "Ingresar";
            this.Ingresar.Size = new System.Drawing.Size(114, 26);
            this.Ingresar.TabIndex = 5;
            this.Ingresar.TabStop = true;
            this.Ingresar.Text = "INGRESAR";
            this.Ingresar.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Ingresar_LinkClicked);
            // 
            // Extraer
            // 
            this.Extraer.ActiveLinkColor = System.Drawing.Color.WhiteSmoke;
            this.Extraer.AutoSize = true;
            this.Extraer.Font = new System.Drawing.Font("Raleway Medium", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Extraer.LinkColor = System.Drawing.Color.Fuchsia;
            this.Extraer.Location = new System.Drawing.Point(342, 3);
            this.Extraer.Name = "Extraer";
            this.Extraer.Size = new System.Drawing.Size(102, 26);
            this.Extraer.TabIndex = 6;
            this.Extraer.TabStop = true;
            this.Extraer.Text = "EXTRAER";
            this.Extraer.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Extraer_LinkClicked);
            // 
            // contenedor1
            // 
            this.contenedor1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contenedor1.Location = new System.Drawing.Point(0, 27);
            this.contenedor1.Name = "contenedor1";
            this.contenedor1.Size = new System.Drawing.Size(566, 327);
            this.contenedor1.TabIndex = 1;
            // 
            // stock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(566, 354);
            this.Controls.Add(this.contenedor1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "stock";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel contenedor1;
        private System.Windows.Forms.LinkLabel Extraer;
        private System.Windows.Forms.LinkLabel Ingresar;
    }
}