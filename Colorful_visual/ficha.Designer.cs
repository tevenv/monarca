﻿namespace Colorful_visual
{
    partial class ficha
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Modificar = new System.Windows.Forms.LinkLabel();
            this.Crear = new System.Windows.Forms.LinkLabel();
            this.contenedor4 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Modificar
            // 
            this.Modificar.ActiveLinkColor = System.Drawing.Color.WhiteSmoke;
            this.Modificar.AutoSize = true;
            this.Modificar.Font = new System.Drawing.Font("Raleway Medium", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Modificar.LinkColor = System.Drawing.Color.Fuchsia;
            this.Modificar.Location = new System.Drawing.Point(109, 2);
            this.Modificar.Name = "Modificar";
            this.Modificar.Size = new System.Drawing.Size(123, 22);
            this.Modificar.TabIndex = 2;
            this.Modificar.TabStop = true;
            this.Modificar.Text = "MODIFICAR";
            this.Modificar.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Modificar_LinkClicked);
            // 
            // Crear
            // 
            this.Crear.ActiveLinkColor = System.Drawing.Color.WhiteSmoke;
            this.Crear.AutoSize = true;
            this.Crear.Font = new System.Drawing.Font("Raleway Medium", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Crear.LinkColor = System.Drawing.Color.Fuchsia;
            this.Crear.Location = new System.Drawing.Point(12, 3);
            this.Crear.Name = "Crear";
            this.Crear.Size = new System.Drawing.Size(77, 22);
            this.Crear.TabIndex = 0;
            this.Crear.TabStop = true;
            this.Crear.Text = "CREAR";
            this.Crear.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Crear_LinkClicked);
            // 
            // contenedor4
            // 
            this.contenedor4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contenedor4.Location = new System.Drawing.Point(0, 27);
            this.contenedor4.Name = "contenedor4";
            this.contenedor4.Size = new System.Drawing.Size(566, 327);
            this.contenedor4.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.Modificar);
            this.panel1.Controls.Add(this.Crear);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(566, 27);
            this.panel1.TabIndex = 2;
            // 
            // ficha
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(566, 354);
            this.Controls.Add(this.contenedor4);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ficha";
            this.Text = "ficha";
            this.Load += new System.EventHandler(this.asignarpermi);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.LinkLabel Modificar;
        private System.Windows.Forms.LinkLabel Crear;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Panel contenedor4;
    }
}