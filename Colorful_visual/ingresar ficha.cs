﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Colorfull;

namespace Colorful_visual
{
    public partial class ingresar_ficha : Form
    {
        Ficha obj = new Ficha();
        validador aux = new validador();

        int est,idusuario;
        DataTable tabla;

        public ingresar_ficha(string codigo, string nombre, int estado, int idusuario)
        {
            InitializeComponent();
            Codigo.Text = codigo;
            Nombre.Text = nombre;
             obj.Estado = estado;
            this.idusuario = idusuario;
            this.seleccionar();

        }

        private void seleccionar()
        {
           tabla = obj.nominsumo();

            for (int i = 0; i < tabla.Rows.Count; i++)
            {
                INSUMO.Items.Add(tabla.Rows[i][0].ToString());

            }

        }

        private void GUARDAR_Click(object sender, EventArgs e)
        {
            obj.Ficha1 = Codigo.Text;
            obj.Nombre = Nombre.Text;
            if (INSUMO.SelectedIndex >= 0)
            {
                if (aux.validarSoloNumeros(CANTIDAD.Text))
                {
                    obj.Cantidad = int.Parse(CANTIDAD.Text);
                    for(int i=0; i <= tabla.Rows.Count; i++)
                    {
                        if(INSUMO.Text == tabla.Rows[i][0].ToString())
                        {
                            obj.Insumo = tabla.Rows[i][1].ToString();
                            break;
                        }
                    }

                    obj.Usuario = idusuario;
                    try
                    {
                        if (obj.crearficha() >= 1 && obj.almacenar("CRE", 0) >= 1)
                        {
                            DataGridViewRow fila = new DataGridViewRow();
                            fila.CreateCells(MOSTRAR);

                            fila.Cells[0].Value = Codigo.Text;
                            fila.Cells[1].Value = Nombre.Text;
                            fila.Cells[2].Value = INSUMO.Text;
                            fila.Cells[3].Value = CANTIDAD.Text;

                            MOSTRAR.Rows.Add(fila);
                        }
                        else
                        {
                            MessageBox.Show("Se presento un error inesperado, revise que los datos esten completos.");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Se a presentado un error inesperado, llamar al apoyo tecnico, error: " + ex);
                    }
                                                                            
                    
                }
                else
                {
                    ERROR.SetError(CANTIDAD, "Valor requerido, solo se permiten numeros enteros");
                }
            }
            else
            {
                MessageBox.Show("Selecciones un insumo");
            }
        }
    }
}
