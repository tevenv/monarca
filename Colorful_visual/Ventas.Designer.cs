﻿namespace Colorful_visual
{
    partial class Ventas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Contenedor3 = new System.Windows.Forms.Panel();
            this.contenedor1 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Facturacion = new System.Windows.Forms.Button();
            this.PRODUCTOS = new System.Windows.Forms.Button();
            this.Clientes = new System.Windows.Forms.Button();
            this.contenedor1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Contenedor3
            // 
            this.Contenedor3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Contenedor3.Location = new System.Drawing.Point(0, 100);
            this.Contenedor3.Name = "Contenedor3";
            this.Contenedor3.Size = new System.Drawing.Size(839, 350);
            this.Contenedor3.TabIndex = 1;
            // 
            // contenedor1
            // 
            this.contenedor1.BackgroundImage = global::Colorful_visual.Properties.Resources.rojao;
            this.contenedor1.Controls.Add(this.panel1);
            this.contenedor1.Dock = System.Windows.Forms.DockStyle.Top;
            this.contenedor1.Location = new System.Drawing.Point(0, 0);
            this.contenedor1.Name = "contenedor1";
            this.contenedor1.Size = new System.Drawing.Size(839, 100);
            this.contenedor1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.Facturacion);
            this.panel1.Controls.Add(this.PRODUCTOS);
            this.panel1.Controls.Add(this.Clientes);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(775, 100);
            this.panel1.TabIndex = 1;
            // 
            // Facturacion
            // 
            this.Facturacion.BackgroundImage = global::Colorful_visual.Properties.Resources.FACTURACION;
            this.Facturacion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Facturacion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Facturacion.FlatAppearance.BorderSize = 0;
            this.Facturacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Facturacion.Location = new System.Drawing.Point(494, 20);
            this.Facturacion.Name = "Facturacion";
            this.Facturacion.Size = new System.Drawing.Size(137, 58);
            this.Facturacion.TabIndex = 3;
            this.Facturacion.UseVisualStyleBackColor = true;
            this.Facturacion.Click += new System.EventHandler(this.Facturacion_Click);
            // 
            // PRODUCTOS
            // 
            this.PRODUCTOS.BackgroundImage = global::Colorful_visual.Properties.Resources.PRODUCTO;
            this.PRODUCTOS.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PRODUCTOS.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PRODUCTOS.FlatAppearance.BorderSize = 0;
            this.PRODUCTOS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PRODUCTOS.Location = new System.Drawing.Point(319, 20);
            this.PRODUCTOS.Name = "PRODUCTOS";
            this.PRODUCTOS.Size = new System.Drawing.Size(137, 58);
            this.PRODUCTOS.TabIndex = 2;
            this.PRODUCTOS.UseVisualStyleBackColor = true;
            this.PRODUCTOS.Click += new System.EventHandler(this.PRODUCTOS_Click);
            // 
            // Clientes
            // 
            this.Clientes.BackgroundImage = global::Colorful_visual.Properties.Resources.CLIENTES;
            this.Clientes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Clientes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Clientes.FlatAppearance.BorderSize = 0;
            this.Clientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Clientes.Location = new System.Drawing.Point(138, 20);
            this.Clientes.Name = "Clientes";
            this.Clientes.Size = new System.Drawing.Size(137, 58);
            this.Clientes.TabIndex = 1;
            this.Clientes.UseVisualStyleBackColor = true;
            this.Clientes.Click += new System.EventHandler(this.Clientes_Click);
            // 
            // Ventas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(839, 450);
            this.Controls.Add(this.Contenedor3);
            this.Controls.Add(this.contenedor1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Ventas";
            this.Text = "Ventas";
            this.TransparencyKey = System.Drawing.Color.White;
            this.contenedor1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel contenedor1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel Contenedor3;
        private System.Windows.Forms.Button Facturacion;
        private System.Windows.Forms.Button PRODUCTOS;
        private System.Windows.Forms.Button Clientes;
    }
}