﻿namespace Colorful_visual
{
    partial class Insumos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.Modificar = new System.Windows.Forms.LinkLabel();
            this.Ingresar = new System.Windows.Forms.LinkLabel();
            this.Crear = new System.Windows.Forms.LinkLabel();
            this.contenedor4 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.Modificar);
            this.panel1.Controls.Add(this.Ingresar);
            this.panel1.Controls.Add(this.Crear);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(566, 27);
            this.panel1.TabIndex = 0;
            // 
            // Modificar
            // 
            this.Modificar.ActiveLinkColor = System.Drawing.Color.WhiteSmoke;
            this.Modificar.AutoSize = true;
            this.Modificar.Font = new System.Drawing.Font("Raleway Medium", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Modificar.LinkColor = System.Drawing.Color.Fuchsia;
            this.Modificar.Location = new System.Drawing.Point(227, 1);
            this.Modificar.Name = "Modificar";
            this.Modificar.Size = new System.Drawing.Size(125, 26);
            this.Modificar.TabIndex = 2;
            this.Modificar.TabStop = true;
            this.Modificar.Text = "MODIFICAR";
            this.Modificar.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Modificar_LinkClicked);
            // 
            // Ingresar
            // 
            this.Ingresar.ActiveLinkColor = System.Drawing.Color.WhiteSmoke;
            this.Ingresar.AutoSize = true;
            this.Ingresar.Font = new System.Drawing.Font("Raleway Medium", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ingresar.LinkColor = System.Drawing.Color.Fuchsia;
            this.Ingresar.Location = new System.Drawing.Point(101, 1);
            this.Ingresar.Name = "Ingresar";
            this.Ingresar.Size = new System.Drawing.Size(114, 26);
            this.Ingresar.TabIndex = 1;
            this.Ingresar.TabStop = true;
            this.Ingresar.Text = "INGRESAR";
            this.Ingresar.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Ingresar_LinkClicked);
            // 
            // Crear
            // 
            this.Crear.ActiveLinkColor = System.Drawing.Color.WhiteSmoke;
            this.Crear.AutoSize = true;
            this.Crear.Font = new System.Drawing.Font("Raleway Medium", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Crear.LinkColor = System.Drawing.Color.Fuchsia;
            this.Crear.Location = new System.Drawing.Point(12, 1);
            this.Crear.Name = "Crear";
            this.Crear.Size = new System.Drawing.Size(78, 26);
            this.Crear.TabIndex = 0;
            this.Crear.TabStop = true;
            this.Crear.Text = "CREAR";
            this.Crear.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Crear_LinkClicked);
            // 
            // contenedor4
            // 
            this.contenedor4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contenedor4.Location = new System.Drawing.Point(0, 27);
            this.contenedor4.Name = "contenedor4";
            this.contenedor4.Size = new System.Drawing.Size(566, 327);
            this.contenedor4.TabIndex = 1;
            // 
            // Insumos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(566, 354);
            this.Controls.Add(this.contenedor4);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Insumos";
            this.Text = "Insumos";
            this.TransparencyKey = System.Drawing.Color.White;
            this.Load += new System.EventHandler(this.asignarpermi);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.LinkLabel Ingresar;
        private System.Windows.Forms.LinkLabel Crear;
        private System.Windows.Forms.Panel contenedor4;
        private System.Windows.Forms.LinkLabel Modificar;
    }
}