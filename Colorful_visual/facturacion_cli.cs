﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Colorful_visual
{
    public partial class facturacion_cli : Form
    {
        Panel contenedor3;
        public facturacion_cli( Panel contenedor3)
        {
            this.contenedor3 = contenedor3;
            InitializeComponent();
        }

        public void abrirFactura(Object formularioHijo)
        {
            if (this.contenedor3.Controls.Count > 0)
            {
                this.contenedor3.Controls.RemoveAt(0);
            }

            Form formulario = formularioHijo as Form;

            int ancho = (-formulario.Size.Width + contenedor3.Width) / 2;
            int alto = (-formulario.Size.Height + contenedor3.Height) / 2;

            formulario.TopLevel = false;
            formulario.Location = new Point(ancho, alto);
            this.contenedor3.Controls.Add(formulario);
            this.contenedor3.Tag = formulario;
            formulario.Show();
        }

        private void GUARDAR_Click(object sender, EventArgs e)
        {
            this.abrirFactura(new Factura());
        }
    }
}
