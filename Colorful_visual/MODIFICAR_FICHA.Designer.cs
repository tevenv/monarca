﻿namespace Colorful_visual
{
    partial class MODIFICAR_FICHA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BUSCA = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.MUESTRA = new System.Windows.Forms.ListBox();
            this.BUSCAR = new System.Windows.Forms.Button();
            this.MODIFICAR = new System.Windows.Forms.Button();
            this.ESTADO = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.ACTUALIZAR = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BUSCA
            // 
            this.BUSCA.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BUSCA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.BUSCA.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BUSCA.Location = new System.Drawing.Point(92, 53);
            this.BUSCA.Multiline = true;
            this.BUSCA.Name = "BUSCA";
            this.BUSCA.Size = new System.Drawing.Size(112, 20);
            this.BUSCA.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(56, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 14);
            this.label5.TabIndex = 17;
            this.label5.Text = "Ficha";
            // 
            // MUESTRA
            // 
            this.MUESTRA.BackColor = System.Drawing.Color.WhiteSmoke;
            this.MUESTRA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MUESTRA.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MUESTRA.FormattingEnabled = true;
            this.MUESTRA.ItemHeight = 15;
            this.MUESTRA.Location = new System.Drawing.Point(317, 53);
            this.MUESTRA.Name = "MUESTRA";
            this.MUESTRA.Size = new System.Drawing.Size(171, 195);
            this.MUESTRA.TabIndex = 16;
            // 
            // BUSCAR
            // 
            this.BUSCAR.BackColor = System.Drawing.Color.Transparent;
            this.BUSCAR.BackgroundImage = global::Colorful_visual.Properties.Resources.buscar;
            this.BUSCAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BUSCAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BUSCAR.FlatAppearance.BorderSize = 0;
            this.BUSCAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BUSCAR.Location = new System.Drawing.Point(210, 53);
            this.BUSCAR.Name = "BUSCAR";
            this.BUSCAR.Size = new System.Drawing.Size(20, 20);
            this.BUSCAR.TabIndex = 19;
            this.BUSCAR.UseVisualStyleBackColor = false;
            this.BUSCAR.Click += new System.EventHandler(this.BUSCAR_Click);
            // 
            // MODIFICAR
            // 
            this.MODIFICAR.BackColor = System.Drawing.Color.Transparent;
            this.MODIFICAR.BackgroundImage = global::Colorful_visual.Properties.Resources.MODIFICAR;
            this.MODIFICAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MODIFICAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MODIFICAR.FlatAppearance.BorderSize = 0;
            this.MODIFICAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MODIFICAR.Location = new System.Drawing.Point(50, 216);
            this.MODIFICAR.Name = "MODIFICAR";
            this.MODIFICAR.Size = new System.Drawing.Size(95, 32);
            this.MODIFICAR.TabIndex = 21;
            this.MODIFICAR.UseVisualStyleBackColor = false;
            this.MODIFICAR.Click += new System.EventHandler(this.MODIFICAR_Click);
            // 
            // ESTADO
            // 
            this.ESTADO.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ESTADO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ESTADO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ESTADO.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ESTADO.ForeColor = System.Drawing.SystemColors.MenuText;
            this.ESTADO.FormattingEnabled = true;
            this.ESTADO.Items.AddRange(new object[] {
            "Activo",
            "Inactivo"});
            this.ESTADO.Location = new System.Drawing.Point(92, 137);
            this.ESTADO.Name = "ESTADO";
            this.ESTADO.Size = new System.Drawing.Size(154, 23);
            this.ESTADO.TabIndex = 75;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(47, 140);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 14);
            this.label6.TabIndex = 74;
            this.label6.Text = "Estado";
            // 
            // ACTUALIZAR
            // 
            this.ACTUALIZAR.BackColor = System.Drawing.Color.Transparent;
            this.ACTUALIZAR.BackgroundImage = global::Colorful_visual.Properties.Resources.ACTUALIZAR;
            this.ACTUALIZAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ACTUALIZAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ACTUALIZAR.FlatAppearance.BorderSize = 0;
            this.ACTUALIZAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ACTUALIZAR.Location = new System.Drawing.Point(179, 216);
            this.ACTUALIZAR.Name = "ACTUALIZAR";
            this.ACTUALIZAR.Size = new System.Drawing.Size(95, 32);
            this.ACTUALIZAR.TabIndex = 76;
            this.ACTUALIZAR.UseVisualStyleBackColor = false;
            this.ACTUALIZAR.Click += new System.EventHandler(this.ACTUALIZAR_Click);
            // 
            // MODIFICAR_FICHA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(558, 328);
            this.Controls.Add(this.ACTUALIZAR);
            this.Controls.Add(this.ESTADO);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.MODIFICAR);
            this.Controls.Add(this.BUSCAR);
            this.Controls.Add(this.BUSCA);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.MUESTRA);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MODIFICAR_FICHA";
            this.Text = "MODIFICAR_FICHA";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button BUSCAR;
        private System.Windows.Forms.TextBox BUSCA;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox MUESTRA;
        private System.Windows.Forms.Button MODIFICAR;
        private System.Windows.Forms.ComboBox ESTADO;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button ACTUALIZAR;
    }
}