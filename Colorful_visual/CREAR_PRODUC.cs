﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Colorfull;

namespace Colorful_visual
{
    public partial class CREAR_PRODUC : Form
    {
        C_Producto obj = new C_Producto();
        int idusuario;
        public CREAR_PRODUC(int idusuario)
        {
            this.idusuario = idusuario;
            InitializeComponent();
            this.seleccionar();
        }
        private void seleccionar()
        {
           DataTable tabla = obj.llamarficha();

            for (int i = 0; i < tabla.Rows.Count; i++)
            {
                FICHA.Items.Add(tabla.Rows[i][0].ToString());

            }

        }

        private void GUARDAR_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(CODIGO.Text))
            {
                ERROR.SetError(CODIGO, "Valor requerido, este valor debe ser unico");
            }
            else
            {
                if (string.IsNullOrEmpty(NOMBRE.Text))
                {
                    ERROR.SetError(NOMBRE, "Valor requerido, este valor debe ser unico");
                }
                else
                {
                    if (FICHA.SelectedIndex >= 0)
                    {
                        if (ESTADO.SelectedIndex >= 0)
                        {
                            obj.Codigo = CODIGO.Text;
                            obj.Nombre = NOMBRE.Text;
                            obj.Ficha = FICHA.Text;
                            obj.Estado = (ESTADO.Text.Equals("Activo")) ? 1 : 0;
                            obj.Cantidad = 0;
                            obj.Usuario = idusuario;

                            this.MUESTRA.Items.Clear();

                            try
                            {
                                if (obj.crear() >= 1 && obj.inproducto("CRE", 0) >= 1)
                                {

                                    DataTable tabla = obj.consulpro(CODIGO.Text);

                                    for (int i = 0; i < tabla.Columns.Count; i++)
                                    {
                                        MUESTRA.Items.Add(tabla.Rows[0][i].ToString());
                                        MUESTRA.Items.Add(" ");

                                    }

                                }
                                else
                                {
                                    if(obj.crear() == -2)
                                    {
                                        MessageBox.Show("El producto ya existe, revisa en la lista de productos con el codigo o el Nombre para poder obtener mas información ");

                                    }
                                    MessageBox.Show("Se presento un error inesperado, revise que los datos esten completos.");
                                }


                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Se a presentado un error inesperado, llamar al apoyo tecnico, error: " + ex);
                            }

                            this.limpiar(this);

                        }
                        else
                        {
                            MessageBox.Show("Seleccione un estado");
                        }

                    }
                    else
                    {
                        MessageBox.Show("Seleccione una ficha");
                    }
                }
            }
        }

        private void limpiar(Control aux)
        {
            foreach (var txt in aux.Controls)
            {
                if (txt is TextBox)
                {
                    ((TextBox)txt).Clear();

                }
                else if (txt is ComboBox)
                {
                    ((ComboBox)txt).SelectedIndex = -1;
                }

            }

            ERROR.Clear();
        }

        private void BUSCAR_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(BUSCA.Text))
            {
                MessageBox.Show("Ingresa por favor un parametro de busqueda");
            }
            else
            {
                Form formulario = new Buscador(BUSCA.Text, MUESTRA, 4);
                this.limpiar(this);
                this.MUESTRA.Items.Clear();
                formulario.Show();

            }
        }

        private void inicipermiso(object sender, EventArgs e)
        {
            DataTable permi = obj.asignarpermi("INSUMOS", idusuario);
            prod.Visible = permi.Rows[0][2].Equals(true) ? true : false;
            BUSCA.Visible = permi.Rows[0][2].Equals(true) ? true : false;
            BUSCAR.Visible = permi.Rows[0][2].Equals(true) ? true : false;
        }
    }
}
