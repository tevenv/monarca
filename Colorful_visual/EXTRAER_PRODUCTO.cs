﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Colorfull;

namespace Colorful_visual
{
    public partial class EXTRAER_PRODUCTO : Form
    {
        C_Producto obj = new C_Producto();
        DataTable tabla = new DataTable();
        validador val = new validador();
        int z = 0;
        int idusuario;

        public EXTRAER_PRODUCTO(int idusuario)
        {
            this.idusuario = idusuario;
            InitializeComponent();
            this.cargar();
        }
        private void cargar()
        {
             tabla = obj.llamarcod();

            for (int i = 0; i < tabla.Rows.Count; i++)
            {
                CODIGO.Items.Add(tabla.Rows[i][0].ToString());

            }
        }

        private void CODIGO_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < tabla.Rows.Count; i++)
            {
                if (tabla.Rows[i][0].ToString().Equals(CODIGO.Text))
                {
                    NOMBRE.Text = tabla.Rows[i][1].ToString();
                    break;
                }
            }

            this.CANTIDAD.Focus();
        }

        private void GUARDAR_Click(object sender, EventArgs e)
        {
            if (CODIGO.SelectedIndex>=0)
            {
                if (val.validarSoloNumeros(CANTIDAD.Text))
                {

                    obj.Codigo = CODIGO.Text;
                    int res =  this.restar(int.Parse(CANTIDAD.Text));
                    obj.Estado = 1;
                    obj.Usuario = idusuario;

                    DataTable tabla = obj.cantidadpro();

                    int x = int.Parse(tabla.Rows[0][0].ToString());
                    

                    if (x == 0 || x < int.Parse(CANTIDAD.Text))
                    {
                        MessageBox.Show("No se cuenta con suficiente inventario de "+ NOMBRE.Text+ " el inventario actualmente es de " + tabla.Rows[0][0].ToString());
                    }
                    else
                    {
                        if (int.Parse(tabla.Rows[0][0].ToString()) < 6 && z==0)
                        {
                            string mens = "El producto " + NOMBRE.Text + " la cantidad actual del producto es de " + tabla.Rows[0][0].ToString()+ " \n Desea seguir viendo este mensaje? ";
                            var resultado = MessageBox.Show(mens, " Notificacion de productos ", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                            if (resultado == DialogResult.No)
                            {
                                z = 1;
                            }
                        }

                        try
                        {
                            if (obj.actualproducto(res) >= 1 && obj.inproducto("EXT", int.Parse(CANTIDAD.Text)) >= 1)
                            {
                                DataGridViewRow fila = new DataGridViewRow();
                                fila.CreateCells(MOSTRAR);

                                fila.Cells[0].Value = CODIGO.Text;
                                fila.Cells[1].Value = NOMBRE.Text;
                                fila.Cells[2].Value = CANTIDAD.Text;

                                MOSTRAR.Rows.Add(fila);
                            }
                            else
                            {
                                MessageBox.Show("Se presento un error inesperado, revise que los datos esten completos.");
                            }

                            this.limpiar(this);

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Se a presentado un error inesperado, llamar al apoyo tecnico, error: " + ex);
                        }
                    }
                 
                }
                else
                {
                    ERROR.SetError(CANTIDAD, "Valor requerido, solo se permiten numeros enteros");
                }

            }
            else
            {
                MessageBox.Show("Seleccione un producto a Extraer.");
            }
        }

        private void limpiar(Control aux)
        {
            foreach (var txt in aux.Controls)
            {
                if (txt is TextBox)
                {
                    ((TextBox)txt).Clear();

                }
                else if (txt is ComboBox)
                {
                    ((ComboBox)txt).SelectedIndex = -1;
                }

            }

            ERROR.Clear();
        }

        private int restar(int x)
        {

            DataTable tabla = obj.cantidadpro();

            return  int.Parse(tabla.Rows[0][0].ToString()) - x;
        }


    }
}
