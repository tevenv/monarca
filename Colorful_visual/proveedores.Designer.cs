﻿namespace ColorFul_visual
{
    partial class proveedores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.NOMBRE = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.NIT = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TELEFONO = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.DIRECCION = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.CORREO = new System.Windows.Forms.TextBox();
            this.MUESTRA = new System.Windows.Forms.ListBox();
            this.nitt = new System.Windows.Forms.Label();
            this.BUSCAR = new System.Windows.Forms.TextBox();
            this.BUSCA = new System.Windows.Forms.Button();
            this.GUARDAR = new System.Windows.Forms.Button();
            this.MODIFICAR = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.Estado = new System.Windows.Forms.ComboBox();
            this.ACTUALIZAR = new System.Windows.Forms.Button();
            this.ERROR = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ERROR)).BeginInit();
            this.SuspendLayout();
            // 
            // NOMBRE
            // 
            this.NOMBRE.BackColor = System.Drawing.Color.WhiteSmoke;
            this.NOMBRE.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NOMBRE.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NOMBRE.Location = new System.Drawing.Point(141, 45);
            this.NOMBRE.Multiline = true;
            this.NOMBRE.Name = "NOMBRE";
            this.NOMBRE.Size = new System.Drawing.Size(193, 20);
            this.NOMBRE.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nombre";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "NIT";
            // 
            // NIT
            // 
            this.NIT.BackColor = System.Drawing.Color.WhiteSmoke;
            this.NIT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NIT.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NIT.Location = new System.Drawing.Point(141, 102);
            this.NIT.Multiline = true;
            this.NIT.Name = "NIT";
            this.NIT.Size = new System.Drawing.Size(193, 20);
            this.NIT.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 162);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Telefono";
            // 
            // TELEFONO
            // 
            this.TELEFONO.BackColor = System.Drawing.Color.WhiteSmoke;
            this.TELEFONO.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TELEFONO.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TELEFONO.Location = new System.Drawing.Point(141, 158);
            this.TELEFONO.Multiline = true;
            this.TELEFONO.Name = "TELEFONO";
            this.TELEFONO.Size = new System.Drawing.Size(193, 20);
            this.TELEFONO.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(19, 214);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Direccion";
            // 
            // DIRECCION
            // 
            this.DIRECCION.BackColor = System.Drawing.Color.WhiteSmoke;
            this.DIRECCION.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DIRECCION.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DIRECCION.Location = new System.Drawing.Point(141, 211);
            this.DIRECCION.Multiline = true;
            this.DIRECCION.Name = "DIRECCION";
            this.DIRECCION.Size = new System.Drawing.Size(193, 20);
            this.DIRECCION.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(19, 274);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Correo electronico";
            // 
            // CORREO
            // 
            this.CORREO.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CORREO.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CORREO.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CORREO.Location = new System.Drawing.Point(141, 270);
            this.CORREO.Multiline = true;
            this.CORREO.Name = "CORREO";
            this.CORREO.Size = new System.Drawing.Size(193, 20);
            this.CORREO.TabIndex = 8;
            // 
            // MUESTRA
            // 
            this.MUESTRA.BackColor = System.Drawing.Color.WhiteSmoke;
            this.MUESTRA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MUESTRA.Font = new System.Drawing.Font("Raleway Medium", 8.999999F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MUESTRA.FormattingEnabled = true;
            this.MUESTRA.ItemHeight = 17;
            this.MUESTRA.Location = new System.Drawing.Point(405, 64);
            this.MUESTRA.Name = "MUESTRA";
            this.MUESTRA.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.MUESTRA.Size = new System.Drawing.Size(198, 221);
            this.MUESTRA.TabIndex = 10;
            // 
            // nitt
            // 
            this.nitt.AutoSize = true;
            this.nitt.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nitt.Location = new System.Drawing.Point(401, 29);
            this.nitt.Name = "nitt";
            this.nitt.Size = new System.Drawing.Size(27, 17);
            this.nitt.TabIndex = 12;
            this.nitt.Text = "NIT";
            // 
            // BUSCAR
            // 
            this.BUSCAR.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BUSCAR.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.BUSCAR.Font = new System.Drawing.Font("Raleway Light", 9.749999F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BUSCAR.Location = new System.Drawing.Point(437, 26);
            this.BUSCAR.Multiline = true;
            this.BUSCAR.Name = "BUSCAR";
            this.BUSCAR.Size = new System.Drawing.Size(130, 20);
            this.BUSCAR.TabIndex = 11;
            // 
            // BUSCA
            // 
            this.BUSCA.BackColor = System.Drawing.Color.Transparent;
            this.BUSCA.BackgroundImage = global::Colorful_visual.Properties.Resources.buscar;
            this.BUSCA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BUSCA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BUSCA.FlatAppearance.BorderSize = 0;
            this.BUSCA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BUSCA.Location = new System.Drawing.Point(580, 25);
            this.BUSCA.Name = "BUSCA";
            this.BUSCA.Size = new System.Drawing.Size(24, 24);
            this.BUSCA.TabIndex = 13;
            this.BUSCA.UseVisualStyleBackColor = false;
            this.BUSCA.Click += new System.EventHandler(this.BUSCA_Click);
            // 
            // GUARDAR
            // 
            this.GUARDAR.BackColor = System.Drawing.Color.Transparent;
            this.GUARDAR.BackgroundImage = global::Colorful_visual.Properties.Resources.GUARDAR;
            this.GUARDAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.GUARDAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.GUARDAR.FlatAppearance.BorderSize = 0;
            this.GUARDAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GUARDAR.Location = new System.Drawing.Point(111, 381);
            this.GUARDAR.Name = "GUARDAR";
            this.GUARDAR.Size = new System.Drawing.Size(95, 32);
            this.GUARDAR.TabIndex = 14;
            this.GUARDAR.UseVisualStyleBackColor = false;
            this.GUARDAR.Click += new System.EventHandler(this.GUARDAR_Click);
            // 
            // MODIFICAR
            // 
            this.MODIFICAR.BackColor = System.Drawing.Color.Transparent;
            this.MODIFICAR.BackgroundImage = global::Colorful_visual.Properties.Resources.MODIFICAR;
            this.MODIFICAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MODIFICAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MODIFICAR.FlatAppearance.BorderSize = 0;
            this.MODIFICAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MODIFICAR.Location = new System.Drawing.Point(235, 381);
            this.MODIFICAR.Name = "MODIFICAR";
            this.MODIFICAR.Size = new System.Drawing.Size(95, 32);
            this.MODIFICAR.TabIndex = 15;
            this.MODIFICAR.UseVisualStyleBackColor = false;
            this.MODIFICAR.Click += new System.EventHandler(this.MODIFICAR_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(395, 342);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 17);
            this.label7.TabIndex = 16;
            this.label7.Text = "Estado";
            // 
            // Estado
            // 
            this.Estado.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Estado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Estado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Estado.FormattingEnabled = true;
            this.Estado.Items.AddRange(new object[] {
            "Activo",
            "Inactivo"});
            this.Estado.Location = new System.Drawing.Point(449, 339);
            this.Estado.Name = "Estado";
            this.Estado.Size = new System.Drawing.Size(153, 25);
            this.Estado.TabIndex = 60;
            // 
            // ACTUALIZAR
            // 
            this.ACTUALIZAR.BackColor = System.Drawing.Color.Transparent;
            this.ACTUALIZAR.BackgroundImage = global::Colorful_visual.Properties.Resources.ACTUALIZAR;
            this.ACTUALIZAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ACTUALIZAR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ACTUALIZAR.FlatAppearance.BorderSize = 0;
            this.ACTUALIZAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ACTUALIZAR.Location = new System.Drawing.Point(357, 381);
            this.ACTUALIZAR.Name = "ACTUALIZAR";
            this.ACTUALIZAR.Size = new System.Drawing.Size(95, 32);
            this.ACTUALIZAR.TabIndex = 18;
            this.ACTUALIZAR.UseVisualStyleBackColor = false;
            this.ACTUALIZAR.Click += new System.EventHandler(this.ACTUALIZAR_Click);
            // 
            // ERROR
            // 
            this.ERROR.ContainerControl = this;
            // 
            // proveedores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(631, 425);
            this.Controls.Add(this.ACTUALIZAR);
            this.Controls.Add(this.Estado);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.MODIFICAR);
            this.Controls.Add(this.GUARDAR);
            this.Controls.Add(this.BUSCA);
            this.Controls.Add(this.nitt);
            this.Controls.Add(this.BUSCAR);
            this.Controls.Add(this.MUESTRA);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.CORREO);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.DIRECCION);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TELEFONO);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.NIT);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.NOMBRE);
            this.Font = new System.Drawing.Font("Raleway Light", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "proveedores";
            this.Text = "proveedores";
            this.Load += new System.EventHandler(this.iniciarpermi);
            ((System.ComponentModel.ISupportInitialize)(this.ERROR)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox NOMBRE;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox NIT;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TELEFONO;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox DIRECCION;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox CORREO;
        private System.Windows.Forms.ListBox MUESTRA;
        private System.Windows.Forms.Label nitt;
        private System.Windows.Forms.TextBox BUSCAR;
        private System.Windows.Forms.Button BUSCA;
        private System.Windows.Forms.Button GUARDAR;
        private System.Windows.Forms.Button MODIFICAR;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox Estado;
        private System.Windows.Forms.Button ACTUALIZAR;
        private System.Windows.Forms.ErrorProvider ERROR;
    }
}