﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Colorfull;

namespace Colorful_visual
{
    public partial class inicio : Form
    {

        Inicio obj = new Inicio();

        public inicio()
        {
            InitializeComponent();
        }

        private void Ingresar_Click(object sender, EventArgs e)
        {
            DataTable tabla = obj.usuarioini(usuario.Text);

            if (tabla.Rows.Count == 1)
            {
                if (tabla.Rows[0][1].Equals(contraseña.Text))
                {

                    if (tabla.Rows[0][2].Equals(false))
                    {
                        MessageBox.Show("usuario inactivo, comuniquese con su administrado");
                        usuario.Clear();
                        contraseña.Clear();
                    }
                    else
                    {
                        DataTable tab = obj.permiusua(int.Parse(tabla.Rows[0][0].ToString()));
                        Form formulario = new principal(int.Parse(tabla.Rows[0][0].ToString()), tab);
                        formulario.Show();
                        this.Visible = false;
                    }                   
                }
                else
                {
                    MessageBox.Show("Contraseña incorrecta");
                    contraseña.Clear();
                }
            }
            else
            {
                MessageBox.Show("El usuario no se encuentra registrao");
                usuario.Clear();
                contraseña.Clear();
            }

        }

        private void Cerrar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }

        private void Registro_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Form formulario = new inscripcion();
            formulario.Show();
            this.Visible = false;
        }

       
    }
}
