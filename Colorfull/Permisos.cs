﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BD_Colorful;
using System.Data;

namespace Colorfull
{
    public class Permisos
    {
        private int usuario;
        private string modulo1;
        private string modulo2;
        private int crear;
        private int modificar;
        private int consultar;
        private int insertar;
        private int usuario2;

        conectar obj;

        public Permisos()
        {
            obj = new conectar();
        }

        public int Usuario { get => usuario; set => usuario = value; }
        public string Modulo1 { get => modulo1; set => modulo1 = value; }
        public string Modulo2 { get => modulo2; set => modulo2 = value; }
        public int Crear { get => crear; set => crear = value; }
        public int Modificar { get => modificar; set => modificar = value; }
        public int Consultar { get => consultar; set => consultar = value; }
        public int Usuario2 { get => usuario2; set => usuario2 = value; }
        public int Insertar { get => insertar; set => insertar = value; }

        public DataTable buscarusuact()
        {
            return obj.consultarBD("cons_usuaact");
        }

        public DataTable codusua(string paramtro)
        {
            string[] parametros = { "parametro" };
            string[] valores = {paramtro};

            return obj.consultarBD("cons_codusu", parametros,valores);
        }

        public int inspermi()
        {
            string[] parametros = { "usuario", "modulo1", "modulo2", "crear", "modificar","insertar", "consultar", "usuario2" };
            string[] valores = {this.Usuario.ToString(), this.Modulo1, this.Modulo2, this.Crear.ToString(), this.Modificar.ToString(), this.Consultar.ToString(), this.Insertar.ToString(), this.Usuario2.ToString() };
            return obj.insertar("IN_permisos", parametros, valores);
        }

        public DataTable conspermiexis(string parametro)
        {
            string[] parametros = {"parametro"};
            string[] valores = { parametro };
            return obj.consultarBD("cons_permiexis", parametros, valores);
        }

        public DataTable conspermi(string parametro)
        {
            string[] parametros = {"parametro"};
            string[] valores = { parametro };
            return obj.consultarBD("bus_permi",parametros,valores);
        }

        public DataTable consulpermi(string parametro)
        {
            string[] parametros = { "parametro" };
            string[] valores = { parametro };
            return obj.consultarBD("cons_permisos", parametros, valores);
        }

        public int actualizar()
        {
            string[] parametros = {"crear", "modificar", "consultar", "insertar", "usuario2", "usuario", "modulo1", "modulo2" };
            string[] valores = {this.Crear.ToString(), this.Modificar.ToString(), this.Consultar.ToString(), this.Insertar.ToString(), this.Usuario2.ToString(), this.Usuario.ToString(), this.Modulo1, this.Modulo2 };
            return obj.insertar("actualizar_permiso", parametros, valores);
        }

        public DataTable asignarpermi(string parametro, int idu)
        {
            string[] parametros = { "parametro", "idusuario" };
            string[] valores = { parametro, idu.ToString() };
            return obj.consultarBD("asignar_permi", parametros, valores);
        }
    }
}
