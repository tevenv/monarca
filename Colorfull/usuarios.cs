﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BD_Colorful;
using System.Data;

namespace Colorfull
{
    public class usuarios
    {
        private string nombre;
        private string apellido;
        private string id;
        private string telefono;
        private string usuario;
        private string contraseña;
        private int estado;
        private int usuario2;
        private conectar obj;

        public usuarios()
        {
            this.nombre = "";
            this.apellido = "";
            this.id = "";
            this.telefono = "";
            this.usuario = "";
            this.contraseña = "";
            this.estado = 0;
           
            obj = new conectar();
        }

        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellido { get => apellido; set => apellido = value; }
        public string Id { get => id; set => id = value; }
        public string Telefono { get => telefono; set => telefono = value; }
        public string Usuario { get => usuario; set => usuario = value; }
        public string Contraseña { get => contraseña; set => contraseña = value; }
        public int Estado { get => estado; set => estado = value; }
        public int Usuario2 { get => usuario2; set => usuario2 = value; }

        public int insusuar()
        {
            string[] parametros = { "Nombre", "Apellido", "CC", "Telefono", "Usuario", "Contraseña","usuario2","Estado" };
            string[] valores = { this.Nombre, this.Apellido, this.Id, this.Telefono, this.Usuario, this.Contraseña, this.Usuario2.ToString(), this.Estado.ToString() };
            return obj.insertar("IN_usuario", parametros, valores);
        }

        public DataTable buscusua(string parametro)
        {
            string[] parametros = { "parametro" };
            string[] valores = { parametro};

            return obj.consultarBD("Bus_usuario", parametros,valores);
        }

        public DataTable consusua(string parametro)
        {
            string[] parametros = { "parametros" };
            string[] valores = { parametro };

            return obj.consultarBD("Consusua", parametros, valores);
        }

        public int actualizarusuario()
        {
            string[] parametros = {  "Telefono", "Contraseña", "usuario2", "Estado", "parametro" };
            string[] valores = { this.Telefono, this.Contraseña, this.Usuario2.ToString(), this.Estado.ToString(), this.Usuario };
            return obj.insertar("Actualizar_usuario", parametros, valores);
        }

        public DataTable asignarpermi(string parametro, int idu)
        {
            string[] parametros = { "parametro", "idusuario" };
            string[] valores = { parametro, idu.ToString() };
            return obj.consultarBD("asignar_permi", parametros, valores);
        }
    }
}
