﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;

using BD_Colorful;



namespace Colorfull
{
    public class AG_insu
    {
        private string insumo;
        private string codigo;
        private string nombre;
        private string proveedor;
        private int cantidad;
        private int estado;
        private int usuario;
        

        private conectar obj;
       

        public AG_insu()
        {
            this.insumo = "";
            this.codigo = "";
            this.nombre = "";
            this.proveedor = "";
            this.cantidad = 0;
            this.estado = 0;
            this.usuario = 0;
            obj = new conectar();  

        }

        public string Insumo { get => insumo; set => insumo = value; }
        public string Codigo { get => codigo; set => codigo = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Proveedor { get => proveedor; set => proveedor = value; }
        public int Cantidad { get => cantidad; set => cantidad = value; }
        public int Estado { get => estado; set => estado = value; }
        public int Usuario { get => usuario; set => usuario = value; }

        public int crear() // crea el insumo y lo inserta en la tabla insumo
        {
            string[] parametros = { "idinsumo", "nombreinsu", "cantidad", "estado", "proveedor", "idusuario"  };
            string[] valores = { this.Codigo, this.Nombre, this.Cantidad.ToString(), this.Estado.ToString(), this.Proveedor, this.usuario.ToString()  };

            return obj.insertar("IN_insumo", parametros, valores);

        }

        public int almacenar(string movimiento, int cantidad) // registra el movimiento y los cambios realizados sobre la tabla insumos
        {
            string documento = "INS"; 

            string[] parametros = { "idproducto", "cantidad", "tipo_de_movimiento", "documento", "estado", "idusuario" };
            string[] valores = {this.Codigo, cantidad.ToString(), movimiento, documento, this.Estado.ToString(), this.Usuario.ToString() };
            return obj.insertar("IN_movimiento", parametros, valores);
        }   

        public DataTable consultarinsumo(string parametro) // llama la tabla de insumos para consultas y cambios
        {
            string[] parametros = {"parametro"};
            string[] valores = {parametro};
            return obj.consultarBD("cons_insumo",parametros,valores);
        }

        public DataTable traerproveedor() // llama el nombre del proveedor para el registro
        {
            return obj.consultarBD("cons_prov");
        }

        public int actualizarinsumo() // actualizar los datos de los insumos.
        {
            string[] parametros = { "idinsumo", "cantidad", "estado", "idusuario"};
            string[] valores = {this.Codigo, this.Cantidad.ToString(), this.Estado.ToString(), this.Usuario.ToString()};

            return obj.insertar("Actualizar_insumo",parametros,valores);
        }

        public DataTable busqueda(string parametro) //hace una consulta rapida de los insumos
        {
            string[] parametros = { "parametro" };
            string[] valores = { parametro };
            return obj.consultarBD("Con_insumo", parametros, valores);

        }

        public DataTable traeinsumosr() // llama el nombre de los insumos; 
        {
            return obj.consultarBD("cons_nominsu");
        }

       public DataTable traercantidad(String parametro)
        {
            string[] parametros = { "parametro" };
            string[] valores = { parametro };
            return obj.consultarBD("Con_insucant", parametros, valores);
        }

        public DataTable asignarpermi(string parametro, int idu)
        {
            string[] parametros = { "parametro", "idusuario" };
            string[] valores = { parametro, idu.ToString() };
            return obj.consultarBD("asignar_permi", parametros, valores);
        }

    }

}
