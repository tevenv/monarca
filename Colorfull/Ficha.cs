﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BD_Colorful;
using System.Data;

namespace Colorfull
{
    public class Ficha
    {
        private string ficha;
        private string nombre;
        private string insumo;
        private int cantidad;
        private int estado;
        private int usuario;

        private string consultar;

        conectar obj = new conectar();

          public Ficha()
          {
              this.ficha = "";
              this.nombre = "";
              this.insumo = "";
              this.cantidad = 0;
              this.estado = 0;
              this.usuario = 0;
              this.consultar = "";

          }

        public string Ficha1 { get => ficha; set => ficha = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Insumo { get => insumo; set => insumo = value; }
        public int Cantidad { get => cantidad; set => cantidad = value; }
        public int Estado { get => estado; set => estado = value; }
        public int Usuario { get => usuario; set => usuario = value; }
        public string Consultar { get => consultar; set => consultar = value; }

        public int crearficha() // crea contenido en la ficha tecnica
        {
            string[] parametros = { "idfichatecnica", "Nombre", "idinsumo", "cantidad", "estado", "idusuario" };
            string[] valores = { this.Ficha1, this.Nombre, this.Insumo, this.Cantidad.ToString(), this.Estado.ToString(),  this.Usuario.ToString()};

            return obj.insertar("IN_ficha", parametros, valores);

        }

        public DataTable nominsumo() //trae el nombre de los insumos creaos
        {
            return obj.consultarBD("cons_insu");
        }

        public DataTable buscar(string parametro) // busca la ficha tecnica deseada
        {
            string[] parametros = { "parametro" };
            string[] valores = { parametro };

            return obj.consultarBD("ficha_tabla", parametros, valores);
        }
      
        public DataTable fichatec(string parametro)//trae los parametros de la ficha tecnica
        {
            string[] parametros = { "parametro" };
            string[] valores = { parametro };

           return obj.consultarBD("bus_ficha", parametros, valores);
        }

       
        public int actualizarestado(int estado1, int usuario1)// actualiza el estado de la ficha tecnica
        {
            string[] parametros = { "estado1", "estado2", "idusuario1", "idusuario2", "parametro" };
            string[] valores = { estado1.ToString(),this.Estado.ToString(), usuario1.ToString(), this.Usuario.ToString(), this.Ficha1 };

            return obj.insertar("actualizar_ficha", parametros,valores);
        }

        public int almacenar(string movimiento, int cantidad) // registra el movimiento y los cambios realizados sobre la tabla insumos
        {
            string documento = "FTC";

            string[] parametros = { "idproducto", "cantidad", "tipo_de_movimiento", "documento", "estado", "idusuario" };
            string[] valores = { this.Ficha1, cantidad.ToString(), movimiento, documento, this.Estado.ToString(), this.Usuario.ToString() };
            return obj.insertar("IN_movimiento", parametros, valores);
        }

        public DataTable usuarioftc(string parametro)//trae el ultimo usuario que modifico la tabla
        {
            string[] parametros = { "parametro" };
            string[] valores = { parametro };

            return obj.consultarBD("USUA_FTC", parametros, valores);
        }
        public DataTable asignarpermi(string parametro, int idu)
        {
            string[] parametros = { "parametro", "idusuario" };
            string[] valores = { parametro, idu.ToString() };
            return obj.consultarBD("asignar_permi", parametros, valores);
        }

    }
}
