﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using BD_Colorful;

namespace Colorfull
{
    public class informes
    {
        conectar obj = new conectar();
        
        public DataTable proveedores(int parametro)
        {
            string[] parametros = {"parametro"};
            string[] valores = {parametro.ToString()};
            return obj.consultarBD("Info_provee",parametros,valores);
        }
        public DataTable allproveedores()
        {
            return obj.consultarBD("info_allprovee");
        }

        public DataTable productos(int estado, string documento, DateTime fechaini, DateTime fechafin)
        {
            string[] parametros = {"estado", "documento", "fechaini", "fechafin" };
            string[] valores = {estado.ToString(), documento, fechaini.ToString("yyyy/MM/dd"), fechafin.ToString("yyyy/MM/dd") };
            return obj.consultarBD("info_product", parametros, valores);
        }

        public DataTable allproductos( string documento, DateTime fechaini, DateTime fechafin)
        {
            string[] parametros = {  "documento", "fechaini", "fechafin" };
            string[] valores = {  documento, fechaini.ToString("yyyy/MM/dd"), fechafin.ToString("yyyy/MM/dd") };
            return obj.consultarBD("info_Allproducto", parametros, valores);
        }

        public DataTable actproductos()
        {
            return obj.consultarBD("info_actproducto");
        }

        public DataTable insumos(int estado, string documento, DateTime fechaini, DateTime fechafin)
        {
            string[] parametros = { "estado", "documento", "fechaini", "fechafin" };
            string[] valores = { estado.ToString(), documento, fechaini.ToString("yyyy/MM/dd"), fechafin.ToString("yyyy/MM/dd") };
            return obj.consultarBD("info_insumos", parametros, valores);
        }

        public DataTable allinsumos(string documento, DateTime fechaini, DateTime fechafin)
        {
            string[] parametros = {"documento", "fechaini", "fechafin" };
            string[] valores = { documento, fechaini.ToString("yyyy/MM/dd"), fechafin.ToString("yyyy/MM/dd") };
            return obj.consultarBD("info_allinsumos", parametros, valores);
        }

        public DataTable actinsumos()
        {
            return obj.consultarBD("info_actinsumos");
        }
    }
}
