﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BD_Colorful;
using System.Data;

namespace Colorfull
{
    public class proveedor
    {
        private string nombre;
        private string nit;
        private string telefono;
        private string direccion;
        private string correo;
        private int estado;
        private int usuario;
        private conectar obj;

        public proveedor()
        {
            this.nombre = "";
            this.nit = "";
            this.telefono = "";
            this.direccion = "";
            this.correo = "";

            obj = new conectar();

        }

        public string Nombre { get => nombre; set => nombre = value; }
        public string Nit { get => nit; set => nit = value; }
        public string Telefono { get => telefono; set => telefono = value; }
        public string Direccion { get => direccion; set => direccion = value; }
        public string Correo { get => correo; set => correo = value; }
        public int Usuario { get => usuario; set => usuario = value; }
        public int Estado { get => estado; set => estado = value; }

        public int insrpro() // inserta los parametros deseados 
        {
            string[] parametros = { "Nombre", "Nit", "Telefono", "Direccion", "Correoelec","estado", "idusuario" };
            string[] valores = {this.Nombre, this.Nit, this.Telefono, this.Direccion, this.Correo, this.Estado.ToString(), this.Usuario.ToString() };

            return obj.insertar("IN_proveedor", parametros, valores);
        }

        public DataTable conspro(string parametro)// consulta el proveedor deseado
        {
            string[] parametros = {"parametro" };
            string[] valores = { parametro };

            return obj.consultarBD("Cons_provee", parametros, valores);
        }

        public DataTable proveed(string parametro) //muestra la informacion del proveedor seleccionado
        {
            string[] parametros = { "parametros" };
            string[] valores = { parametro };

            return obj.consultarBD("Cons_proveedor", parametros, valores);
        }

        public int actuprovee() //actualiza el proveedor.
        {
            string[] parametros = {  "Nit", "Telefono", "Direccion", "Correoelec", "estado", "idusuario" };
            string[] valores = { this.Nit, this.Telefono, this.Direccion, this.Correo, this.Estado.ToString(), this.Usuario.ToString() };

            return obj.insertar("Actualizar_proveedor", parametros, valores);
        }

        public DataTable asignarpermi(string parametro, int idu)
        {
            string[] parametros = { "parametro", "idusuario" };
            string[] valores = { parametro, idu.ToString() };
            return obj.consultarBD("asignar_permi", parametros, valores);
        }

    }
}
