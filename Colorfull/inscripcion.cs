﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BD_Colorful;
using System.Data;
using System.Drawing;

namespace Colorfull
{
    public class Inscripcion
    {

        conectar obj = new conectar();

        private string empresa;
        private string NIT;
        private string telefono;
        private string direccion;
        private string slogan;
        private string logo;

        private string nombre;
        private string apellido;
        private string identificacion;
        private string telefonoad;
        private string usuario;
        private string contraseña;

        public Inscripcion()
        {
            
        }

        public string Empresa { get => empresa; set => empresa = value; }
        public string NIT1 { get => NIT; set => NIT = value; }
        public string Telefono { get => telefono; set => telefono = value; }
        public string Direccion { get => direccion; set => direccion = value; }
        public string Slogan { get => slogan; set => slogan = value; }
        public string Logo { get => logo; set => logo = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellido { get => apellido; set => apellido = value; }
        public string Identificacion { get => identificacion; set => identificacion = value; }
        public string Telefonoad { get => telefonoad; set => telefonoad = value; }
        public string Usuario { get => usuario; set => usuario = value; }
        public string Contraseña { get => contraseña; set => contraseña = value; }


        public int insertempresa()
        {
            string[] parametros = { "NombreEmpre", "nit","telefono","direccion","logo", "slogan", "Nombreadmin", "Apellidoadmin", "identificacion", "telefonoadm", "Usuario", "contraseña"};
            string[] valores = {this.Empresa, this.NIT1, this.Telefono, this.Direccion, this.logo, this.slogan, this.Nombre, this.Apellido, this.Identificacion, this.Telefonoad, this.Usuario, this.Contraseña };
            return obj.insertar("IN_empresa", parametros, valores);
        }

        public DataTable mostrarlog(int parametro)
        {
            string[] parametros = { "parametro" };
            string[] valores = { parametro.ToString() };
            return obj.consultarBD("llamarimg", parametros, valores);
        } 



    }
}
