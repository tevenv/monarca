﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;

using BD_Colorful;

namespace Colorfull
{
    public class Inicio
    {
        conectar obj = new conectar();

        public DataTable usuarioini(string parametro)
        {
            string[] parametros = { "parametro" };
            string[] valores = { parametro };
            return obj.consultarBD("ini_usua", parametros, valores);
        }

        public DataTable permiusua(int parametro)
        {
            string[] parametros = { "parametro" };
            string[] valores = { parametro.ToString() };
            return obj.consultarBD("ini_permi", parametros, valores);
        }
    }
}
