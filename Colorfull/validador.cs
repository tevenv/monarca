﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;

namespace Colorfull
{
    public class validador
    {
        private Regex rgx;

        public bool validarNIT(string cadena)
        {

            rgx = new Regex(@"\A[0-9]{8,11}(-)[0-9]{1,2}\Z");
            if (rgx.IsMatch(cadena))
                return true;
            return false;
        }

        public bool validarSoloLetras(string cadena)
        {
            rgx = new Regex("^[A-Z a-z áéíóúñÑ]*$");
            if (rgx.IsMatch(cadena))
                return true;
            return false;
        }
        public bool validarSoloNumeros(string cadena)
        {
            rgx = new Regex("^[0-9]+$");
            if (rgx.IsMatch(cadena))
                return true;
            return false;
        }

        public bool validarCorreoElectronico(string cadena)
        {
            rgx = new Regex(@"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
                            + "@"
                            + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$");
            if (rgx.IsMatch(cadena))
                return true;
            return false;
        }

        public bool validarNumeroCelular(string cadena)
        {
            rgx = new Regex(@"\A[0-9]{10}\Z");
            if (rgx.IsMatch(cadena))
                return true;
            return false;
                       
        }

        public bool validarNumeroIdentificacion(string cadena)
        {
            rgx = new Regex(@"\A[0-9]{8,10}\Z");
            if (rgx.IsMatch(cadena))
                return true;
            return false;

        }

        public bool validarDireccion(string cadena)
        {
            rgx = new Regex("^.*(?=.*[0-9])(?=.*[a-zA-ZñÑ\\s]).*$");
            if (rgx.IsMatch(cadena))
                return true;
            return false;
        }
    }
}
