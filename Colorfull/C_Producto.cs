﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BD_Colorful;

using System.Data;
using System.Collections;

namespace Colorfull
{
    public class C_Producto
    {
        private string codigo;
        private string nombre;
        private int cantidad;
        private int estado;
        private int usuario;
        private string ficha;
        private conectar obj;
        public C_Producto()
        {
            this.codigo = "";
            this.nombre = "";
            this.cantidad = 0;
            this.estado = 0;
            this.usuario = 0;
            this.ficha = "";
            obj = new conectar();
        }

        public string Codigo { get => codigo; set => codigo = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public int Cantidad { get => cantidad; set => cantidad = value; }
        public int Estado { get => estado; set => estado = value; }
        public int Usuario { get => usuario; set => usuario = value; }
        public string Ficha { get => ficha; set => ficha = value; }

        public int crear() // crea un producto nuevo
        {
            
            string[] parametros = { "idproducto", "Nombre", "cantidad", "idfichatecnica", "estado", "idusuario" };
            string[] valores = {this.Codigo, this.Nombre, this.Cantidad.ToString(), this.Ficha, this.Estado.ToString(),  this.Usuario.ToString() };

            return obj.insertar("IN_producto", parametros, valores);
        }

        public int inproducto(string movimiento,int cantidad) // ingresa el movimiento del producto en tabla movimiento
        {            
            string documento = "PRO";

            string[] parametros = { "idproducto", "cantidad", "tipo_de_movimiento", "documento","estado", "idusuario" };
            string[] valores = { this.Codigo, cantidad.ToString(), movimiento, documento, this.Estado.ToString(), this.Usuario.ToString() };

            return obj.insertar("IN_movimiento", parametros, valores);
        }

        public int exproducto() // extrae el movimiento del producto en tabla movimiento
        {
            string movimiento = "EXT";
            string documento = "PROD";

            string[] parametros = { "idproducto", "cantidad", "tipo_de_movimiento", "documento", "idusuario" };
            string[] valores = { this.Codigo, this.Cantidad.ToString(), movimiento, documento, this.Usuario.ToString() };

            return obj.insertar("IN_movimiento", parametros, valores);
        }

        public DataTable consulta() // llama la ficha tecnica
        {
            string[] parametro = { "parametro" };
            string[] valores = { this.ficha };

            return obj.consultarBD("llamar_ficha", parametro, valores);
        }

        private Tuple<List<string>, List<int>> operacion() //hace la operacion de insercion del insumo basado en la ficha tecnica
        {
            DataTable tabla = new DataTable();

            List<string> codigoA = new List<string>();
            List<int> cantidadins = new List<int>();
            List<int> cantidadfic = new List<int>();

            tabla = this.consulta();

            for (int i = 0; i < tabla.Rows.Count; i++)
            {

                codigoA.Add(Convert.ToString(tabla.Rows[i][0]));
                cantidadins.Add(Convert.ToInt32(Convert.ToString(tabla.Rows[i][1])));
                cantidadfic.Add(Convert.ToInt32(Convert.ToString(tabla.Rows[i][2])));

            }

            for (int i = 0; i < cantidadins.Count; i++)
            {
                cantidadins[i] = cantidadins[i] - (cantidadfic[i] * this.Cantidad);
            }

            Tuple<List<string>, List<int>> aux = new Tuple<List<string>, List<int>>(codigoA, cantidadins);

            return aux;
        }

        private Tuple<List<string>, List<int>> operacionins() //crea el movimiento del insumo en la tabla movimiento
        {
            DataTable tabla = new DataTable();

            List<string> codigoA = new List<string>();
            List<int> cantidadins = new List<int>();
            List<int> cantidadfic = new List<int>();

            tabla = this.consulta();

            for (int i = 0; i < tabla.Rows.Count; i++)
            {

                codigoA.Add(Convert.ToString(tabla.Rows[i][0]));
                cantidadins.Add(Convert.ToInt32(Convert.ToString(tabla.Rows[i][1])));
                cantidadfic.Add(Convert.ToInt32(Convert.ToString(tabla.Rows[i][2])));

            }

            for (int i = 0; i < cantidadins.Count; i++)
            {
                cantidadins[i] = (cantidadfic[i] * this.Cantidad);
            }

            Tuple<List<string>, List<int>> aux = new Tuple<List<string>, List<int>>(codigoA, cantidadins);

            return aux;
        }

        public int actualizarins() //actualiza la cantidad de insumos en la tabla insumo
        {
            Tuple<List<string>, List<int>> aux = operacion();

            List<string> idinsumo = new List<string>();
            List<int> cantidad = new List<int>();

            int x = 0;

            idinsumo = aux.Item1;
            cantidad = aux.Item2;

            for (int i = 0; i < idinsumo.Count; i++)
            {
                string[] parametro = { "idinsumo", "cantidad" };
                string[] valores = { idinsumo[i], cantidad[i].ToString() };



                x = obj.insertar("actualizar_insu", parametro, valores);
            }

            return x;
        }

        public int exinsumo() // reguistra la extracion de los insumos en la tabla movimiento
        {
            Tuple<List<string>, List<int>> aux = operacionins();

            List<string> idinsumo = new List<string>();
            List<int> cantidad = new List<int>();

            int x = 0;

            string movimiento = "EXT";
            string documento = "INS";

            idinsumo = aux.Item1;
            cantidad = aux.Item2;

            for (int i = 0; i < idinsumo.Count; i++)
            {
                string[] parametros = { "idproducto", "cantidad", "tipo_de_movimiento", "documento", "estado", "idusuario" };
                string[] valores = { idinsumo[i], cantidad[i].ToString(), movimiento, documento, this.Estado.ToString(),this.Usuario.ToString() };



                x = obj.insertar("IN_movimiento", parametros, valores);
            }

            return x;

        }

        public DataTable consultabpro() // trae los campos de la tabla producto para su insercion por medio de Cb
        {
            string[] parametro = { "parametro" };
            string[] valores = { this.Codigo };

            return obj.consultarBD("cons_prod", parametro, valores);
        }

        public DataTable llamarcod() // llamara el codigo que se usara en el Cb
        {
            return obj.consultarBD("cod_producto");
        }

        public DataTable llamarficha() // consulta el codigo de las fichas tecnicas
        {
            return obj.consultarBD("cons_ficha");
        }

        public DataTable consulpro(string parametro) // trae los campos de la tabla producto para su insercion por medio de Cb
        {
            string[] parametros = { "parametro" };
            string[] valores = { parametro };

            return obj.consultarBD("consul_prod", parametros, valores);
        }
        public DataTable buscar(string parametro) // busca la ficha tecnica deseada
        {
            string[] parametros = { "parametro" };
            string[] valores = { parametro };

            return obj.consultarBD("prod_tabla", parametros, valores);
        }

        public int actualproducto(int cant)
        {
            string[] parametros = { "cantidad", "estado", "idusuario", "parametro" };
            string[] valores = { cant.ToString(), this.Estado.ToString(), this.Usuario.ToString(), this.Codigo };
            return obj.insertar("Actualizar_produ", parametros, valores);
        }

        public Tuple<List<string>, List<int>, int, int> control()
        {
            DataTable tabla = this.consulta();

            List<string> codigoA = new List<string>();
            List<int> cantidadins = new List<int>();
            List<int> cantidadfic = new List<int>();
            List<int> aux = new List<int>();
            List<string> insumo = new List<string>();
            List<int> cantidad = new List<int>();
            List<int> cant = new List<int>();
            int j = 0, x = 0;

            

            for (int i = 0; i < tabla.Rows.Count; i++)
            {

                codigoA.Add(Convert.ToString(tabla.Rows[i][0]));
                cantidadins.Add(Convert.ToInt32(Convert.ToString(tabla.Rows[i][1])));
                cantidadfic.Add(Convert.ToInt32(Convert.ToString(tabla.Rows[i][2])));

            }
           

            for (int i = 0; i< cantidadfic.Count; i++)
            {
                aux.Add(cantidadfic[i] * 6);
                cant.Add(cantidadfic[i]* this.Cantidad);

                if (cantidadins[i] < aux[i])
                {
                    if (cantidadins[i] == 0 || cantidadins[i]<cantidadfic[i]|| cantidadins[i]<cant[i])
                    {
                        
                        j = 1;
                    }
                    insumo.Add(codigoA[i]);
                    cantidad.Add(cantidadins[i]); 

                    x = 1;
                }

               

            }

            Tuple<List<string>, List<int>, int, int> auxi = new Tuple<List<string>, List<int>, int, int>(insumo, cantidad, x, j);

            return auxi;

        }

        public DataTable cantidadpro()
        {

            string[] parametros = {"parametro"};
            string[] valores = {this.codigo};

            return obj.consultarBD("con_cantpro", parametros, valores);
            
        }

        public DataTable asignarpermi(string parametro, int idu)
        {
            string[] parametros = { "parametro", "idusuario" };
            string[] valores = { parametro, idu.ToString() };
            return obj.consultarBD("asignar_permi", parametros, valores);
        }
    }
}
