﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;


namespace BD_Colorful
{
    public class conectar
    {
        private MySqlDataReader reader = null;
        private MySqlCommand cmd;
        private MySqlConnection Base;

        public conectar()
        {
          Base = new MySqlConnection("server=127.0.0.1; port=3306; uid=root; pwd=Steven93;  database=colorful; ");
        }

        public int insertar(string comando, string[] parametros, string[] valores)
        {
            cmd = new MySqlCommand(comando, Base);
            int i = 0;
            int filas = 0;

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = comando;

            
            foreach (string parametro in parametros)
            {
                cmd.Parameters.AddWithValue(parametro, valores[i]);

                i++;
            }

            try
            {
                Base.Open();
                filas = cmd.ExecuteNonQuery();
                Base.Close();
            }
            catch (MySqlException ex)
            {
                Base.Close();

                switch (ex.Number)
                {
                    case 2627:
                        return -1;
                    case 1062:
                        return -2;
                    default:
                        return -3;

                }
            }

            return filas;

        }

        public DataTable consultarBD(string comando)
        {
            DataTable tabla = new DataTable();

            cmd = new MySqlCommand(comando, Base);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                Base.Open();
                reader = cmd.ExecuteReader();
                tabla.Load(reader);
                Base.Close();
            }
            catch (Exception e)
            {
                Base.Close();
            }

            return tabla;
        }

        public DataTable consultarBD(string comando, string[] parametros, string[] valores)
        {
            DataTable tabla = new DataTable();
            int i = 0;



            cmd = new MySqlCommand(comando, Base);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = comando;

            foreach (string parametro in parametros)
            {
                cmd.Parameters.AddWithValue(parametro, valores[i]);

                i++;
            }

            try
            {
                
                Base.Open();
                reader = cmd.ExecuteReader();
              
                tabla.Load(reader);
                Base.Close();
            }
            catch (Exception e)
            {
                Base.Close();
            }
           int f= tabla.Rows.Count;
            return tabla;
        }



    }
}
